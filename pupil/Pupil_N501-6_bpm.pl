  ŠV  9Q  $TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1297867019
$MTIME = 1297867019
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2021-02-15T21:36:59' / Date FITS file was generated
IRAF-TLM= '2021-02-15T21:37:01' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  15. / [s] Requested exposure duration
EXPTIME =                  15. / [s] Exposure duration
EXPDUR  =                  15. / [s] Exposure duration
DARKTIME=           16.2098701 / [s] Dark time
OBSID   = 'ct4m20210129t004245' / Unique Observation ID
DATE-OBS= '2021-01-29T00:42:45.336392' / DateTime of observation start (UTC)
TIME-OBS= '00:42:45.336392'    / Time of observation start (UTC)
MJD-OBS =       59243.02969139 / MJD of observation start
MJD-END =      59243.029865001 / Exposure end
OPENSHUT= '2021-01-29T00:42:45.487770' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               961071 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'DECam Engineering'  / Current observing orogram
OBSERVER= 'Alistair Walker'    / Observer name(s)
PROPOSER= 'Walker  '           / Proposal Principle Investigator
DTPI    = 'Walker  '           / Proposal Principle Investigator (iSTB)
PROPID  = '2013A-9999'         / Proposal ID
EXCLUDED= 'GUIDER  '           / DECam components not used for this frame
SEQID   = '5 exposures'        / Sequence name
SEQNUM  =                    1 / Number of image in sequence
SEQTOT  =                    5 / Total number of images in sequence
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    0 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'N501 DECam c0010 5010.0 75.0' / Unique filter identifier
INSTANCE= 'DECam_20210128'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '04:00:40.601'       / [HH:MM:SS] Target RA
DEC     = '-30:29:25.998'      / [DD:MM:SS] Target DEC
TELRA   = '04:00:40.680'       / [HH:MM:SS] Telescope RA
TELDEC  = '-30:29:27.298'      / [DD:MM:SS] Telescope DEC
HA      = '00:31:34.780'       / [HH:MM:SS] Telescope hour angle
ZD      =                 6.77 / [deg] Telescope zenith distance
AZ      =             265.7317 / [deg] Telescope azimuth angle
DOMEAZ  =               263.87 / [deg] Dome azimuth angle
ZPDELRA =             -39.2328 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                  14. / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-162.57,890.57,2396.09,-146.68,-121.13, 0.00' / DECam hexapod setting
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    T / RASICAM local sky clear flag
WINDSPD =                8.047 / [m/s] Wind speed
WINDDIR =                 337. / [deg] Wind direction (from North)
HUMIDITY=                  41. / [%] Ambient relative humidity (outside)
PRESSURE=                 778. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                0.765 / [arcsec] DIMM2 Seeing
MASS2   =                 0.29 /  MASS(2) FSEE
ASTIG1  =                 0.04 / 4MAPS correction 1
ASTIG2  =                 0.05 / 4MAPS correction 2
OUTTEMP =                 14.9 / [deg C] Outside temperature
AIRMASS =                 1.01 / Airmass
GSKYVAR =                0.036 / RASICAM global sky standard deviation
GSKYHOT =                0.049 / RASICAM global sky fraction above threshold
LSKYVAR =                   0. / RASICAM local sky standard deviation
LSKYHOT =                   0. / RASICAM local sky fraction above threshold
LSKYPOW =                   0. / RASICAM local sky normalized power
MSURTEMP=               16.087 / [deg C] Mirror surface average temperature
MAIRTEMP=                 16.1 / [deg C] Mirror temperature above surface
UPTRTEMP=               15.537 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 15.9 / [deg C] Mirror top surface temperature
UTN-TEMP=                15.67 / [deg C] Upper truss temperature north
UTS-TEMP=               15.355 / [deg C] Upper truss temperature south
UTW-TEMP=               15.455 / [deg C] Upper truss temperature west
UTE-TEMP=                15.67 / [deg C] Upper truss temperature east
PMN-TEMP=                 15.9 / [deg C] Mirror north edge temperature
PMS-TEMP=                 16.1 / [deg C] Mirror south edge temperature
PMW-TEMP=                 16.3 / [deg C] Mirror west edge temperature
PME-TEMP=                16.05 / [deg C] Mirror east edge temperature
DOMELOW =                15.39 / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                 11.7 / [deg C] Dome floor temperature
DONUTFS4= '[1.21,1.98,-8.07,-0.08,0.05,-0.06,0.03,-0.06,-0.48,]' / Mean Wavefron
DONUTFS3= '[]      '           / Mean Wavefront for Sensor FS3
DONUTFS2= '[]      '           / Mean Wavefront for Sensor FS2
DONUTFS1= '[-0.27,-0.14,9.56,0.62,0.10,-0.03,-0.09,0.11,-0.05,]' / Mean Wavefron
DONUTFN1= '[1.66,2.13,-8.09,-0.18,-0.01,0.00,-0.02,0.23,-0.33,]' / Mean Wavefron
DONUTFN2= '[]      '           / Mean Wavefront for Sensor FN2
DONUTFN3= '[]      '           / Mean Wavefront for Sensor FN3
DONUTFN4= '[]      '           / Mean Wavefront for Sensor FN4
HIERARCH TIME_RECORDED = '2021-01-29T00:43:20.706697'
DOXT    =                -0.05 / [arcsec] X-theta from donut analysis
FADZ    =              -120.61 / [um] FA Delta focus.
FADY    =                -0.12 / [um] FA Delta Y.
FADX    =                -0.05 / [um] FA Delta X.
FAYT    =                -0.87 / [arcsec] FA Delta Y-theta.
DODZ    =              -120.61 / [um] Delta-Z from donut analysis
DODY    =                -0.87 / [um] Y-decenter from donut analysis
DODX    =                -1.32 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2021-01-28T08:57:59' / Time of last RASICAM exposure (UTC)
DOYT    =                -0.12 / [arcsec] Y-theta from donut analysis
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                -1.32 / [arcsec] FA Delta X-theta.
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:89'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTACQNAM= '/data_local/images/DTS/2013A-9999/DECam_00961071.fits.fz'
ODATEOBS= '2021-01-29T00:42:45.336392'
DTPROPID= '2013A-9999'
DTCALDAT= '2021-01-28'
DTNSANAM= 'c4d_210129_004245_ori.fits.fz'
DTINSTRU= 'decam   '
DTTELESC= 'ct4m    '
DTSITE  = 'ct      '
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.29778007677574
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     47339.5509601826
GAINB   =      3.2338133205853
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     43881.5407484054
CRPIX1  =             209.3625 / Coordinate Reference axis 1
CRPIX2  =        230.270828125 / Coordinate Reference axis 2
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 0x0 4.010000' / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =                2000. / [yr] Equinox of WCS
PV1_7   =    0.000618368874624 / PV distortion coefficient
CUNIT1  = 'deg     '
PV2_8   =    -0.00842887692623 / PV distortion coefficient
PV2_9   =     0.00790302236959 / PV distortion coefficient
CD1_1   =  -1.1679624622720E-5 / World coordinate transformation matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =    -0.00441101222605 / PV distortion coefficient
PV2_1   =        1.01700592995 / PV distortion coefficient
PV2_2   =    -0.00863195988986 / PV distortion coefficient
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =     -0.0181936225912 / PV distortion coefficient
PV2_5   =      0.0168279891847 / PV distortion coefficient
PV2_6   =      -0.010365069688 / PV distortion coefficient
PV2_7   =     0.00641705596954 / PV distortion coefficient
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =       0.010153274645 / PV distortion coefficient
PV2_10  =    -0.00261077202228 / PV distortion coefficient
PV1_4   =     0.00862489761958 / PV distortion coefficient
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =    -0.00978790747587 / PV distortion coefficient
PV1_1   =        1.01430869337 / PV distortion coefficient
PV1_0   =     0.00392185213728 / PV distortion coefficient
PV1_9   =     0.00654393666949 / PV distortion coefficient
PV1_8   =    -0.00736713022181 / PV distortion coefficient
CD1_2   =  0.00466262971156481 / World coordinate transformation matrix
PV1_5   =      -0.017864081795 / PV distortion coefficient
CUNIT2  = 'deg     '
CD2_1   =  -0.0046623936290495 / World coordinate transformation matrix
CD2_2   =  -1.1659626630144E-5 / World coordinate transformation matrix
PV1_10  =    -0.00361076208606 / PV distortion coefficient
CTYPE2  = 'DEC--TPV'           / Coordinate type
CTYPE1  = 'RA---TPV'           / Coordinate type
CRVAL1  =              60.1695 / [deg] WCS Reference Coordinate (RA)
CRVAL2  =           -30.490916 / [deg] WCS Reference Coordinate (DEC)
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Mon Feb 15 11:47:43 2021' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Mon Feb 15 11:47:43 2021' / overscan corrected
BIASFIL = 'STARFLAT_20210128_99b9e9f-59242Z_01.fits' / Bias correction file
BAND    = 'N501    '
NITE    = '20210128'
DESBIAS = 'Mon Feb 15 12:03:23 2021'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Mon Feb 15 12:03:26 2021'
DESBPM  = 'Mon Feb 15 12:03:28 2021' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Mon Feb 15 12:03:28 2021' / Flag saturated pixels
NSATPIX =                   16 / Number of saturated pixels
FLATMEDA=     3.29778007677574
FLATMEDB=      3.2338133205853
SATURATE=     47339.5509601826
DESGAINC= 'Mon Feb 15 12:03:28 2021'
FLATFIL = 'STARFLAT_20210128_99b9e9f-59242N501F_01.fits' / Dome flat correction
DESFLAT = 'Mon Feb 15 12:03:28 2021'
STARFIL = 'STARFLAT_20210128_99bacc2-N501P_ci_01.fits' / Star flat correction fi
DESSTAR = 'Mon Feb 15 12:03:29 2021'
HISTORY Mon Feb 15 12:03:29 2021 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Mon Feb 15 12:42:01 2021' / bleed trail masking
NBLEED  = 112  / Number of bleed trail pixels.
STARMASK= 'Mon Feb 15 12:42:01 2021' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC21A_ODIN20210128_99
HISTORY ddfda-filN501-ccd01N501/DEC21A_ODIN20210128_99ddfda-N501-210129004245_01
HISTORY .fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Feb 15 12:42 Bad pixel file is omibleed_01_bpm.pl'
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             21.85797
NCOMBINE=                 9180
PUPILSKY=       22.49327137554
PUPILSUM=     111706.402058267
PUPILMAX=     2.44603681564331
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'PPghSNZePNfePNZe'   / HDU checksum updated 2021-02-15T21:36:46
DATASUM = '2688794094'         / data unit checksum updated 2021-02-15T21:36:46
NPUPSCL =                84270
PUPSCL  =     0.73135501800962
    Ț     Ą  Ì                     Ì  S        êÿÿÿ               ÿS       @2``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
 
@`
 
@`
 
@`
 
@`
 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@`
```````````````````````````` @` @ @ @ @ @` @ @ @` @ @``` @ @``````````````````````` @`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@`
 
@`
 
@`
 
@`
 
@`
`
`
`
`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@=   ÿ      Ą 2 ÿ      AĄ  ÿ 
   
  @À $@œ  ÿ 
   
  @ž 4@”  ÿ 
   
  @Č @@Ż  ÿ 
   
  @­ J@Ș  ÿ 
   
  @© R@Š  ÿ 
   
  @„ Z@ą  ÿ 
   
  @ą a@  ÿ 
   
  @ h@  ÿ 
   
  @ n@  ÿ 
   
  @ t@  ÿ 
   
  @ z@  ÿ 
   
  @ ~@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ą@~  ÿ 
   
  @ Š@|  ÿ 
   
  @~ š@{  ÿ 
   
  @| Ź@y  ÿ 
   
  @z °@w  ÿ 
   
  @x Ž@u  ÿ 
   
  @w ¶@t  ÿ 
   
  @u ș@r  ÿ 
   
  @t Œ@q  ÿ 
   
  @r À@o  ÿ 
   
  @q Â@n  ÿ 
   
  @o Æ@l  ÿ 
   
  @n È@k  ÿ 
   
  @l Ì@i  ÿ 
   
  @k Î@h  ÿ 
   
  @j Ń@f  ÿ 
   
  @h Ô@e  ÿ 
   
  @g Ö@d  ÿ 
   
  @f Ű@c  ÿ 
   
  @d Ü@a  ÿ 
   
  @c Ț@`  ÿ 
   
  @b à@_  ÿ 
   
  @a â@^  ÿ 
   
  @` ä@]  ÿ 
   
  @^ è@[  ÿ 
   
  @] ê@Z  ÿ 
   
  @\ ì@Y  ÿ 
   
  @[ î@X  ÿ 
   
  @Z đ@W  ÿ 
   
  @Y ò@V  ÿ 
   
  @X ô@U  ÿ 
   
  @W ö@T  ÿ 
   
  @V ű@S  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S ț@P  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@L  ÿ 
   
  @N@K  ÿ 
   
  @M
@J  ÿ 
   
  @L@I  ÿ 
   
  @K@H  ÿ 
   
  @J@G  ÿ 
   
  @I@F  ÿ 
   
  @H@E  ÿ 
   
  @G@D  ÿ 
   
  @G@C  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@A  ÿ 
   
  @C@@  ÿ 
   
  @B @?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?&@<  ÿ 
   
  @>(@;  ÿ 
   
  @=*@:  ÿ 
   
  @<,@9  ÿ 
   
  @;.@8  ÿ 
   
  @:0@7  ÿ 
   
  @92@6  ÿ 
   
  @84@5  ÿ 
   
  @76@4  ÿ 
   
  @68@3  ÿ 
   
  @5:@2  ÿ 
   
  @4<@1  ÿ 
   
  @3>@0  ÿ 
   
  @2@@/  ÿ 
   
  @1B@.  ÿ 
   
  @0D@-  ÿ 
   
  @/F@,  ÿ 
   
  @.H@+  ÿ 
   
  @-J@*  ÿ 
   
  @,L@)  ÿ 
   
  @+N@(  ÿ 
   
  @+O@'  ÿ 
   
  @*P@'  ÿ      @) Ą@ Ł@&  ÿ      @) @ @&  ÿ      @) @ @&  ÿ      @) @# @&  ÿ      @( @( @%  ÿ      @( @, @%  ÿ      @( @/ @%  ÿ      @( @2 @%  ÿ      @' @5 @$  ÿ      @' @8 @$  ÿ      @' @: @$  ÿ      @' @= @$  ÿ      @' @? @$  ÿ      @& @A @#  ÿ      @& @C @#  ÿ      @& @D @#  ÿ      @& @F @#  ÿ      @& @H @#  ÿ      @& @J @#  ÿ      @% @K @"  ÿ      @% @L @"  ÿ      @% @N @"  ÿ      @% @O @"  ÿ      @% @P @"  ÿ      @% @Q @"  ÿ      @% @R @"  ÿ      @% @S @"  ÿ      @$ @T @!  ÿ      @$ @U @!  ÿ      @$ @V @!  ÿ      @$ @W @!  ÿ      @$ @X @!  ÿ      @$ @Y @!  ÿ      @$ @Z @!  ÿ      @$ @[ @!  ÿ      @$ @\ @!  ÿ      @$ @\ @   ÿ      @$ @\ @!  ÿ      @$ @[ @!  ÿ      @$ @Z @!  ÿ      @$ @Y @!  ÿ      @$ @X @!  ÿ      @$ @V @!  ÿ      @$ @U @!  ÿ      @$ @T @!  ÿ      @% @T @"  ÿ      @% @R @"  ÿ      @% @P @"  ÿ      @% @N @"  ÿ      @% @M @"  ÿ      @% @L @"  ÿ      @& @J @#  ÿ      @& @I @#  ÿ      @& @H @#  ÿ      @& @F @#  ÿ      @& @D @#  ÿ      @& @B @#  ÿ      @' @@ @$  ÿ      @' @> @$  ÿ      @' @< @$  ÿ      @' @: @$  ÿ      @' @6 @$  ÿ      @( @4 @%  ÿ      @( @1 @%  ÿ      @( @. @%  ÿ      @( @* @%  ÿ      @) @& @&  ÿ      @) @! @&  ÿ      @) @ @&  ÿ      @) @  @&  ÿ      @* €@ §@'  ÿ 
   
  @*P@'  ÿ 
   
  @+O@'  ÿ 
   
  @+N@(  ÿ 
   
  @,L@)  ÿ 
   
  @-J@*  ÿ 
   
  @.H@+  ÿ 
   
  @/F@,  ÿ 
   
  @0D@-  ÿ 
   
  @1B@.  ÿ 
   
  @2@@/  ÿ 
   
  @3>@0  ÿ 
   
  @4<@1  ÿ 
   
  @5:@2  ÿ 
   
  @68@3  ÿ 
   
  @76@4  ÿ 
   
  @84@5  ÿ 
   
  @92@6  ÿ 
   
  @:0@7  ÿ 
   
  @;.@8  ÿ 
   
  @<,@9  ÿ 
   
  @=*@:  ÿ 
   
  @>(@;  ÿ 
   
  @?&@<  ÿ 
   
  @@$@=  ÿ 
   
  @A"@>  ÿ 
   
  @B @?  ÿ 
   
  @C@@  ÿ 
   
  @D@A  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @G@C  ÿ 
   
  @G@D  ÿ 
   
  @H@E  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@H  ÿ 
   
  @L@I  ÿ 
   
  @M
@J  ÿ 
   
  @N@K  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S ț@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ű@S  ÿ 
   
  @W ö@T  ÿ 
   
  @X ô@U  ÿ 
   
  @Y ò@V  ÿ 
   
  @Z đ@W  ÿ 
   
  @[ î@X  ÿ 
   
  @\ ì@Y  ÿ 
   
  @] ê@Z  ÿ 
   
  @^ è@[  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @b à@_  ÿ 
   
  @c Ț@`  ÿ 
   
  @d Ü@a  ÿ 
   
  @f Ű@c  ÿ 
   
  @g Ö@d  ÿ 
   
  @h Ô@e  ÿ 
   
  @j Ń@f  ÿ 
   
  @k Î@h  ÿ 
   
  @l Ì@i  ÿ 
   
  @n È@k  ÿ 
   
  @o Æ@l  ÿ 
   
  @q Â@n  ÿ 
   
  @r À@o  ÿ 
   
  @t Œ@q  ÿ 
   
  @u ș@r  ÿ 
   
  @w ¶@t  ÿ 
   
  @x Ž@u  ÿ 
   
  @z °@w  ÿ 
   
  @| Ź@y  ÿ 
   
  @~ š@{  ÿ 
   
  @ Š@|  ÿ 
   
  @ ą@~  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ~@  ÿ 
   
  @ z@  ÿ 
   
  @ t@  ÿ 
   
  @ n@  ÿ 
   
  @ h@  ÿ 
   
  @ą a@  ÿ 
   
  @„ Z@ą  ÿ 
   
  @© R@Š  ÿ 
   
  @­ J@Ș  ÿ 
   
  @Č @@Ż  ÿ 
   
  @ž 4@”  ÿ 
   
  @À $@œ = ÿ      AĄ