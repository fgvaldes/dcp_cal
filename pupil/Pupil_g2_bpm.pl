  �V  G�  
�$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1279369018
$MTIME = 1279369018
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-07-16T18:57:32' / Date FITS file was generated
IRAF-TLM= '2020-07-16T18:57:15' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'RAW     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  90. / [s] Requested exposure duration
EXPTIME =                  90. / [s] Exposure duration
EXPDUR  =                  90. / [s] Exposure duration
DARKTIME=             91.09551 / [s] Dark time
OBSID   = 'ct4m20131228t031151' / Unique Observation ID
DATE-OBS= '2013-12-28T03:11:51.145424' /  UTC epoch
TIME-OBS= '03:11:51.145424'    / Time of observation start (UTC)
MJD-OBS =       56654.13323085 / MJD of observation start
MJD-END =      56654.134272517 / Exposure end
OPENSHUT= '2013-12-28T03:11:51.182440' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               268800 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'survey  '           / Current observing orogram
OBSERVER= 'David James, Ting Li, Phillip Rooney' / Observer name(s)
PROPOSER= 'Frieman '           / Proposal Principle Investigator
DTPI    = 'Frieman           ' /  Principal Investigator
PROPID  = '2012B-0001'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
HEX     =                 2420 / DES Hex number
TILING  =                    4 / DES Tiling number
SEQID   = '387-24 enqueued on 2013-12-28 02:57:10Z by SurveyTactician' / Sequenc
SEQNUM  =                    1 / Number of image in sequence
SEQTOT  =                    1 / Total number of images in sequence
AOS     =                    T / AOS data available if true
BCAM    =                    T / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'g DECam SDSS c0001 4720.0 1520.0' / Unique filter identifier
FILTPOS = 'cassette_2'         / Filter position in FCM
INSTANCE= 'DECam_20131227'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '02:35:11.409'       / [HH:MM:SS] Target RA
DEC     = '-01:39:04.435'      / [DD:MM:SS] Target DEC
TELRA   = '02:35:11.418'       / [HH:MM:SS] Telescope RA
TELDEC  = '-01:39:06.800'      / [DD:MM:SS] Telescope DEC
HA      = '02:19:42.120'       / [HH:MM:SS] Telescope hour angle
ZD      =                 43.7 / [deg] Telescope zenith distance
AZ      =               304.18 / [deg] Telescope azimuth angle
DOMEAZ  =                 305. / [deg] Dome azimuth angle
TELFOCUS= '2441.94,-2767.29,2185.64,118.52,-0.38, 0.00' / DECam hexapod settings
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    T / RASICAM local sky clear flag
WINDSPD =                7.081 / [m/s] Wind speed
WINDDIR =                 179. / [deg] Wind direction (from North)
HUMIDITY=                  62. / [%] Ambient relative humidity (outside)
PRESSURE=                 780. / [Torr] Barometric pressure (outside)
DIMMSEE =                0.692 / [arcsec] DIMM Seeing
OUTTEMP =                 14.9 / [deg C] Outside temperature
AIRMASS =                 1.38 / Airmass
GSKYVAR =                 0.02 / RASICAM global sky standard deviation
GSKYHOT =                 0.01 / RASICAM global sky fraction above threshold
LSKYVAR =                   0. / RASICAM local sky standard deviation
LSKYHOT =                   0. / RASICAM local sky fraction above threshold
LSKYPOW =                   0. / RASICAM local sky normalized power
MSURTEMP=                13.75 / [deg C] Mirror surface average temperature
MAIRTEMP=                 14.1 / [deg C] Mirror temperature above surface
UPTRTEMP=               15.473 / [deg C] Upper truss average temperature
LWTRTEMP=                -999. / [deg C] Lower truss average temperature
PMOSTEMP=                 14.1 / [deg C] Mirror top surface temperature
UTN-TEMP=                15.55 / [deg C] Upper truss temperature north
UTS-TEMP=                15.43 / [deg C] Upper truss temperature south
UTW-TEMP=                15.43 / [deg C] Upper truss temperature west
UTE-TEMP=                15.48 / [deg C] Upper truss temperature east
PMN-TEMP=                 13.6 / [deg C] Mirror north edge temperature
PMS-TEMP=                 13.6 / [deg C] Mirror south edge temperature
PMW-TEMP=                 13.5 / [deg C] Mirror west edge temperature
PME-TEMP=                 14.3 / [deg C] Mirror east edge temperature
DOMELOW =                15.69 / [deg C] Low dome temperature
DOMEHIGH=                -999. / [deg C] High dome temperature
DOMEFLOR=                  9.1 / [deg C] Dome floor temperature
G-MEANX =                0.021 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0422 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[0.01,1.46,-8.91,-0.49,0.09,0.14,0.03,-0.06,-0.50,]' / Mean Wavefront
DONUTFS3= '[0.16,1.25,8.71,-0.33,0.20,0.20,0.05,0.10,-0.55,]' / Mean Wavefront f
DONUTFS2= '[1.03,-0.40,-8.82,-0.15,0.16,0.24,0.09,0.35,-0.17,]' / Mean Wavefront
DONUTFS1= '[1.20,-0.03,8.66,-0.19,0.32,0.46,0.04,0.24,0.04,]' / Mean Wavefront f
G-FLXVAR=           914144.068 / [arcsec] Guider mean guide star flux variances
G-MEANXY=             0.005293 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[-0.31,-0.17,-8.87,-0.67,-0.21,0.26,0.32,0.30,-0.34,]' / Mean Wavefro
DONUTFN2= '[1.25,0.16,8.62,-0.85,-0.33,0.28,0.33,0.18,-0.19,]' / Mean Wavefront
DONUTFN3= '[-0.22,1.51,-8.80,-0.22,-0.38,0.21,0.34,0.03,0.23,]' / Mean Wavefront
DONUTFN4= '[0.69,0.49,8.65,-0.03,-0.59,0.15,0.53,-0.05,0.10,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2013-12-28T03:13:44.085801'
G-FEEDBK=                   10 / [%] Guider feedback
G-CCDNUM=                    3 / Number of guide CCDs that remained active
DOXT    =                 0.09 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.5096 / [arcsec] Guider x-axis maximum offset
FADZ    =                 16.5 / [um] FA Delta focus.
FADY    =               118.13 / [um] FA Delta Y.
FADX    =               -98.39 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                -1.99 / [arcsec] FA Delta Y-theta.
DODZ    =                 16.5 / [um] Delta-Z from donut analysis
DODY    =                -0.48 / [um] Y-decenter from donut analysis
DODX    =                -0.53 / [um] X-decenter from donut analysis
BCAMAZ  =                   0. / [arcsec] BCAM hexapod rot. about z-axis
MULTIEXP=                    F / Frame contains multiple exposures if true
BCAMAX  =               91.411 / [arcsec] BCAM hexapod rot. about x-axis
BCAMAY  =              -67.987 / [arcsec] BCAM hexapod rot. about y-axis
BCAMDY  =            -6335.744 / [micron] BCAM hexapod y-offset
BCAMDX  =            -2679.502 / [micron] BCAM hexapod x-offset
SKYUPDAT= '2013-12-28T03:09:45' / Time of last RASICAM exposure (UTC)
G-SEEING=                2.036 / [arcsec] Guider average seeing
G-TRANSP=                 0.56 / Guider average sky transparency
G-MEANY2=             0.014322 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                 0.37 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.297 / [s] Guider avg. latency between exposures
LUTVER  = 'working-trunk'      / Hexapod Lookup Table version
FAXT    =                -3.96 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.2388 / [arcsec] Guider y-axis maximum offset
G-MEANX2=              0.03103 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:19'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTSITE  = 'ct                ' /  observatory location
DTTELESC= 'ct4m              ' /  telescope identifier
DTINSTRU= 'decam             ' /  instrument identifier
DTCALDAT= '2013-12-27        ' /  calendar date from observing schedule
ODATEOBS= '                  ' /  previous DATE-OBS
DTUTC   = '2013-12-28T03:14:13' /  post exposure UTC epoch from DTS
DTOBSERV= 'NOAO              ' /  scheduling institution
DTPROPID= '2012B-0001        ' /  observing proposal ID
DTPIAFFL= '                  ' /  PI affiliation
DTTITLE = '                  ' /  title of observing proposal
DTCOPYRI= 'AURA              ' /  copyright holder of data
DTACQUIS= 'pipeline5.ctio.noao.edu' /  host name of data acquisition computer
DTACCOUN= 'sispi             ' /  observing account name
DTACQNAM= '/data_local/images/DTS/2012B-0001/DECam_00268800.fits.fz' /  file na
DTNSANAM= 'c4d_131228_031413_ori.fits' /  file name in NOAO Science Archive
DT_RTNAM= 'c4d_131228_031413_ori' /  NSA root name
DTQUEUE = 'des               ' /  DTS queue (23351)
DTSTATUS= 'done              ' /  data transport status
SB_HOST = 'pipeline5.ctio.noao.edu' /  iSTB client host
SB_ACCOU= 'sispi             ' /  iSTB client user account
SB_SITE = 'ct                ' /  iSTB host site
SB_LOCAL= 'dec               ' /  locale of iSTB daemon
SB_DIR1 = '20131227          ' /  level 1 directory in NSA DS
SB_DIR2 = 'ct4m              ' /  level 2 directory in NSA DS
SB_DIR3 = '2012B-0001        ' /  level 3 directory in NSA DS
SB_RECNO=               137966 /  iSTB sequence number
SB_ID   = 'dec137966         ' /  unique iSTB identifier
SB_NAME = 'c4d_131228_031413_ori.fits' /  name assigned by iSTB
SB_RTNAM= 'c4d_131228_031413_ori' /  NSA root name
RMCOUNT =                    0 /  remediation counter
RECNO   =               137966 /  NOAO Science Archive sequence number
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.85481012708721
RDNOISEA=        6.67373068433 / [electrons] Read noise for amp A
SATURATA=     44269.1082041844
GAINB   =     3.94491845665264
RDNOISEB=        6.52878464819 / [electrons] Read noise for amp B
SATURATB=     40570.3971004829
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =   2.087520149181E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -1.015554463106E-02 / Projection distortion parameter
PV2_9   =   8.962633753844E-03 / Projection distortion parameter
CD1_1   =  -1.9897817938842E-7 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -3.118913118624E-03 / Projection distortion parameter
PV2_1   =   1.013896382096E+00 / Projection distortion parameter
PV2_2   =  -1.025197852159E-02 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -1.623566840769E-02 / Projection distortion parameter
PV2_5   =   2.022650928651E-02 / Projection distortion parameter
PV2_6   =  -1.121325330664E-02 / Projection distortion parameter
PV2_7   =   6.422711612000E-03 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   1.891452778858E-03 / Projection distortion parameter
PV2_10  =  -2.654354928400E-03 / Projection distortion parameter
PV1_4   =   7.834823949981E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -2.485766972051E-03 / Projection distortion parameter
PV1_1   =   1.014794267197E+00 / Projection distortion parameter
PV1_0   =   1.709501104213E-03 / Projection distortion parameter
PV1_9   =   7.872055622797E-03 / Projection distortion parameter
PV1_8   =  -7.273769983855E-03 / Projection distortion parameter
CD1_2   =  0.00466284244376257 / Linear projection matrix
PV1_5   =  -1.977298282348E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =   -0.004662631089097 / Linear projection matrix
CD2_2   =  -9.1732254452800E-7 / Linear projection matrix
PV1_10  =  -5.311615314207E-04 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   3.880065788329E+01 / World coordinate on this axis
CRVAL2  =  -1.649372388433E+00 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Wed Jul  3 12:27:21 2019' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Wed Jul  3 12:27:21 2019' / overscan corrected
BIASFIL = 'DECALS_20131227_8e4d9cc-56653Z_01.fits' / Bias correction file
BAND    = 'g       '
NITE    = '20131227'
DESBIAS = 'Wed Jul  3 12:59:08 2019'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Wed Jul  3 12:59:15 2019'
DESBPM  = 'Wed Jul  3 12:59:20 2019' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Wed Jul  3 12:59:20 2019' / Flag saturated pixels
NSATPIX =                   87 / Number of saturated pixels
FLATMEDA=     3.85481012708721
FLATMEDB=     3.94491845665264
SATURATE=     44269.1082041844
DESGAINC= 'Wed Jul  3 12:59:20 2019'
FLATFIL = 'STARFLAT_20131115_94b995c-56611gF_01.fits' / Dome flat correction fil
DESFLAT = 'Wed Jul  3 12:59:27 2019'
STARFIL = 'STARFLAT_20160816_94ae731-gP_ci_01.fits' / Star flat correction file
DESSTAR = 'Wed Jul  3 12:59:29 2019'
HISTORY Wed Jul  3 12:59:30 2019 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Wed Jul 3 13:34:15 2019' / bleed trail masking
NBLEED  = 708  / Number of bleed trail pixels.
STARMASK= 'Wed Jul 3 13:34:15 2019' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DECALS_DR9_94fb337-fil
HISTORY g-ccd01g/DECALS_DR9_94fb337-g-131228031151_01.fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FWHM    =             5.146984 / Median FWHM in pixels
ELLIPTIC=            0.2054638 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Wed Jul  3 13:52:02 2019' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
PHOTREF = 'PS1     '           / Photometric ref. catalog
MAGZERO =               29.870 / [mag] Magnitude zero point
MAGZPT  =               24.984 / [mag] Magnitude zero point per sec
NPHTMTCH=                 2818 / Number of phot. stars matched
RADIUS  =                 2.42 / [pix] Average radius of stars
RADSTD  =                0.069 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             112.9295
NCOMBINE=                59005
PUPILSKY=     114.169730970494
PUPILSUM=     140662.201359625
PUPILMAX=     3.73072242736816
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'UXMXaUMUWUMUaUMU'   / HDU checksum updated 2020-07-16T17:05:42
DATASUM = '704525813'          / data unit checksum updated 2020-07-16T17:05:42
NPUPSCL =                85748
PUPSCL  =      0.7379839606566
   �     �  �                 	P  	P  �  4  
�      ����               ��4       @2``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
 
@`
 
@`
 
@`
 
@`
 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@`
``````` @ @ @ @` @ @ @ @```````` @ 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@`
 
@`
 
@`
 
@`
 
@`
`
`
`
`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@=   ��      � 2 ��      A�  �� 
   
  @� $@�  �� 
   
  @� 4@�  �� 
   
  @� @@�  �� 
   
  @� J@�  �� 
   
  @� R@�  �� 
   
  @� Z@�  �� 
   
  @� a@�  �� 
   
  @� h@�  �� 
   
  @� n@�  �� 
   
  @� t@�  �� 
   
  @� z@�  �� 
   
  @� ~@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @~ �@�  �� 
   
  @| �@  �� 
   
  @{ �@~  �� 
   
  @y �@|  �� 
   
  @w �@z  �� 
   
  @u �@x  �� 
   
  @t �@w  �� 
   
  @r �@u  �� 
   
  @q �@t  �� 
   
  @o �@r  �� 
   
  @n �@q  �� 
   
  @l �@o  �� 
   
  @k �@n  �� 
   
  @i �@l  �� 
   
  @h �@k  �� 
   
  @g �@i  �� 
   
  @e �@h  �� 
   
  @d �@g  �� 
   
  @c �@f  �� 
   
  @a �@d  �� 
   
  @` �@c  �� 
   
  @_ �@b  �� 
   
  @^ �@a  �� 
   
  @] �@`  �� 
   
  @[ �@^  �� 
   
  @Z �@]  �� 
   
  @Y �@\  �� 
   
  @X �@[  �� 
   
  @W �@Z  �� 
   
  @V �@Y  �� 
   
  @U �@X  �� 
   
  @T �@W  �� 
   
  @S �@V  �� 
   
  @R �@U  �� 
   
  @Q �@T  �� 
   
  @P �@S  �� 
   
  @O @R  �� 
   
  @N@Q  �� 
   
  @M@P  �� 
   
  @L@O  �� 
   
  @K@N  �� 
   
  @J
@M  �� 
   
  @I@L  �� 
   
  @H@K  �� 
   
  @G@J  �� 
   
  @F@I  �� 
   
  @E@H  �� 
   
  @D@G  �� 
   
  @D@F  �� 
   
  @C@F  �� 
   
  @B@E  �� 
   
  @A@D  �� 
   
  @@@C  �� 
   
  @? @B  �� 
   
  @>"@A  �� 
   
  @=$@@  �� 
   
  @<&@?  �� 
   
  @;(@>  �� 
   
  @:*@=  �� 
   
  @9,@<  �� 
   
  @8.@;  �� 
   
  @70@:  �� 
   
  @62@9  �� 
   
  @54@8  �� 
   
  @46@7  �� 
   
  @38@6  �� 
   
  @2:@5  �� 
   
  @1<@4  �� 
   
  @0>@3  �� 
   
  @/@@2  �� 
   
  @.B@1  �� 
   
  @-D@0  �� 
   
  @,F@/  �� 
   
  @+H@.  �� 
   
  @*J@-  �� 
   
  @)L@,  �� 
   
  @(N@+  �� 
   
  @(O@*  �� 
   
  @'P@*  �� 
   
  @&R@)  �� 
   
  @%T@(  �� 
   
  @$V@'  �� 
   
  @#X@&  �� 
   
  @"Z@%  ��      @" �@ �@%  ��      @" �@ �@%  ��      @! �@ �@$  ��      @! �@ �@$  ��      @! �@ �@$  ��      @! �@ �@$  ��      @! �@  �@$  ��      @! �@" �@$  ��      @! �@$ �@$  ��      @! �@& �@$  ��      @! �@( �@$  ��      @! �@* �@$  ��      @! �@+ �@#  ��      @! �@* �@$  ��      @! �@( �@$  ��      @! �@& �@$  ��      @! �@$ �@$  ��      @! �@" �@$  ��      @! �@  �@$  ��      @! �@ �@$  ��      @! �@ �@$  ��      @! �@ �@$  ��      @! �@ �@$  ��      @" �@ �@%  ��      @" �@ �@%  �� 
   
  @"Z@%  �� 
   
  @#X@&  �� 
   
  @$V@'  �� 
   
  @%T@(  �� 
   
  @&R@)  �� 
   
  @'P@*  �� 
   
  @(O@*  �� 
   
  @(N@+  �� 
   
  @)L@,  �� 
   
  @*J@-  �� 
   
  @+H@.  �� 
   
  @,F@/  �� 
   
  @-D@0  �� 
   
  @.B@1  �� 
   
  @/@@2  �� 
   
  @0>@3  �� 
   
  @1<@4  �� 
   
  @2:@5  �� 
   
  @38@6  �� 
   
  @46@7  �� 
   
  @54@8  �� 
   
  @62@9  �� 
   
  @70@:  �� 
   
  @8.@;  �� 
   
  @9,@<  �� 
   
  @:*@=  �� 
   
  @;(@>  �� 
   
  @<&@?  �� 
   
  @=$@@  �� 
   
  @>"@A  �� 
   
  @? @B  �� 
   
  @@@C  �� 
   
  @A@D  �� 
   
  @B@E  �� 
   
  @C@F  �� 
   
  @D@F  �� 
   
  @D@G  �� 
   
  @E@H  �� 
   
  @F@I  �� 
   
  @G@J  �� 
   
  @H@K  �� 
   
  @I@L  �� 
   
  @J
@M  �� 
   
  @K@N  �� 
   
  @L@O  �� 
   
  @M@P  �� 
   
  @N@Q  �� 
   
  @O @R  �� 
   
  @P �@S  �� 
   
  @Q �@T  �� 
   
  @R �@U  �� 
   
  @S �@V  �� 
   
  @T �@W  �� 
   
  @U �@X  �� 
   
  @V �@Y  �� 
   
  @W �@Z  �� 
   
  @X �@[  �� 
   
  @Y �@\  �� 
   
  @Z �@]  �� 
   
  @[ �@^  �� 
   
  @] �@`  �� 
   
  @^ �@a  �� 
   
  @_ �@b  �� 
   
  @` �@c  �� 
   
  @a �@d  �� 
   
  @c �@f  �� 
   
  @d �@g  �� 
   
  @e �@h  �� 
   
  @g �@i  �� 
   
  @h �@k  �� 
   
  @i �@l  �� 
   
  @k �@n  �� 
   
  @l �@o  �� 
   
  @n �@q  �� 
   
  @o �@r  �� 
   
  @q �@t  �� 
   
  @r �@u  �� 
   
  @t �@w  �� 
   
  @u �@x  �� 
   
  @w �@z  �� 
   
  @y �@|  �� 
   
  @{ �@~  �� 
   
  @| �@  �� 
   
  @~ �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� ~@�  �� 
   
  @� z@�  �� 
   
  @� t@�  �� 
   
  @� n@�  �� 
   
  @� h@�  �� 
   
  @� a@�  �� 
   
  @� Z@�  �� 
   
  @� R@�  �� 
   
  @� J@�  �� 
   
  @� @@�  �� 
   
  @� 4@�  �� 
   
  @� $@� = ��      A�