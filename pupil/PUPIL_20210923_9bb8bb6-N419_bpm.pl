  �V  =h  �$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1317461433
$MTIME = 1317461433
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2021-09-30T16:30:33' / Date FITS file was generated
IRAF-TLM= '2021-09-30T16:30:32' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  60. / [s] Requested exposure duration
EXPTIME =                  60. / [s] Exposure duration
EXPDUR  =                  60. / [s] Exposure duration
DARKTIME=           61.2814701 / [s] Dark time
OBSID   = 'ct4m20210923t235651' / Unique Observation ID
DATE-OBS= '2021-09-23T23:56:51.179597' / DateTime of observation start (UTC)
TIME-OBS= '23:56:51.179597'    / Time of observation start (UTC)
MJD-OBS =       59480.99781458 / MJD of observation start
MJD-END =      59480.998509024 / Exposure end
OPENSHUT= '2021-09-23T23:56:51.403420' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =              1036631 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'DECam Engineering'  / Current observing orogram
OBSERVER= 'Kathy Vivas'        / Observer name(s)
PROPOSER= 'Walker  '           / Proposal Principle Investigator
DTPI    = 'Walker  '           / Proposal Principle Investigator (iSTB)
PROPID  = '2013A-9999'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
SEQID   = '3 exposures'        / Sequence name
SEQNUM  =                    1 / Number of image in sequence
SEQTOT  =                    1 / Total number of images in sequence
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'N419 DECam c0013 4194.0 75.0' / Unique filter identifier
INSTANCE= 'DECam_20210923'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '14:29:15.000'       / [HH:MM:SS] Target RA
DEC     = '-15:09:05.000'      / [DD:MM:SS] Target DEC
TELRA   = '19:16:29.849'       / [HH:MM:SS] Telescope RA
TELDEC  = '-30:13:12.097'      / [DD:MM:SS] Telescope DEC
HA      = '00:07:54.280'       / [HH:MM:SS] Telescope hour angle
ZD      =                 1.63 / [deg] Telescope zenith distance
AZ      =             268.9499 / [deg] Telescope azimuth angle
DOMEAZ  =              267.439 / [deg] Dome azimuth angle
ZPDELRA =            -160.1634 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                -27.9 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-978.16,1067.96,2817.20,-230.19,-139.56,-0.00' / DECam hexapod settin
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    T / RASICAM local sky clear flag
WINDSPD =               12.875 / [m/s] Wind speed
WINDDIR =                  35. / [deg] Wind direction (from North)
HUMIDITY=                  15. / [%] Ambient relative humidity (outside)
PRESSURE=                 779. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                1.158 / [arcsec] DIMM2 Seeing
MASS2   =                 0.59 /  MASS(2) FSEE
ASTIG1  =                 0.01 / 4MAPS correction 1
ASTIG2  =                 0.14 / 4MAPS correction 2
OUTTEMP =                 14.6 / [deg C] Outside temperature
AIRMASS =                   1. / Airmass
GSKYVAR =                0.025 / RASICAM global sky standard deviation
GSKYHOT =                0.045 / RASICAM global sky fraction above threshold
LSKYVAR =                   0. / RASICAM local sky standard deviation
LSKYHOT =                   0. / RASICAM local sky fraction above threshold
LSKYPOW =                   0. / RASICAM local sky normalized power
MSURTEMP=                13.05 / [deg C] Mirror surface average temperature
MAIRTEMP=                 13.6 / [deg C] Mirror temperature above surface
UPTRTEMP=               14.069 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 11.2 / [deg C] Mirror top surface temperature
UTN-TEMP=               14.135 / [deg C] Upper truss temperature north
UTS-TEMP=                14.02 / [deg C] Upper truss temperature south
UTW-TEMP=               14.265 / [deg C] Upper truss temperature west
UTE-TEMP=               13.855 / [deg C] Upper truss temperature east
PMN-TEMP=                13.25 / [deg C] Mirror north edge temperature
PMS-TEMP=                 13.6 / [deg C] Mirror south edge temperature
PMW-TEMP=                 11.5 / [deg C] Mirror west edge temperature
PME-TEMP=                13.85 / [deg C] Mirror east edge temperature
DOMELOW =                14.09 / [deg C] Low dome temperature
DOMEHIGH=                 15.2 / [deg C] High dome temperature
DOMEFLOR=                  9.4 / [deg C] Dome floor temperature
G-MEANX =              -0.0403 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0547 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.94,2.06,-8.21,-0.13,-0.22,-0.24,-0.37,0.02,-0.38,]' / Mean Wavefro
DONUTFS3= '[]      '           / Mean Wavefront for Sensor FS3
DONUTFS2= '[1.66,0.54,-8.25,0.25,-0.14,-0.03,-0.24,0.28,-0.03,]' / Mean Wavefron
DONUTFS1= '[1.40,0.96,8.92,0.32,0.33,0.14,-0.22,0.26,0.09,]' / Mean Wavefront fo
G-FLXVAR=           223561.335 / [arcsec] Guider mean guide star flux variances
G-MEANXY=              7.6E-05 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[0.15,0.21,-8.51,-0.26,-0.25,0.05,0.17,0.11,-0.19,]' / Mean Wavefront
DONUTFN2= '[0.09,1.66,9.01,-0.31,0.07,0.07,0.23,0.23,-0.14,]' / Mean Wavefront f
DONUTFN3= '[1.38,1.22,-8.35,0.10,-0.24,-0.16,0.30,0.06,0.18,]' / Mean Wavefront
DONUTFN4= '[1.25,1.10,8.99,0.17,0.02,-0.16,0.31,-0.06,0.15,]' / Mean Wavefront f
HIERARCH TIME_RECORDED = '2021-09-23T23:58:13.116820'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    2 / Number of guide CCDs that remained active
DOXT    =                 0.06 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.1318 / [arcsec] Guider x-axis maximum offset
FADZ    =               -56.51 / [um] FA Delta focus.
FADY    =              -179.02 / [um] FA Delta Y.
FADX    =                743.5 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                 9.36 / [arcsec] FA Delta Y-theta.
DODZ    =               -56.51 / [um] Delta-Z from donut analysis
DODY    =                -1.12 / [um] Y-decenter from donut analysis
DODX    =                -1.11 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2021-09-23T23:49:59' / Time of last RASICAM exposure (UTC)
G-SEEING=                 1.88 / [arcsec] Guider average seeing
G-TRANSP=                0.746 / Guider average sky transparency
G-MEANY2=             0.010842 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.02 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.563 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                30.46 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.3212 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.008242 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:93'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTNSANAM= 'c4d_210923_235651_ori.fits.fz'
ODATEOBS= '2021-09-23T23:56:51.179597'
DTINSTRU= 'decam   '
DTACQNAM= '/data_local/images/DTS/2013A-9999/DECam_01036631.fits.fz'
DTPROPID= '2013A-9999'
DTCALDAT= '2021-09-23'
DTSITE  = 'ct      '
DTTELESC= 'ct4m    '
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.53683505504847
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     44139.8667368334
GAINB   =     3.50860068863668
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     40444.8164932496
CRPIX1  =             209.3625 / Coordinate Reference axis 1
CRPIX2  =        230.270828125 / Coordinate Reference axis 2
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =                2000. / [yr] Equinox of WCS
PV1_7   =    0.000618368874624 / PV distortion coefficient
CUNIT1  = 'deg     '
PV2_8   =    -0.00842887692623 / PV distortion coefficient
PV2_9   =     0.00790302236959 / PV distortion coefficient
CD1_1   =  -1.1679624622720E-5 / World coordinate transformation matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =    -0.00441101222605 / PV distortion coefficient
PV2_1   =        1.01700592995 / PV distortion coefficient
PV2_2   =    -0.00863195988986 / PV distortion coefficient
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =     -0.0181936225912 / PV distortion coefficient
PV2_5   =      0.0168279891847 / PV distortion coefficient
PV2_6   =      -0.010365069688 / PV distortion coefficient
PV2_7   =     0.00641705596954 / PV distortion coefficient
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =       0.010153274645 / PV distortion coefficient
PV2_10  =    -0.00261077202228 / PV distortion coefficient
PV1_4   =     0.00862489761958 / PV distortion coefficient
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =    -0.00978790747587 / PV distortion coefficient
PV1_1   =        1.01430869337 / PV distortion coefficient
PV1_0   =     0.00392185213728 / PV distortion coefficient
PV1_9   =     0.00654393666949 / PV distortion coefficient
PV1_8   =    -0.00736713022181 / PV distortion coefficient
CD1_2   =  0.00466262971156481 / World coordinate transformation matrix
PV1_5   =      -0.017864081795 / PV distortion coefficient
CUNIT2  = 'deg     '
CD2_1   =  -0.0046623936290495 / World coordinate transformation matrix
CD2_2   =  -1.1659626630144E-5 / World coordinate transformation matrix
PV1_10  =    -0.00361076208606 / PV distortion coefficient
CTYPE2  = 'DEC--TPV'           / Coordinate type
CTYPE1  = 'RA---TPV'           / Coordinate type
CRVAL1  =           289.124371 / [deg] WCS Reference Coordinate (RA)
CRVAL2  =           -30.220027 / [deg] WCS Reference Coordinate (DEC)
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Tue Sep 28 13:06:19 2021' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Tue Sep 28 13:06:19 2021' / overscan corrected
BIASFIL = 'CAL21B_20210923_9baf36b-59480Z_01.fits' / Bias correction file
BAND    = 'N419    '
NITE    = '20210923'
DESBIAS = 'Tue Sep 28 13:23:19 2021'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Tue Sep 28 13:23:20 2021'
DESBPM  = 'Tue Sep 28 13:23:20 2021' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Tue Sep 28 13:23:20 2021' / Flag saturated pixels
NSATPIX =                  578 / Number of saturated pixels
FLATMEDA=     3.53683505504847
FLATMEDB=     3.50860068863668
SATURATE=     44139.8667368334
DESGAINC= 'Tue Sep 28 13:23:20 2021'
FLATFIL = 'CAL21B_20210925_9bb6475-59482N419F_01.fits' / Dome flat correction fi
DESFLAT = 'Tue Sep 28 13:23:20 2021'
STARFIL = 'STARFLAT_20210923_9bb69e1-N419P_ci_01.fits' / Star flat correction fi
DESSTAR = 'Tue Sep 28 13:23:20 2021'
HISTORY Tue Sep 28 13:23:20 2021 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Tue Sep 28 14:02:57 2021' / bleed trail masking
NBLEED  = 324  / Number of bleed trail pixels.
STARMASK= 'Tue Sep 28 14:02:57 2021' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/PUPIL_20210923_9bb8bb6
HISTORY -filN419-ccd01N419/PUPIL_20210923_9bb8bb6-N419-210923235651_01.fits omib
HISTORY leed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Sep 28 14:03 Bad pixel file is omibleed_01_bpm.pl'
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             3.269361
NCOMBINE=                16440
PUPILSKY=     3.50583958802382
PUPILSUM=     15106.5830764239
PUPILMAX=    0.232910096645355
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'gXSbjXSZgXSagXSY'   / HDU checksum updated 2021-09-29T00:12:36
DATASUM = '2606571225'         / data unit checksum updated 2021-09-29T00:12:36
NPUPSCL =               106196
PUPSCL  =      7.5285614801152
   �     �  �                 
D  
D  �  P  �      ����               ��P       @&``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
 
@`
`
 
@`
`
 
@`
`
 
@`
 
@`
 
@ 
@`
 
@ 
@ 
@`
`
`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
````````` @`` @ @ @ @ @` @ @ @ @ @`` @`````````` @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
`
`
 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
`
 
@`
`
 
@`
`
 
@`
`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@1   ��      � & ��      A�  �� 
   
  @� &@�  �� 
   
  @� 6@�  �� 
   
  @� B@�  �� 
   
  @� L@�  �� 
   
  @� T@�  �� 
   
  @� \@�  �� 
   
  @� d@�  �� 
   
  @� l@�  �� 
   
  @� r@�  �� 
   
  @� x@�  �� 
   
  @� ~@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@  �� 
   
  @� �@}  �� 
   
  @~ �@{  �� 
   
  @| �@y  �� 
   
  @{ �@w  �� 
   
  @y �@v  �� 
   
  @w �@t  �� 
   
  @u �@r  �� 
   
  @s �@p  �� 
   
  @r �@o  �� 
   
  @p �@m  �� 
   
  @n �@k  �� 
   
  @m �@j  �� 
   
  @k �@h  �� 
   
  @j �@g  �� 
   
  @h �@e  �� 
   
  @g �@d  �� 
   
  @f �@c  �� 
   
  @d �@a  �� 
   
  @c �@`  �� 
   
  @b �@_  �� 
   
  @` �@]  �� 
   
  @_ �@\  �� 
   
  @^ �@[  �� 
   
  @\ �@Y  �� 
   
  @[ �@X  �� 
   
  @Z �@W  �� 
   
  @Y �@V  �� 
   
  @X �@U  �� 
   
  @W �@T  �� 
   
  @U �@R  �� 
   
  @T �@Q  �� 
   
  @S �@P  �� 
   
  @R @O  �� 
   
  @Q@N  �� 
   
  @P@M  �� 
   
  @O@L  �� 
   
  @N@K  �� 
   
  @M
@J  �� 
   
  @L@I  �� 
   
  @K@H  �� 
   
  @J@G  �� 
   
  @I@F  �� 
   
  @H@E  �� 
   
  @G@D  �� 
   
  @F@C  �� 
   
  @E@B  �� 
   
  @D@A  �� 
   
  @C@@  �� 
   
  @B @?  �� 
   
  @A"@>  �� 
   
  @@$@=  �� 
   
  @?&@<  �� 
   
  @>(@;  �� 
   
  @=*@:  �� 
   
  @<,@9  �� 
   
  @;.@8  �� 
   
  @:0@7  �� 
   
  @92@6  �� 
   
  @84@5  �� 
   
  @76@4  �� 
   
  @68@3  �� 
   
  @5:@2  �� 
   
  @4<@1  �� 
   
  @3>@0  �� 
   
  @2@@/  �� 
   
  @1B@.  �� 
   
  @0D@-  �� 
   
  @/F@,  �� 
   
  @.H@+  �� 
   
  @.I@*  �� 
   
  @-J@*  �� 
   
  @,L@)  �� 
   
  @+N@(  �� 
   
  @*P@'  �� 
   
  @)R@&  �� 
   
  @(T@%  �� 
   
  @'V@$  �� 
   
  @&X@#  �� 
   
  @%Z@"  �� 
   
  @$\@!  �� 
   
  @#^@   �� 
   
  @"`@  �� 
   
  @!b@  �� 
   
  @ d@  �� 
   
  @f@  �� 
   
  @h@  �� 
   
  @j@  �� 
   
  @l@  �� 
   
  @n@  �� 
   
  @p@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@  �@  ��      @ �@$ �@  ��      @ �@& �@  ��      @ �@( �@  ��      @ �@, �@  ��      @ �@. �@  ��      @ �@0 �@  ��      @ �@2 �@  ��      @ �@4 �@  ��      @ �@6 �@  ��      @ �@8 �@  ��      @ �@: �@  ��      @ �@< �@  ��      @ �@> �@  ��      @ �@@ �@  ��      @ �@A �@  ��      @ �@@ �@  ��      @ �@> �@  ��      @ �@< �@  ��      @ �@: �@  ��      @ �@8 �@  ��      @ �@6 �@  ��      @ �@4 �@  ��      @ �@2 �@  ��      @ �@0 �@  ��      @ �@. �@  ��      @ �@, �@  ��      @ �@( �@  ��      @ �@& �@  ��      @ �@$ �@  ��      @ �@  �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  �� 
   
  @p@  �� 
   
  @n@  �� 
   
  @l@  �� 
   
  @j@  �� 
   
  @h@  �� 
   
  @f@  �� 
   
  @ d@  �� 
   
  @!b@  �� 
   
  @"`@  �� 
   
  @#^@   �� 
   
  @$\@!  �� 
   
  @%Z@"  �� 
   
  @&X@#  �� 
   
  @'V@$  �� 
   
  @(T@%  �� 
   
  @)R@&  �� 
   
  @*P@'  �� 
   
  @+N@(  �� 
   
  @,L@)  �� 
   
  @-J@*  �� 
   
  @.I@*  �� 
   
  @.H@+  �� 
   
  @/F@,  �� 
   
  @0D@-  �� 
   
  @1B@.  �� 
   
  @2@@/  �� 
   
  @3>@0  �� 
   
  @4<@1  �� 
   
  @5:@2  �� 
   
  @68@3  �� 
   
  @76@4  �� 
   
  @84@5  �� 
   
  @92@6  �� 
   
  @:0@7  �� 
   
  @;.@8  �� 
   
  @<,@9  �� 
   
  @=*@:  �� 
   
  @>(@;  �� 
   
  @?&@<  �� 
   
  @@$@=  �� 
   
  @A"@>  �� 
   
  @B @?  �� 
   
  @C@@  �� 
   
  @D@A  �� 
   
  @E@B  �� 
   
  @F@C  �� 
   
  @G@D  �� 
   
  @H@E  �� 
   
  @I@F  �� 
   
  @J@G  �� 
   
  @K@H  �� 
   
  @L@I  �� 
   
  @M
@J  �� 
   
  @N@K  �� 
   
  @O@L  �� 
   
  @P@M  �� 
   
  @Q@N  �� 
   
  @R @O  �� 
   
  @S �@P  �� 
   
  @T �@Q  �� 
   
  @U �@R  �� 
   
  @W �@T  �� 
   
  @X �@U  �� 
   
  @Y �@V  �� 
   
  @Z �@W  �� 
   
  @[ �@X  �� 
   
  @\ �@Y  �� 
   
  @^ �@[  �� 
   
  @_ �@\  �� 
   
  @` �@]  �� 
   
  @b �@_  �� 
   
  @c �@`  �� 
   
  @d �@a  �� 
   
  @f �@c  �� 
   
  @g �@d  �� 
   
  @h �@e  �� 
   
  @j �@g  �� 
   
  @k �@h  �� 
   
  @m �@j  �� 
   
  @n �@k  �� 
   
  @p �@m  �� 
   
  @r �@o  �� 
   
  @s �@p  �� 
   
  @u �@r  �� 
   
  @w �@t  �� 
   
  @y �@v  �� 
   
  @{ �@w  �� 
   
  @| �@y  �� 
   
  @~ �@{  �� 
   
  @� �@}  �� 
   
  @� �@  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� ~@�  �� 
   
  @� x@�  �� 
   
  @� r@�  �� 
   
  @� l@�  �� 
   
  @� d@�  �� 
   
  @� \@�  �� 
   
  @� T@�  �� 
   
  @� L@�  �� 
   
  @� B@�  �� 
   
  @� 6@�  �� 
   
  @� &@� 1 ��      A�