  ŠV  @s  8$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1288877611
$MTIME = 1288877611
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-11-03T20:33:31' / Date FITS file was generated
IRAF-TLM= '2020-11-03T20:33:20' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  30. / [s] Requested exposure duration
EXPTIME =                  30. / [s] Exposure duration
EXPDUR  =                  30. / [s] Exposure duration
DARKTIME=           31.2258799 / [s] Dark time
OBSID   = 'ct4m20201011t024840' / Unique Observation ID
DATE-OBS= '2020-10-11T02:48:40.351938' / DateTime of observation start (UTC)
TIME-OBS= '02:48:40.351938'    / Time of observation start (UTC)
MJD-OBS =        59133.1171337 / MJD of observation start
MJD-END =      59133.117480922 / Exposure end
OPENSHUT= '2020-10-11T02:48:40.516860' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               944671 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'DECam Engineering'  / Current observing orogram
OBSERVER= 'Kathy Vivas'        / Observer name(s)
PROPOSER= 'Walker  '           / Proposal Principle Investigator
DTPI    = 'Walker  '           / Proposal Principle Investigator (iSTB)
PROPID  = '2013A-9999'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'VR DECam c0007 6300.0 2600.0' / Unique filter identifier
INSTANCE= 'DECam_20201010'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '20:39:46.181'       / [HH:MM:SS] Target RA
DEC     = '-35:01:40.001'      / [DD:MM:SS] Target DEC
TELRA   = '20:39:46.357'       / [HH:MM:SS] Telescope RA
TELDEC  = '-35:01:40.397'      / [DD:MM:SS] Telescope DEC
HA      = '02:44:47.850'       / [HH:MM:SS] Telescope hour angle
ZD      =                34.76 / [deg] Telescope zenith distance
AZ      =             251.0425 / [deg] Telescope azimuth angle
DOMEAZ  =               249.78 / [deg] Dome azimuth angle
ZPDELRA =             -23.6871 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                  8.9 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '2012.12,1592.94,2429.73,-152.99,38.15,-0.00' / DECam hexapod settings
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =                 10.3 / [m/s] Wind speed
WINDDIR =                 331. / [deg] Wind direction (from North)
HUMIDITY=                  18. / [%] Ambient relative humidity (outside)
PRESSURE=                 780. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE= 'NaN     '           / [arcsec] DIMM2 Seeing
MASS2   = 'NaN     '           /  MASS(2) FSEE
ASTIG1  =                -0.07 / 4MAPS correction 1
ASTIG2  =                -0.04 / 4MAPS correction 2
OUTTEMP =                 15.6 / [deg C] Outside temperature
AIRMASS =                 1.22 / Airmass
GSKYVAR =                0.043 / RASICAM global sky standard deviation
GSKYHOT =                0.953 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               14.588 / [deg C] Mirror surface average temperature
MAIRTEMP=                 15.1 / [deg C] Mirror temperature above surface
UPTRTEMP=                14.69 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 13.7 / [deg C] Mirror top surface temperature
UTN-TEMP=                14.52 / [deg C] Upper truss temperature north
UTS-TEMP=                14.79 / [deg C] Upper truss temperature south
UTW-TEMP=               14.955 / [deg C] Upper truss temperature west
UTE-TEMP=               14.495 / [deg C] Upper truss temperature east
PMN-TEMP=                14.55 / [deg C] Mirror north edge temperature
PMS-TEMP=                 15.1 / [deg C] Mirror south edge temperature
PMW-TEMP=                 13.6 / [deg C] Mirror west edge temperature
PME-TEMP=                 15.1 / [deg C] Mirror east edge temperature
DOMELOW =               15.265 / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                  9.4 / [deg C] Dome floor temperature
G-MEANX =              -0.0516 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0297 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.33,1.72,-9.12,-0.13,-0.11,-0.10,0.02,-0.03,-0.38,]' / Mean Wavefro
DONUTFS3= '[0.37,1.16,8.42,0.08,-0.23,0.14,-0.27,-0.02,-0.30,]' / Mean Wavefront
DONUTFS2= '[0.86,0.40,-9.10,0.29,0.02,-0.15,0.05,0.25,-0.02,]' / Mean Wavefront
DONUTFS1= '[2.01,0.90,8.47,0.31,0.06,0.35,-0.19,0.17,0.21,]' / Mean Wavefront fo
G-FLXVAR=         17340910.525 / [arcsec] Guider mean guide star flux variances
G-MEANXY=             0.002671 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[0.59,0.94,-9.13,-0.33,0.01,-0.11,-0.16,0.19,-0.22,]' / Mean Wavefron
DONUTFN2= '[0.84,1.43,8.54,-0.35,-0.08,0.32,0.09,0.15,-0.06,]' / Mean Wavefront
DONUTFN3= '[0.66,1.18,-8.97,-0.02,-0.23,-0.12,-0.20,0.04,0.27,]' / Mean Wavefron
DONUTFN4= '[1.86,1.81,8.51,0.12,-0.43,0.09,0.02,-0.11,0.20,]' / Mean Wavefront f
HIERARCH TIME_RECORDED = '2020-10-11T02:49:32.110700'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    3 / Number of guide CCDs that remained active
DOXT    =                 0.12 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.1234 / [arcsec] Guider x-axis maximum offset
FADZ    =                51.62 / [um] FA Delta focus.
FADY    =                 7.03 / [um] FA Delta Y.
FADX    =              -111.08 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =               -14.36 / [arcsec] FA Delta Y-theta.
DODZ    =                51.62 / [um] Delta-Z from donut analysis
DODY    =                -1.06 / [um] Y-decenter from donut analysis
DODX    =                -1.19 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2020-10-11T02:48:19' / Time of last RASICAM exposure (UTC)
G-SEEING=                2.682 / [arcsec] Guider average seeing
G-TRANSP=                0.541 / Guider average sky transparency
G-MEANY2=             0.016475 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                   0. / [arcsec] Y-theta from donut analysis
G-LATENC=                1.283 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                -3.02 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.2406 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.006806 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:86'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTPROPID= '2013A-9999'
DTCALDAT= '2020-10-10'
DTSITE  = 'ct      '
DTTELESC= 'ct4m    '
DTACQNAM= '/data_local/images/DTS/2013A-9999/DECam_00944671.fits.fz'
DTINSTRU= 'decam   '
ODATEOBS= '2020-10-11T02:48:40.351938'
DTNSANAM= 'c4d_201011_024840_ori.fits.fz'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.47656436552582
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     44905.0877780564
GAINB   =     3.42507125003395
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     41431.1705190347
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =  -5.024944166180E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -7.518309987306E-03 / Projection distortion parameter
PV2_9   =   7.645285959324E-03 / Projection distortion parameter
CD1_1   =  -1.6555734130566E-5 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -1.103490478215E-02 / Projection distortion parameter
PV2_1   =   1.041072708052E+00 / Projection distortion parameter
PV2_2   =  -7.611082639394E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -4.708599487790E-02 / Projection distortion parameter
PV2_5   =   1.490678936910E-02 / Projection distortion parameter
PV2_6   =  -1.030284914207E-02 / Projection distortion parameter
PV2_7   =   1.792576002572E-02 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   4.339902545378E-03 / Projection distortion parameter
PV2_10  =  -2.834364628169E-03 / Projection distortion parameter
PV1_4   =   7.392156197298E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -4.857569031256E-03 / Projection distortion parameter
PV1_1   =   1.014908060820E+00 / Projection distortion parameter
PV1_0   =   2.479061132134E-03 / Projection distortion parameter
PV1_9   =   8.010888416329E-03 / Projection distortion parameter
PV1_8   =  -7.273053780327E-03 / Projection distortion parameter
CD1_2   =  0.00466285797893121 / Linear projection matrix
PV1_5   =  -2.020409088488E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046622816933101 / Linear projection matrix
CD2_2   =  -1.6456610412960E-5 / Linear projection matrix
PV1_10  =  -1.341296365071E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   3.099422758502E+02 / World coordinate on this axis
CRVAL2  =  -3.503367718117E+01 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Mon Oct 12 16:35:43 2020' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Mon Oct 12 16:35:43 2020' / overscan corrected
BIASFIL = 'DEC20A_AZ20201008_98d1ca4-59132Z_01.fits' / Bias correction file
BAND    = 'VR      '
NITE    = '20201010'
DESBIAS = 'Mon Oct 12 16:46:10 2020'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Mon Oct 12 16:46:10 2020'
DESBPM  = 'Mon Oct 12 16:46:10 2020' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Mon Oct 12 16:46:10 2020' / Flag saturated pixels
NSATPIX =                 1622 / Number of saturated pixels
FLATMEDA=     3.47656436552582
FLATMEDB=     3.42507125003395
SATURATE=     44905.0877780564
DESGAINC= 'Mon Oct 12 16:46:10 2020'
FLATFIL = 'DEC20A_AZ20201008_98d1ca4-59132VRF_01.fits' / Dome flat correction fi
DESFLAT = 'Mon Oct 12 16:46:11 2020'
STARFIL = 'STARFLAT_20201010_98d2bf0-VRP_ci_01.fits' / Star flat correction file
DESSTAR = 'Mon Oct 12 16:46:11 2020'
HISTORY Mon Oct 12 16:46:11 2020 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Mon Oct 12 17:20:08 2020' / bleed trail masking
NBLEED  = 4357  / Number of bleed trail pixels.
STARMASK= 'Mon Oct 12 17:20:08 2020' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC20B_AW20201010_98d4
HISTORY 98e-filVR-ccd01VR/DEC20B_AW20201010_98d498e-VR-201011024840_01.fits omib
HISTORY leed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Oct 12 17:20 Bad pixel file is omibleed_01_bpm.pl'
FWHM    =             4.792542 / Median FWHM in pixels
ELLIPTIC=            0.1031149 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Mon Oct 12 17:34:25 2020' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
PHOTREF = 'Gaia    '           / Photometric ref. catalog
MAGZERO =               29.453 / [mag] Magnitude zero point
MAGZPT  =               25.760 / [mag] Magnitude zero point per sec
NPHTMTCH=                15559 / Number of phot. stars matched
RADIUS  =                 2.63 / [pix] Average radius of stars
RADSTD  =                0.099 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             208.9284
NCOMBINE=                50640
PUPILSKY=     209.891302873502
PUPILSUM=     83943.6580768252
PUPILMAX=     1.99835538864136
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'MAFjP6DjMADjM3Dj'   / HDU checksum updated 2020-11-03T20:23:31
DATASUM = '789981182'          / data unit checksum updated 2020-11-03T20:23:31
NPUPSCL =                82574
PUPSCL  =     0.99378980749298
   Ț     Ą  Ì                 	Ű  	Ű  Ì  8  8      êÿÿÿ               ÿ8       @7``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
 
@`
 
@`
 
@`
 
@`
 
@ 
@ 
@`
`
`
 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
``````````````` @ @ @ @ @` @ @ @ @ @```````````````` @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@`
`
`
 
@ 
@ 
@`
 
@`
 
@`
 
@`
 
@`
`
`
`
`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@B   ÿ      Ą 7 ÿ      AĄ  ÿ 
   
  @À $@œ  ÿ 
   
  @č 3@”  ÿ 
   
  @ł >@°  ÿ 
   
  @ź H@«  ÿ 
   
  @Ș P@§  ÿ 
   
  @Š X@Ł  ÿ 
   
  @ą `@  ÿ 
   
  @ f@  ÿ 
   
  @ l@  ÿ 
   
  @ r@  ÿ 
   
  @ x@  ÿ 
   
  @ |@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ą@~  ÿ 
   
  @ Š@|  ÿ 
   
  @} Ș@z  ÿ 
   
  @{ ź@x  ÿ 
   
  @z °@w  ÿ 
   
  @x Ž@u  ÿ 
   
  @w ¶@t  ÿ 
   
  @u ș@r  ÿ 
   
  @t Œ@q  ÿ 
   
  @r À@o  ÿ 
   
  @q Â@n  ÿ 
   
  @o Æ@l  ÿ 
   
  @n È@k  ÿ 
   
  @m Ë@i  ÿ 
   
  @k Î@h  ÿ 
   
  @j Đ@g  ÿ 
   
  @i Ò@f  ÿ 
   
  @g Ö@d  ÿ 
   
  @f Ű@c  ÿ 
   
  @e Ú@b  ÿ 
   
  @d Ü@a  ÿ 
   
  @c Ț@`  ÿ 
   
  @a â@^  ÿ 
   
  @` ä@]  ÿ 
   
  @_ æ@\  ÿ 
   
  @^ è@[  ÿ 
   
  @] ê@Z  ÿ 
   
  @\ ì@Y  ÿ 
   
  @[ î@X  ÿ 
   
  @Z đ@W  ÿ 
   
  @Y ò@V  ÿ 
   
  @X ô@U  ÿ 
   
  @W ö@T  ÿ 
   
  @V ű@S  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S ț@P  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@L  ÿ 
   
  @N@K  ÿ 
   
  @M
@J  ÿ 
   
  @L@I  ÿ 
   
  @K@H  ÿ 
   
  @K@G  ÿ 
   
  @J@G  ÿ 
   
  @I@F  ÿ 
   
  @H@E  ÿ 
   
  @G@D  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@A  ÿ 
   
  @C@@  ÿ 
   
  @B @?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?&@<  ÿ 
   
  @>(@;  ÿ 
   
  @=*@:  ÿ 
   
  @=+@9  ÿ 
   
  @<,@9  ÿ 
   
  @;.@8  ÿ 
   
  @:0@7  ÿ 
   
  @92@6  ÿ 
   
  @93@5  ÿ 
   
  @84@5  ÿ 
   
  @76@4  ÿ 
   
  @68@3  ÿ 
   
  @5:@2  ÿ 
   
  @4<@1  ÿ 
   
  @3>@0  ÿ 
   
  @2@@/  ÿ 
   
  @1B@.  ÿ 
   
  @0D@-  ÿ 
   
  @/F@,  ÿ 
   
  @.H@+  ÿ 
   
  @-J@*  ÿ 
   
  @,L@)  ÿ      @, @ @)  ÿ      @, @ @)  ÿ      @, @ @)  ÿ      @+ @" @(  ÿ      @+ @& @(  ÿ      @+ @( @(  ÿ      @+ @+ @(  ÿ      @+ @. @(  ÿ      @+ @0 @'  ÿ      @* @2 @'  ÿ      @* @4 @'  ÿ      @* @6 @'  ÿ      @* @8 @'  ÿ      @* @9 @'  ÿ      @* @: @'  ÿ      @* @< @'  ÿ      @) @> @&  ÿ      @) @@ @&  ÿ      @) @B @&  ÿ      @) @D @&  ÿ      @) @F @&  ÿ      @) @G @%  ÿ      @) @F @&  ÿ      @) @D @&  ÿ      @) @B @&  ÿ      @) @@ @&  ÿ      @) @> @&  ÿ      @* @< @'  ÿ      @* @: @'  ÿ      @* @9 @'  ÿ      @* @8 @'  ÿ      @* @6 @'  ÿ      @* @4 @'  ÿ      @* @2 @'  ÿ      @+ @0 @'  ÿ      @+ @. @(  ÿ      @+ @+ @(  ÿ      @+ @( @(  ÿ      @+ @& @(  ÿ      @+ @" @(  ÿ      @, @ @)  ÿ      @, @ @)  ÿ      @, @ @)  ÿ 
   
  @,L@)  ÿ 
   
  @-J@*  ÿ 
   
  @.H@+  ÿ 
   
  @/F@,  ÿ 
   
  @0D@-  ÿ 
   
  @1B@.  ÿ 
   
  @2@@/  ÿ 
   
  @3>@0  ÿ 
   
  @4<@1  ÿ 
   
  @5:@2  ÿ 
   
  @68@3  ÿ 
   
  @76@4  ÿ 
   
  @84@5  ÿ 
   
  @93@5  ÿ 
   
  @92@6  ÿ 
   
  @:0@7  ÿ 
   
  @;.@8  ÿ 
   
  @<,@9  ÿ 
   
  @=+@9  ÿ 
   
  @=*@:  ÿ 
   
  @>(@;  ÿ 
   
  @?&@<  ÿ 
   
  @@$@=  ÿ 
   
  @A"@>  ÿ 
   
  @B @?  ÿ 
   
  @C@@  ÿ 
   
  @D@A  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @G@D  ÿ 
   
  @H@E  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@G  ÿ 
   
  @K@H  ÿ 
   
  @L@I  ÿ 
   
  @M
@J  ÿ 
   
  @N@K  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S ț@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ű@S  ÿ 
   
  @W ö@T  ÿ 
   
  @X ô@U  ÿ 
   
  @Y ò@V  ÿ 
   
  @Z đ@W  ÿ 
   
  @[ î@X  ÿ 
   
  @\ ì@Y  ÿ 
   
  @] ê@Z  ÿ 
   
  @^ è@[  ÿ 
   
  @_ æ@\  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @c Ț@`  ÿ 
   
  @d Ü@a  ÿ 
   
  @e Ú@b  ÿ 
   
  @f Ű@c  ÿ 
   
  @g Ö@d  ÿ 
   
  @i Ò@f  ÿ 
   
  @j Đ@g  ÿ 
   
  @k Î@h  ÿ 
   
  @m Ë@i  ÿ 
   
  @n È@k  ÿ 
   
  @o Æ@l  ÿ 
   
  @q Â@n  ÿ 
   
  @r À@o  ÿ 
   
  @t Œ@q  ÿ 
   
  @u ș@r  ÿ 
   
  @w ¶@t  ÿ 
   
  @x Ž@u  ÿ 
   
  @z °@w  ÿ 
   
  @{ ź@x  ÿ 
   
  @} Ș@z  ÿ 
   
  @ Š@|  ÿ 
   
  @ ą@~  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ |@  ÿ 
   
  @ x@  ÿ 
   
  @ r@  ÿ 
   
  @ l@  ÿ 
   
  @ f@  ÿ 
   
  @ą `@  ÿ 
   
  @Š X@Ł  ÿ 
   
  @Ș P@§  ÿ 
   
  @ź H@«  ÿ 
   
  @ł >@°  ÿ 
   
  @č 3@”  ÿ 
   
  @À $@œ B ÿ      AĄ