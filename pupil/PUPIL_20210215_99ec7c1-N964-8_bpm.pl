  ŠV  =  æ$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1298456511
$MTIME = 1298456511
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2021-02-22T17:21:51' / Date FITS file was generated
IRAF-TLM= '2021-02-22T17:21:54' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                 900. / [s] Requested exposure duration
EXPTIME =                 900. / [s] Exposure duration
EXPDUR  =                 900. / [s] Exposure duration
DARKTIME=          901.2163701 / [s] Dark time
OBSID   = 'ct4m20210216t045839' / Unique Observation ID
DATE-OBS= '2021-02-16T04:58:39.443513' / DateTime of observation start (UTC)
TIME-OBS= '04:58:39.443513'    / Time of observation start (UTC)
MJD-OBS =       59261.20740097 / MJD of observation start
MJD-END =      59261.217817637 / Exposure end
OPENSHUT= '2021-02-16T04:58:39.599910' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               965743 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'Lyman Alpha Galaxies in the Epoch of Reionization' / Current observin
OBSERVER= 'Isak Wold, Cristobal Moya' / Observer name(s)
PROPOSER= 'Malhotra'           / Proposal Principle Investigator
DTPI    = 'Malhotra'           / Proposal Principle Investigator (iSTB)
PROPID  = '2018B-0327'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'N964 DECam c0008 9645.0 94.0' / Unique filter identifier
INSTANCE= 'DECam_20210215'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '09:57:38.614'       / [HH:MM:SS] Target RA
DEC     = '02:51:10.429'       / [DD:MM:SS] Target DEC
TELRA   = '09:57:38.660'       / [HH:MM:SS] Telescope RA
TELDEC  = '02:51:09.698'       / [DD:MM:SS] Telescope DEC
HA      = '00:02:21.330'       / [HH:MM:SS] Telescope hour angle
ZD      =                32.92 / [deg] Telescope zenith distance
AZ      =             359.0684 / [deg] Telescope azimuth angle
DOMEAZ  =                1.392 / [deg] Dome azimuth angle
ZPDELRA =            -149.9242 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                 15.7 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-400.98,-1403.14,2720.32,-25.85,-133.91,-0.00' / DECam hexapod settin
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =               18.508 / [m/s] Wind speed
WINDDIR =                   2. / [deg] Wind direction (from North)
HUMIDITY=                  57. / [%] Ambient relative humidity (outside)
PRESSURE=                 779. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                1.529 / [arcsec] DIMM2 Seeing
MASS2   =                 1.32 /  MASS(2) FSEE
ASTIG1  =                 0.06 / 4MAPS correction 1
ASTIG2  =                -0.03 / 4MAPS correction 2
OUTTEMP =                 12.3 / [deg C] Outside temperature
AIRMASS =                 1.19 / Airmass
GSKYVAR =                0.042 / RASICAM global sky standard deviation
GSKYHOT =                0.082 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               12.363 / [deg C] Mirror surface average temperature
MAIRTEMP=                 12.3 / [deg C] Mirror temperature above surface
UPTRTEMP=               12.027 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 12.1 / [deg C] Mirror top surface temperature
UTN-TEMP=               12.085 / [deg C] Upper truss temperature north
UTS-TEMP=               11.855 / [deg C] Upper truss temperature south
UTW-TEMP=                 12.1 / [deg C] Upper truss temperature west
UTE-TEMP=                12.07 / [deg C] Upper truss temperature east
PMN-TEMP=                 12.2 / [deg C] Mirror north edge temperature
PMS-TEMP=                 12.3 / [deg C] Mirror south edge temperature
PMW-TEMP=                12.55 / [deg C] Mirror west edge temperature
PME-TEMP=                 12.4 / [deg C] Mirror east edge temperature
DOMELOW =               13.125 / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                  10. / [deg C] Dome floor temperature
G-MEANX =              -0.0187 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0855 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[0.74,2.32,-8.96,-0.14,0.25,0.03,0.18,-0.10,-0.36,]' / Mean Wavefront
DONUTFS3= '[0.45,1.40,8.48,-0.21,-0.15,0.01,0.10,0.04,-0.24,]' / Mean Wavefront
DONUTFS2= '[1.07,0.94,-9.01,-0.09,0.35,-0.09,0.07,0.37,-0.02,]' / Mean Wavefront
DONUTFS1= '[1.50,1.67,8.48,0.04,0.17,-0.04,0.06,0.17,0.12,]' / Mean Wavefront fo
G-FLXVAR=            235822.69 / [arcsec] Guider mean guide star flux variances
G-MEANXY=            -0.003474 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[1.40,0.83,-9.15,-0.27,-0.00,-0.10,-0.29,0.23,-0.21,]' / Mean Wavefro
DONUTFN2= '[1.92,1.35,8.57,-0.60,-0.14,-0.10,-0.22,0.14,-0.01,]' / Mean Wavefron
DONUTFN3= '[1.84,2.19,-9.05,-0.05,0.11,-0.00,-0.24,0.05,0.33,]' / Mean Wavefront
DONUTFN4= '[3.76,1.56,8.41,-0.05,-0.33,0.05,-0.27,-0.06,0.21,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2021-02-16T05:14:01.098948'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    2 / Number of guide CCDs that remained active
DOXT    =                -0.03 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.6083 / [arcsec] Guider x-axis maximum offset
FADZ    =                47.86 / [um] FA Delta focus.
FADY    =              -107.19 / [um] FA Delta Y.
FADX    =              -205.05 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =               -11.34 / [arcsec] FA Delta Y-theta.
DODZ    =                47.86 / [um] Delta-Z from donut analysis
DODY    =                -1.58 / [um] Y-decenter from donut analysis
DODX    =                -1.53 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2021-02-16T04:58:34' / Time of last RASICAM exposure (UTC)
G-SEEING=                1.656 / [arcsec] Guider average seeing
G-TRANSP=                0.516 / Guider average sky transparency
G-MEANY2=             0.074804 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                 0.17 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.335 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                -5.32 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.9392 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.041673 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:89'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTACQNAM= '/data_local/images/DTS/2018B-0327/DECam_00965743.fits.fz'
ODATEOBS= '2021-02-16T04:58:39.443513'
DTINSTRU= 'decam   '
DTTELESC= 'ct4m    '
DTCALDAT= '2021-02-15'
DTNSANAM= 'c4d_210216_045839_ori.fits.fz'
DTPROPID= '2018B-0327'
DTSITE  = 'ct      '
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =      3.7227292612782
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=      41935.745804517
GAINB   =     3.66482569192712
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     38720.7258758821
CRPIX1  =             209.3625 / Coordinate Reference axis 1
CRPIX2  =        230.270828125 / Coordinate Reference axis 2
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 0x0 4.010000' / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =                2000. / [yr] Equinox of WCS
PV1_7   =    0.000618368874624 / PV distortion coefficient
CUNIT1  = 'deg     '
PV2_8   =    -0.00842887692623 / PV distortion coefficient
PV2_9   =     0.00790302236959 / PV distortion coefficient
CD1_1   =  -1.1679624622720E-5 / World coordinate transformation matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =    -0.00441101222605 / PV distortion coefficient
PV2_1   =        1.01700592995 / PV distortion coefficient
PV2_2   =    -0.00863195988986 / PV distortion coefficient
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =     -0.0181936225912 / PV distortion coefficient
PV2_5   =      0.0168279891847 / PV distortion coefficient
PV2_6   =      -0.010365069688 / PV distortion coefficient
PV2_7   =     0.00641705596954 / PV distortion coefficient
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =       0.010153274645 / PV distortion coefficient
PV2_10  =    -0.00261077202228 / PV distortion coefficient
PV1_4   =     0.00862489761958 / PV distortion coefficient
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =    -0.00978790747587 / PV distortion coefficient
PV1_1   =        1.01430869337 / PV distortion coefficient
PV1_0   =     0.00392185213728 / PV distortion coefficient
PV1_9   =     0.00654393666949 / PV distortion coefficient
PV1_8   =    -0.00736713022181 / PV distortion coefficient
CD1_2   =  0.00466262971156481 / World coordinate transformation matrix
PV1_5   =      -0.017864081795 / PV distortion coefficient
CUNIT2  = 'deg     '
CD2_1   =  -0.0046623936290495 / World coordinate transformation matrix
CD2_2   =  -1.1659626630144E-5 / World coordinate transformation matrix
PV1_10  =    -0.00361076208606 / PV distortion coefficient
CTYPE2  = 'DEC--TPV'           / Coordinate type
CTYPE1  = 'RA---TPV'           / Coordinate type
CRVAL1  =           149.411083 / [deg] WCS Reference Coordinate (RA)
CRVAL2  =             2.852694 / [deg] WCS Reference Coordinate (DEC)
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Mon Feb 22 08:30:59 2021' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Mon Feb 22 08:30:59 2021' / overscan corrected
BIASFIL = 'DEC21A_AZ20210215_99e42d0-59260Z_01.fits' / Bias correction file
BAND    = 'N964    '
NITE    = '20210215'
DESBIAS = 'Mon Feb 22 08:38:19 2021'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Mon Feb 22 08:38:21 2021'
DESBPM  = 'Mon Feb 22 08:38:21 2021' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Mon Feb 22 08:38:21 2021' / Flag saturated pixels
NSATPIX =                 7407 / Number of saturated pixels
FLATMEDA=      3.7227292612782
FLATMEDB=     3.66482569192712
SATURATE=      41935.745804517
DESGAINC= 'Mon Feb 22 08:38:21 2021'
FLATFIL = 'DEC21A_AZ20210215_99e42d0-59260N964F_01.fits' / Dome flat correction
DESFLAT = 'Mon Feb 22 08:38:21 2021'
STARFIL = 'STARFLAT_20210128_99bacc2-N964P_ci_01.fits' / Star flat correction fi
DESSTAR = 'Mon Feb 22 08:38:22 2021'
HISTORY Mon Feb 22 08:38:22 2021 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
AMPMTCH = 'B 1.0009 (0.0005,0.0050)' / Amp match
DESBLEED= 'Mon Feb 22 08:58:56 2021' / bleed trail masking
NBLEED  = 19939  / Number of bleed trail pixels.
STARMASK= 'Mon Feb 22 08:58:56 2021' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/PUPIL_20210215_99ec7c1
HISTORY -filN964-ccd01N964/PUPIL_20210215_99ec7c1-N964-210216045839_01.fits omib
HISTORY leed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Feb 22  8:59 Bad pixel file is omibleed_01_bpm.pl'
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             618.0387
NCOMBINE=                 3780
PUPILSKY=     600.011804804172
PUPILSUM=     3129243.49403467
PUPILMAX=     60.6845512390137
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'bp8acn5Wbn5abn5U'   / HDU checksum updated 2021-02-22T17:16:28
DATASUM = '3082760493'         / data unit checksum updated 2021-02-22T17:16:28
NPUPSCL =                93923
PUPSCL  =    0.029930468528561
    Ț     Ą  Ì                 \  \  Ì  b  æ      êÿÿÿ               ÿb       @(``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
`
`
 
@ 
@ 
@ 
@`
`
`
 
@ 
@ 
@`
 
@`
 
@ 
@ 
@`
`
``````````````````````` @``` @ @ @```` @` @ @ @` @` @``` @`````` @`````````````````````````` 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
 
@`
`
`
`
`
`
`
 
@`
`
`
`
`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@3   ÿ      Ą ( ÿ      AĄ  ÿ 
   
  @Ç @Æ  ÿ 
   
  @» ,@ș  ÿ 
   
  @Ž :@ł  ÿ 
   
  @ź F@­  ÿ 
   
  @© O@©  ÿ 
   
  @„ X@€  ÿ 
   
  @Ą `@   ÿ 
   
  @ f@  ÿ 
   
  @ n@  ÿ 
   
  @ t@  ÿ 
   
  @ z@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @  @  ÿ 
   
  @ €@~  ÿ 
   
  @} š@|  ÿ 
   
  @{ Ź@z  ÿ 
   
  @y °@x  ÿ 
   
  @w Ž@v  ÿ 
   
  @v ¶@u  ÿ 
   
  @t ș@s  ÿ 
   
  @r Ÿ@q  ÿ 
   
  @q À@p  ÿ 
   
  @o Ä@n  ÿ 
   
  @n Ç@l  ÿ 
   
  @l Ê@k  ÿ 
   
  @k Í@i  ÿ 
   
  @i Đ@h  ÿ 
   
  @h Ó@f  ÿ 
   
  @f Ö@e  ÿ 
   
  @e Ű@d  ÿ 
   
  @d Û@b  ÿ 
   
  @b Ț@a  ÿ 
   
  @a à@`  ÿ 
   
  @` ă@^  ÿ 
   
  @^ æ@]  ÿ 
   
  @] è@\  ÿ 
   
  @\ ê@[  ÿ 
   
  @[ í@Y  ÿ 
   
  @Z ï@X  ÿ 
   
  @X ò@W  ÿ 
   
  @W ô@V  ÿ 
   
  @V ö@U  ÿ 
   
  @U ű@T  ÿ 
   
  @T ú@S  ÿ 
   
  @S ü@R  ÿ 
   
  @R ÿ@P  ÿ 
   
  @Q@O  ÿ 
   
  @P@N  ÿ 
   
  @O@M  ÿ 
   
  @N@L  ÿ 
   
  @M	@K  ÿ 
   
  @L@J  ÿ 
   
  @K@J  ÿ 
   
  @J@I  ÿ 
   
  @I@H  ÿ 
   
  @H@G  ÿ 
   
  @G@F  ÿ 
   
  @G@E  ÿ 
   
  @F@D  ÿ 
   
  @E@C  ÿ 
   
  @D@B  ÿ 
   
  @C@B  ÿ 
   
  @B@A  ÿ 
   
  @B@@  ÿ 
   
  @A!@?  ÿ 
   
  @@#@>  ÿ 
   
  @?$@>  ÿ 
   
  @?%@=  ÿ 
   
  @>'@<  ÿ 
   
  @=)@;  ÿ 
   
  @<*@;  ÿ 
   
  @<+@:  ÿ 
   
  @;-@9  ÿ 
   
  @:.@9  ÿ 
   
  @:/@8  ÿ 
   
  @91@7  ÿ 
   
  @82@7  ÿ 
   
  @83@6  ÿ 
   
  @75@5  ÿ 
   
  @66@5  ÿ 
   
  @67@4  ÿ 
   
  @59@3  ÿ 
   
  @4:@3  ÿ 
   
  @4;@2  ÿ 
   
  @3=@1  ÿ 
   
  @2?@0  ÿ 
   
  @1@@0  ÿ 
   
  @1A@/  ÿ 
   
  @0B@/  ÿ 
   
  @0C@.  ÿ 
   
  @/D@.  ÿ 
   
  @/E@-  ÿ 
   
  @.G@,  ÿ 
   
  @-I@+  ÿ 
   
  @,K@*  ÿ 
   
  @+M@)  ÿ 
   
  @*N@)  ÿ 
   
  @*O@(  ÿ 
   
  @)P@(  ÿ 
   
  @)Q@'  ÿ 
   
  @(S@&  ÿ 
   
  @'U@%  ÿ 
   
  @&V@%  ÿ 
   
  @&W@$  ÿ 
   
  @%X@$  ÿ 
   
  @%Y@#  ÿ 
   
  @$[@"  ÿ 
   
  @#]@!  ÿ 
   
  @"_@   ÿ      @" Ź@ ­@   ÿ      @" €@ „@   ÿ      @! Ą@ ą@  ÿ      @! @$ @  ÿ      @! @* @  ÿ      @  @. @  ÿ      @  @2 @  ÿ      @  @6 @  ÿ      @  @: @  ÿ      @ @< @  ÿ      @ @@ @  ÿ      @ @B @  ÿ      @ @E @  ÿ      @ @H @  ÿ      @ @J @  ÿ      @ @L @  ÿ      @ @N @  ÿ      @ @P @  ÿ      @ @R @  ÿ      @ @T @  ÿ      @ @U @  ÿ      @ @W @  ÿ      @ @X @  ÿ      @ @Z @  ÿ      @ @\ @  ÿ      @ @^ @  ÿ      @ @_ @  ÿ      @ @` @  ÿ      @ @b @  ÿ      @ @d @  ÿ      @ @f @  ÿ      @ @g @  ÿ      @ @h @  ÿ      @ @h @  ÿ      @ @i @  ÿ      @ @j @  ÿ      @ @k @  ÿ      @ @l @  ÿ      @ @m @  ÿ      @ @n @  ÿ      @ @m @  ÿ      @ @l @  ÿ      @ @k @  ÿ      @ @j @  ÿ      @ @i @  ÿ      @ @h @  ÿ      @ @h @  ÿ      @ @f @  ÿ      @ @e @  ÿ      @ @d @  ÿ      @ @c @  ÿ      @ @b @  ÿ      @ @a @  ÿ      @ @` @  ÿ      @ @^ @  ÿ      @ @\ @  ÿ      @ @Z @  ÿ      @ @Y @  ÿ      @ @X @  ÿ      @ @V @  ÿ      @ @T @  ÿ      @ @S @  ÿ      @ @Q @  ÿ      @ @O @  ÿ      @ @M @  ÿ      @ @K @  ÿ      @ @H @  ÿ      @ @F @  ÿ      @ @D @  ÿ      @ @A @  ÿ      @ @> @  ÿ      @  @; @  ÿ      @  @8 @  ÿ      @  @4 @  ÿ      @  @0 @  ÿ      @! @, @  ÿ      @! @( @  ÿ      @! @"  @  ÿ      @" ą@ Ł@   ÿ      @" §@ š@   ÿ 
   
  @"_@   ÿ 
   
  @#]@!  ÿ 
   
  @$[@"  ÿ 
   
  @%Y@#  ÿ 
   
  @&W@$  ÿ 
   
  @'U@%  ÿ 
   
  @(S@&  ÿ 
   
  @)Q@'  ÿ 
   
  @*O@(  ÿ 
   
  @+M@)  ÿ 
   
  @,K@*  ÿ 
   
  @-I@+  ÿ 
   
  @.G@,  ÿ 
   
  @/E@-  ÿ 
   
  @0C@.  ÿ 
   
  @1A@/  ÿ 
   
  @2?@0  ÿ 
   
  @3=@1  ÿ 
   
  @4;@2  ÿ 
   
  @59@3  ÿ 
   
  @67@4  ÿ 
   
  @75@5  ÿ 
   
  @84@5  ÿ 
   
  @83@6  ÿ 
   
  @91@7  ÿ 
   
  @:0@7  ÿ 
   
  @:/@8  ÿ 
   
  @;-@9  ÿ 
   
  @<+@:  ÿ 
   
  @=)@;  ÿ 
   
  @>'@<  ÿ 
   
  @?&@<  ÿ 
   
  @?%@=  ÿ 
   
  @@#@>  ÿ 
   
  @A!@?  ÿ 
   
  @B@@  ÿ 
   
  @C@A  ÿ 
   
  @D@B  ÿ 
   
  @E@C  ÿ 
   
  @F@D  ÿ 
   
  @G@E  ÿ 
   
  @H@F  ÿ 
   
  @I@G  ÿ 
   
  @J@H  ÿ 
   
  @K@I  ÿ 
   
  @L@J  ÿ 
   
  @M	@K  ÿ 
   
  @N@L  ÿ 
   
  @O@M  ÿ 
   
  @P@N  ÿ 
   
  @Q@O  ÿ 
   
  @R ÿ@P  ÿ 
   
  @S ę@Q  ÿ 
   
  @T û@R  ÿ 
   
  @U ù@S  ÿ 
   
  @V ś@T  ÿ 
   
  @W ő@U  ÿ 
   
  @X ó@V  ÿ 
   
  @Z ï@X  ÿ 
   
  @[ í@Y  ÿ 
   
  @\ ë@Z  ÿ 
   
  @] é@[  ÿ 
   
  @^ ç@\  ÿ 
   
  @` ă@^  ÿ 
   
  @a á@_  ÿ 
   
  @b ß@`  ÿ 
   
  @d Û@b  ÿ 
   
  @e Ù@c  ÿ 
   
  @f Ś@d  ÿ 
   
  @h Ó@f  ÿ 
   
  @i Ń@g  ÿ 
   
  @k Í@i  ÿ 
   
  @l Ë@j  ÿ 
   
  @n Ç@l  ÿ 
   
  @o Ć@m  ÿ 
   
  @q Á@o  ÿ 
   
  @r ż@p  ÿ 
   
  @t »@r  ÿ 
   
  @v ·@t  ÿ 
   
  @w ”@u  ÿ 
   
  @y ±@w  ÿ 
   
  @{ ­@y  ÿ 
   
  @} ©@{  ÿ 
   
  @ „@}  ÿ 
   
  @ Ą@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ {@  ÿ 
   
  @ u@  ÿ 
   
  @ o@  ÿ 
   
  @ i@  ÿ 
   
  @  c@  ÿ 
   
  @€ [@ą  ÿ 
   
  @š S@Š  ÿ 
   
  @Ź J@«  ÿ 
   
  @± @@°  ÿ 
   
  @· 4@¶  ÿ 
   
  @ż #@ż 3 ÿ      AĄ