  �V  A�  
�$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1279298983
$MTIME = 1279298983
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-07-15T23:48:25' / Date FITS file was generated
IRAF-TLM= '2020-07-15T23:48:06' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  90. / [s] Requested exposure duration
EXPTIME =                  90. / [s] Exposure duration
EXPDUR  =                  90. / [s] Exposure duration
DARKTIME=             91.10093 / [s] Dark time
OBSID   = 'ct4m20170817t052351' / Unique Observation ID
DATE-OBS= '2017-08-17T05:23:51.824921' / DateTime of observation start (UTC)
TIME-OBS= '05:23:51.824921'    / Time of observation start (UTC)
MJD-OBS =       57982.22490538 / MJD of observation start
MJD-END =      57982.225947047 / Exposure end
OPENSHUT= '2017-08-17T05:23:51.866800' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               668038 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'survey  '           / Current observing orogram
OBSERVER= 'Erika Cook, Marcus Sauseda, Jen Marshall' / Observer name(s)
PROPOSER= 'Frieman '           / Proposal Principle Investigator
DTPI    = 'Frieman '           / Proposal Principle Investigator (iSTB)
PROPID  = '2012B-0001'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
HEX     =                 1434 / DES Hex number
TILING  =                    8 / DES Tiling number
SEQID   = '-186-410 enqueued on 2017-08-17 05:15:35Z by SurveyTacticianY1' / Seq
SEQNUM  =                    1 / Number of image in sequence
SEQTOT  =                    1 / Total number of images in sequence
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'r DECam SDSS c0002 6415.0 1480.0' / Unique filter identifier
FILTPOS = 'cassette_8'         / Filter position in FCM
INSTANCE= 'DECam_20170816'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '22:44:15.716'       / [HH:MM:SS] Target RA
DEC     = '-41:06:52.070'      / [DD:MM:SS] Target DEC
TELRA   = '22:44:15.266'       / [HH:MM:SS] Telescope RA
TELDEC  = '-41:06:46.897'      / [DD:MM:SS] Telescope DEC
HA      = '-00:21:35.600'      / [HH:MM:SS] Telescope hour angle
ZD      =                11.71 / [deg] Telescope zenith distance
AZ      =             159.3929 / [deg] Telescope azimuth angle
DOMEAZ  =              183.061 / [deg] Dome azimuth angle
ZPDELRA =              19.3755 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                 -1.1 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-441.50,897.04,2878.15,-91.84,-122.98,-0.01' / DECam hexapod settings
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    T / RASICAM local sky clear flag
WINDSPD =               15.289 / [m/s] Wind speed
WINDDIR =                  33. / [deg] Wind direction (from North)
HUMIDITY=                  25. / [%] Ambient relative humidity (outside)
PRESSURE=                 781. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE= 'NaN     '           / [arcsec] DIMM2 Seeing
MASS2   = 'NaN     '           /  MASS(2) FSEE
ASTIG1  =                 0.04 / 4MAPS correction 1
ASTIG2  =                -0.04 / 4MAPS correction 2
OUTTEMP =                 11.3 / [deg C] Outside temperature
AIRMASS =                 1.02 / Airmass
GSKYVAR =                0.028 / RASICAM global sky standard deviation
GSKYHOT =                0.896 / RASICAM global sky fraction above threshold
LSKYVAR =                   0. / RASICAM local sky standard deviation
LSKYHOT =                   0. / RASICAM local sky fraction above threshold
LSKYPOW =                   0. / RASICAM local sky normalized power
MSURTEMP=                  9.7 / [deg C] Mirror surface average temperature
MAIRTEMP=                 10.3 / [deg C] Mirror temperature above surface
UPTRTEMP=               11.841 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                10.35 / [deg C] Mirror top surface temperature
UTN-TEMP=               11.995 / [deg C] Upper truss temperature north
UTS-TEMP=               11.675 / [deg C] Upper truss temperature south
UTW-TEMP=               11.865 / [deg C] Upper truss temperature west
UTE-TEMP=                11.83 / [deg C] Upper truss temperature east
PMN-TEMP=                  9.4 / [deg C] Mirror north edge temperature
PMS-TEMP=                  9.5 / [deg C] Mirror south edge temperature
PMW-TEMP=                  9.7 / [deg C] Mirror west edge temperature
PME-TEMP=                 10.2 / [deg C] Mirror east edge temperature
DOMELOW =                12.22 / [deg C] Low dome temperature
DOMEHIGH=                   0. / [deg C] High dome temperature
DOMEFLOR=                  5.3 / [deg C] Dome floor temperature
G-MEANX =              -0.0198 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0557 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.13,0.98,-8.64,-0.18,0.00,-0.09,-0.01,-0.13,-0.38,]' / Mean Wavefro
DONUTFS3= '[0.26,1.94,8.96,-0.02,-0.01,0.05,0.05,0.01,-0.36,]' / Mean Wavefront
DONUTFS2= '[1.70,0.67,-8.56,0.27,0.07,-0.12,0.07,0.22,0.02,]' / Mean Wavefront f
DONUTFS1= '[0.56,1.14,8.92,0.26,0.27,0.22,0.08,0.19,0.16,]' / Mean Wavefront for
G-FLXVAR=          4087027.346 / [arcsec] Guider mean guide star flux variances
G-MEANXY=            -0.000343 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[1.04,0.33,-8.61,-0.02,-0.01,-0.01,-0.00,0.19,-0.19,]' / Mean Wavefro
DONUTFN2= '[]      '           / Mean Wavefront for Sensor FN2
DONUTFN3= '[0.93,1.01,-8.49,0.15,0.01,-0.02,0.02,-0.04,0.40,]' / Mean Wavefront
DONUTFN4= '[1.87,1.84,8.91,0.29,-0.09,0.10,0.16,-0.14,0.24,]' / Mean Wavefront f
HIERARCH TIME_RECORDED = '2017-08-17T05:25:44.095693'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    4 / Number of guide CCDs that remained active
DOXT    =                -0.03 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.1224 / [arcsec] Guider x-axis maximum offset
FADZ    =               -29.52 / [um] FA Delta focus.
FADY    =               360.73 / [um] FA Delta Y.
FADX    =              -202.82 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                10.44 / [arcsec] FA Delta Y-theta.
DODZ    =               -29.52 / [um] Delta-Z from donut analysis
DODY    =                -1.07 / [um] Y-decenter from donut analysis
DODX    =                -1.13 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2017-08-17T05:23:40' / Time of last RASICAM exposure (UTC)
G-SEEING=                1.727 / [arcsec] Guider average seeing
G-TRANSP=                0.653 / Guider average sky transparency
G-MEANY2=             0.009971 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.11 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.301 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                14.62 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.2669 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.005483 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:56'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTCALDAT= '2017-08-16'
DTPROPID= '2012B-0001'
DTACQNAM= 'pipeline1.ctio.noao.edu!/data_local/images/DTS/2012B-0001/DECam_006&'
CONTINUE= '        '           /   '68038.fits.fz'
DTNSANAM= 'c4d_170817_052351_ori.fits.fz'
ODATEOBS= '2017-08-17T05:23:51.824921'
DTINSTRU= 'decam   '
DTTELESC= 'ct4m    '
DTSITE  = 'ct      '
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.51832868556976
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     44372.0419414761
GAINB   =     3.48965546453304
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     40664.3900643608
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =  -5.024944166180E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -7.518309987306E-03 / Projection distortion parameter
PV2_9   =   7.645285959324E-03 / Projection distortion parameter
CD1_1   =  -9.5882701932608E-6 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -1.103884791411E-02 / Projection distortion parameter
PV2_1   =   1.041069974106E+00 / Projection distortion parameter
PV2_2   =  -7.613300371310E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -4.707512259719E-02 / Projection distortion parameter
PV2_5   =   1.490915428213E-02 / Projection distortion parameter
PV2_6   =  -1.030223689897E-02 / Projection distortion parameter
PV2_7   =   1.792576002572E-02 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   4.342734991369E-03 / Projection distortion parameter
PV2_10  =  -2.834364628169E-03 / Projection distortion parameter
PV1_4   =   7.389892681578E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -4.863533300837E-03 / Projection distortion parameter
PV1_1   =   1.014894592248E+00 / Projection distortion parameter
PV1_0   =   2.471315564513E-03 / Projection distortion parameter
PV1_9   =   8.010888416329E-03 / Projection distortion parameter
PV1_8   =  -7.273053780327E-03 / Projection distortion parameter
CD1_2   =  0.00466293063112193 / Linear projection matrix
PV1_5   =  -2.020897084082E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046627458895814 / Linear projection matrix
CD2_2   =  -9.3477525514688E-6 / Linear projection matrix
PV1_10  =  -1.341296365071E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   3.410690880572E+02 / World coordinate on this axis
CRVAL2  =  -4.111348994945E+01 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Sun Jul 21 08:25:53 2019' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Sun Jul 21 08:25:53 2019' / overscan corrected
BIASFIL = 'TOO17B_20170814_8f6078f-57981Z_01.fits' / Bias correction file
BAND    = 'r       '
NITE    = '20170816'
DESBIAS = 'Sun Jul 21 08:39:03 2019'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Sun Jul 21 08:39:04 2019'
DESBPM  = 'Sun Jul 21 08:39:04 2019' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Sun Jul 21 08:39:04 2019' / Flag saturated pixels
NSATPIX =                 3687 / Number of saturated pixels
FLATMEDA=     3.51832868556976
FLATMEDB=     3.48965546453304
SATURATE=     44372.0419414761
DESGAINC= 'Sun Jul 21 08:39:04 2019'
FLATFIL = 'TOO17B_20170814_8f6078f-57981rF_01.fits' / Dome flat correction file
DESFLAT = 'Sun Jul 21 08:39:04 2019'
STARFIL = 'STARFLAT_20170814_94ad7dc-rP_ci_01.fits' / Star flat correction file
DESSTAR = 'Sun Jul 21 08:39:04 2019'
HISTORY Sun Jul 21 08:39:04 2019 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Sun Jul 21 08:58:45 2019' / bleed trail masking
NBLEED  = 9453  / Number of bleed trail pixels.
STARMASK= 'Sun Jul 21 08:58:45 2019' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DECALS_DR9_9520d19-fil
HISTORY r-ccd01r/DECALS_DR9_9520d19-r-170817052351_01.fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FWHM    =             3.947406 / Median FWHM in pixels
ELLIPTIC=            0.1120143 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Sun Jul 21 09:15:06 2019' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
PHOTREF = 'Gaia    '           / Photometric ref. catalog
MAGZERO =               30.071 / [mag] Magnitude zero point
MAGZPT  =               25.185 / [mag] Magnitude zero point per sec
NPHTMTCH=                 2720 / Number of phot. stars matched
RADIUS  =                 1.93 / [pix] Average radius of stars
RADSTD  =                0.050 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =              248.337
NCOMBINE=                60000
PUPILSKY=     251.605957049187
PUPILSUM=     163068.642807699
PUPILMAX=     2.97724008560181
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'LaFGLWDFLaDFLUDF'   / HDU checksum updated 2020-07-15T23:42:58
DATASUM = '172241694'          / data unit checksum updated 2020-07-15T23:42:58
NPUPSCL =                95264
PUPSCL  =     0.60166444451932
   �     �  �                 	e  	e  �  9  
�      ����               ��9       @,``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
 
@`
`
 
@`
`
 
@`
 
@`
 
@`
 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@`
`````` @ @` @ @```````` @`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
 
@`
`
 
@`
`
 
@`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@7   ��      � , ��      A�  �� 
   
  @� %@�  �� 
   
  @� 4@�  �� 
   
  @� @@�  �� 
   
  @� J@�  �� 
   
  @� T@�  �� 
   
  @� \@�  �� 
   
  @� b@�  �� 
   
  @� j@�  �� 
   
  @� p@�  �� 
   
  @� v@�  �� 
   
  @� |@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@  �� 
   
  @� �@}  �� 
   
  @~ �@{  �� 
   
  @| �@y  �� 
   
  @z �@w  �� 
   
  @x �@u  �� 
   
  @w �@t  �� 
   
  @u �@r  �� 
   
  @s �@p  �� 
   
  @r �@o  �� 
   
  @p �@m  �� 
   
  @o �@l  �� 
   
  @m �@j  �� 
   
  @l �@i  �� 
   
  @j �@g  �� 
   
  @i �@f  �� 
   
  @h �@e  �� 
   
  @f �@c  �� 
   
  @e �@b  �� 
   
  @d �@a  �� 
   
  @b �@_  �� 
   
  @a �@^  �� 
   
  @` �@]  �� 
   
  @_ �@\  �� 
   
  @] �@Z  �� 
   
  @\ �@Y  �� 
   
  @[ �@X  �� 
   
  @Z �@W  �� 
   
  @Y �@V  �� 
   
  @X �@U  �� 
   
  @W �@T  �� 
   
  @V �@S  �� 
   
  @U �@R  �� 
   
  @T �@Q  �� 
   
  @S �@P  �� 
   
  @R @O  �� 
   
  @Q@N  �� 
   
  @P@M  �� 
   
  @O@L  �� 
   
  @N@K  �� 
   
  @M
@J  �� 
   
  @L@I  �� 
   
  @K@H  �� 
   
  @J@G  �� 
   
  @I@F  �� 
   
  @H@E  �� 
   
  @G@D  �� 
   
  @F@C  �� 
   
  @E@B  �� 
   
  @D@A  �� 
   
  @C@@  �� 
   
  @B @?  �� 
   
  @A"@>  �� 
   
  @@$@=  �� 
   
  @?&@<  �� 
   
  @>(@;  �� 
   
  @=*@:  �� 
   
  @<,@9  �� 
   
  @;.@8  �� 
   
  @:0@7  �� 
   
  @92@6  �� 
   
  @84@5  �� 
   
  @76@4  �� 
   
  @68@3  �� 
   
  @5:@2  �� 
   
  @4<@1  �� 
   
  @3>@0  �� 
   
  @2@@/  �� 
   
  @1B@.  �� 
   
  @0D@-  �� 
   
  @/F@,  �� 
   
  @.H@+  �� 
   
  @-J@*  �� 
   
  @,L@)  �� 
   
  @+N@(  �� 
   
  @*P@'  �� 
   
  @)R@&  �� 
   
  @(T@%  �� 
   
  @'V@$  �� 
   
  @&X@#  �� 
   
  @%Z@"  �� 
   
  @$\@!  �� 
   
  @#^@   �� 
   
  @"`@  �� 
   
  @!b@  �� 
   
  @ d@  �� 
   
  @f@  �� 
   
  @g@  �� 
   
  @h@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@P �@  �� 
   
  @h@  �� 
   
  @g@  �� 
   
  @f@  �� 
   
  @ d@  �� 
   
  @!b@  �� 
   
  @"`@  �� 
   
  @#^@   �� 
   
  @$\@!  �� 
   
  @%Z@"  �� 
   
  @&X@#  �� 
   
  @'V@$  �� 
   
  @(T@%  �� 
   
  @)R@&  �� 
   
  @*P@'  �� 
   
  @+N@(  �� 
   
  @,L@)  �� 
   
  @-J@*  �� 
   
  @.H@+  �� 
   
  @/F@,  �� 
   
  @0D@-  �� 
   
  @1B@.  �� 
   
  @2@@/  �� 
   
  @3>@0  �� 
   
  @4<@1  �� 
   
  @5:@2  �� 
   
  @68@3  �� 
   
  @76@4  �� 
   
  @84@5  �� 
   
  @92@6  �� 
   
  @:0@7  �� 
   
  @;.@8  �� 
   
  @<,@9  �� 
   
  @=*@:  �� 
   
  @>(@;  �� 
   
  @?&@<  �� 
   
  @@$@=  �� 
   
  @A"@>  �� 
   
  @B @?  �� 
   
  @C@@  �� 
   
  @D@A  �� 
   
  @E@B  �� 
   
  @F@C  �� 
   
  @G@D  �� 
   
  @H@E  �� 
   
  @I@F  �� 
   
  @J@G  �� 
   
  @K@H  �� 
   
  @L@I  �� 
   
  @M
@J  �� 
   
  @N@K  �� 
   
  @O@L  �� 
   
  @P@M  �� 
   
  @Q@N  �� 
   
  @R @O  �� 
   
  @S �@P  �� 
   
  @T �@Q  �� 
   
  @U �@R  �� 
   
  @V �@S  �� 
   
  @W �@T  �� 
   
  @X �@U  �� 
   
  @Y �@V  �� 
   
  @Z �@W  �� 
   
  @[ �@X  �� 
   
  @\ �@Y  �� 
   
  @] �@Z  �� 
   
  @_ �@\  �� 
   
  @` �@]  �� 
   
  @a �@^  �� 
   
  @b �@_  �� 
   
  @d �@a  �� 
   
  @e �@b  �� 
   
  @f �@c  �� 
   
  @h �@e  �� 
   
  @i �@f  �� 
   
  @j �@g  �� 
   
  @l �@i  �� 
   
  @m �@j  �� 
   
  @o �@l  �� 
   
  @p �@m  �� 
   
  @r �@o  �� 
   
  @s �@p  �� 
   
  @u �@r  �� 
   
  @w �@t  �� 
   
  @x �@u  �� 
   
  @z �@w  �� 
   
  @| �@y  �� 
   
  @~ �@{  �� 
   
  @� �@}  �� 
   
  @� �@  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� |@�  �� 
   
  @� v@�  �� 
   
  @� p@�  �� 
   
  @� j@�  �� 
   
  @� b@�  �� 
   
  @� \@�  �� 
   
  @� T@�  �� 
   
  @� J@�  �� 
   
  @� @@�  �� 
   
  @� 4@�  �� 
   
  @� %@� 7 ��      A�