  ŠV  A€  k$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1297421869
$MTIME = 1297421869
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2021-02-10T17:57:49' / Date FITS file was generated
IRAF-TLM= '2021-02-10T17:57:52' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  30. / [s] Requested exposure duration
EXPTIME =                  30. / [s] Exposure duration
EXPDUR  =                  30. / [s] Exposure duration
DARKTIME=             31.28228 / [s] Dark time
OBSID   = 'ct4m20210204t045746' / Unique Observation ID
DATE-OBS= '2021-02-04T04:57:46.094903' / DateTime of observation start (UTC)
TIME-OBS= '04:57:46.094903'    / Time of observation start (UTC)
MJD-OBS =       59249.20678351 / MJD of observation start
MJD-END =      59249.207130732 / Exposure end
OPENSHUT= '2021-02-04T04:57:46.246840' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               963462 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'Lyman Alpha Galaxies in the Epoch of Reionization' / Current observin
OBSERVER= 'A. Walker, J. Rhoads, W.Hu' / Observer name(s)
PROPOSER= 'Malhotra'           / Proposal Principle Investigator
DTPI    = 'Malhotra'           / Proposal Principle Investigator (iSTB)
PROPID  = '2018B-0327'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'N964 DECam c0008 9645.0 94.0' / Unique filter identifier
INSTANCE= 'DECam_20210203'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '10:03:14.486'       / [HH:MM:SS] Target RA
DEC     = '02:54:13.918'       / [DD:MM:SS] Target DEC
TELRA   = '10:03:14.508'       / [HH:MM:SS] Telescope RA
TELDEC  = '02:54:14.501'       / [DD:MM:SS] Telescope DEC
HA      = '-00:51:23.920'      / [HH:MM:SS] Telescope hour angle
ZD      =                35.19 / [deg] Telescope zenith distance
AZ      =              22.7458 / [deg] Telescope azimuth angle
DOMEAZ  =               24.112 / [deg] Dome azimuth angle
ZPDELRA =               2.5769 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                 14.7 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-1604.93,-1035.65,2168.28,-54.74,-165.25,-0.00' / DECam hexapod setti
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =               11.104 / [m/s] Wind speed
WINDDIR =                  60. / [deg] Wind direction (from North)
HUMIDITY=                  34. / [%] Ambient relative humidity (outside)
PRESSURE=                 779. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                0.912 / [arcsec] DIMM2 Seeing
MASS2   =                 0.63 /  MASS(2) FSEE
ASTIG1  =                -0.05 / 4MAPS correction 1
ASTIG2  =                 0.04 / 4MAPS correction 2
OUTTEMP =                 16.2 / [deg C] Outside temperature
AIRMASS =                 1.22 / Airmass
GSKYVAR =                0.039 / RASICAM global sky standard deviation
GSKYHOT =                0.102 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               15.025 / [deg C] Mirror surface average temperature
MAIRTEMP=                 15.4 / [deg C] Mirror temperature above surface
UPTRTEMP=               16.045 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 14.3 / [deg C] Mirror top surface temperature
UTN-TEMP=               15.935 / [deg C] Upper truss temperature north
UTS-TEMP=               16.155 / [deg C] Upper truss temperature south
UTW-TEMP=                16.36 / [deg C] Upper truss temperature west
UTE-TEMP=                15.73 / [deg C] Upper truss temperature east
PMN-TEMP=                 14.9 / [deg C] Mirror north edge temperature
PMS-TEMP=                 15.4 / [deg C] Mirror south edge temperature
PMW-TEMP=                 14.6 / [deg C] Mirror west edge temperature
PME-TEMP=                 15.2 / [deg C] Mirror east edge temperature
DOMELOW = 'NaN     '           / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                 11.1 / [deg C] Dome floor temperature
G-MEANX =              -0.0113 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0603 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.28,0.55,-9.04,0.37,-0.20,0.25,-0.00,0.03,-0.39,]' / Mean Wavefront
DONUTFS3= '[]      '           / Mean Wavefront for Sensor FS3
DONUTFS2= '[]      '           / Mean Wavefront for Sensor FS2
DONUTFS1= '[1.51,0.30,8.44,0.41,0.00,0.12,-0.16,0.37,0.18,]' / Mean Wavefront fo
G-FLXVAR=           991420.423 / [arcsec] Guider mean guide star flux variances
G-MEANXY=            -0.000565 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[1.42,0.19,-9.11,0.04,0.02,0.06,-0.33,0.38,-0.10,]' / Mean Wavefront
DONUTFN2= '[1.20,0.48,8.51,-0.35,-0.07,0.06,-0.17,0.30,0.11,]' / Mean Wavefront
DONUTFN3= '[2.27,0.22,-9.07,-0.02,0.05,0.12,-0.34,0.17,0.42,]' / Mean Wavefront
DONUTFN4= '[2.44,1.13,8.44,0.09,-0.27,0.11,-0.15,0.06,0.35,]' / Mean Wavefront f
HIERARCH TIME_RECORDED = '2021-02-04T04:58:38.361251'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    3 / Number of guide CCDs that remained active
DOXT    =                 0.08 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.0921 / [arcsec] Guider x-axis maximum offset
FADZ    =                52.59 / [um] FA Delta focus.
FADY    =              -450.17 / [um] FA Delta Y.
FADX    =              -869.98 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                -2.45 / [arcsec] FA Delta Y-theta.
DODZ    =                52.59 / [um] Delta-Z from donut analysis
DODY    =                -1.68 / [um] Y-decenter from donut analysis
DODX    =                -0.48 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2021-02-04T04:57:36' / Time of last RASICAM exposure (UTC)
G-SEEING=                1.764 / [arcsec] Guider average seeing
G-TRANSP=                0.656 / Guider average sky transparency
G-MEANY2=             0.019737 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.09 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.302 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                13.09 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.2926 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.006968 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:89'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
ODATEOBS= '2021-02-04T04:57:46.094903'
DTINSTRU= 'decam   '
DTCALDAT= '2021-02-03'
DTTELESC= 'ct4m    '
DTACQNAM= '/data_local/images/DTS/2018B-0327/DECam_00963462.fits.fz'
DTNSANAM= 'c4d_210204_045746_ori.fits.fz'
DTPROPID= '2018B-0327'
DTSITE  = 'ct      '
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.72214269389129
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     41942.3544014618
GAINB   =     3.66495131000586
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     38719.3987032186
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 0x0 4.010000' / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =   1.292520097439E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -1.085639046593E-02 / Projection distortion parameter
PV2_9   =   6.447478695312E-03 / Projection distortion parameter
CD1_1   =  -3.3360200283162E-6 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -5.858192904999E-03 / Projection distortion parameter
PV2_1   =   1.022925495553E+00 / Projection distortion parameter
PV2_2   =  -9.897966362568E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -2.552170798154E-02 / Projection distortion parameter
PV2_5   =   2.041675948973E-02 / Projection distortion parameter
PV2_6   =  -9.413646840431E-03 / Projection distortion parameter
PV2_7   =   9.366132237292E-03 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   1.074538809323E-02 / Projection distortion parameter
PV2_10  =  -2.902607846633E-03 / Projection distortion parameter
PV1_4   =   7.981504903317E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -1.061873720196E-02 / Projection distortion parameter
PV1_1   =   1.015184874705E+00 / Projection distortion parameter
PV1_0   =   4.354499191785E-03 / Projection distortion parameter
PV1_9   =   7.785488836336E-03 / Projection distortion parameter
PV1_8   =  -7.287409639159E-03 / Projection distortion parameter
CD1_2   =  0.00466273978526977 / Linear projection matrix
PV1_5   =  -2.009767593706E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046630629593158 / Linear projection matrix
CD2_2   =  -3.3493158303264E-6 / Linear projection matrix
PV1_10  =  -3.686927828818E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   1.508055497106E+02 / World coordinate on this axis
CRVAL2  =   2.899574710916E+00 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Tue Feb  9 09:15:48 2021' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Tue Feb  9 09:15:48 2021' / overscan corrected
BIASFIL = 'DEC21A_LAGER20210203_99cf08e-59248Z_01.fits' / Bias correction file
BAND    = 'N964    '
NITE    = '20210203'
DESBIAS = 'Tue Feb  9 09:28:45 2021'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Tue Feb  9 09:28:47 2021'
DESBPM  = 'Tue Feb  9 09:28:47 2021' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Tue Feb  9 09:28:47 2021' / Flag saturated pixels
NSATPIX =                    5 / Number of saturated pixels
FLATMEDA=     3.72214269389129
FLATMEDB=     3.66495131000586
SATURATE=     41942.3544014618
DESGAINC= 'Tue Feb  9 09:28:47 2021'
FLATFIL = 'DEC21A_LAGER20210203_99cf08e-59248N964F_01.fits' / Dome flat correcti
DESFLAT = 'Tue Feb  9 09:28:47 2021'
STARFIL = 'STARFLAT_20210128_99bacc2-N964P_ci_01.fits' / Star flat correction fi
DESSTAR = 'Tue Feb  9 09:28:47 2021'
HISTORY Tue Feb  9 09:28:48 2021 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
AMPMTCH = 'B 1.0012 (0.0005,0.0050)' / Amp match
DESBLEED= 'Tue Feb 9 10:36:02 2021' / bleed trail masking
NBLEED  = 39  / Number of bleed trail pixels.
STARMASK= 'Tue Feb 9 10:36:02 2021' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC21A_LAGER20210203_9
HISTORY 9d1213-filN964-ccd01N964/DEC21A_LAGER20210203_99d1213-N964-210204045746_
HISTORY 01.fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Feb  9 10:36 Bad pixel file is omibleed_01_bpm.pl'
FWHM    =             4.409146 / Median FWHM in pixels
ELLIPTIC=            0.1213768 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Tue Feb  9 10:51:26 2021' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
ASTRMREF= 'Gaia-2  '           / Astrometric ref. catalog
PHOTREF = 'PS1     '           / Photometric ref. catalog
MAGZERO =               25.346 / [mag] Magnitude zero point
MAGZPT  =               21.653 / [mag] Magnitude zero point per sec
NPHTMTCH=                 4215 / Number of phot. stars matched
RADIUS  =                 2.42 / [pix] Average radius of stars
RADSTD  =                0.144 / [pix] Standard deviation of star radii
CATALOG = 'omimask_01_cat1.txt'
OBJMASK = 'omimask_01_obm1.pl'
AVSKY   =             23.64429
AVSIG   =             3.167922
ILLMASK = 'DEC21A_LAGER20210203_99d1213-N964-210204045746_01_obm.pl'
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             23.63406
NCOMBINE=                 4200
PUPILSKY=     23.7669826129725
PUPILSUM=     95406.8638106295
PUPILMAX=     1.96536552906036
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'M3l1P3l0M3l0M3l0'   / HDU checksum updated 2021-02-10T17:53:54
DATASUM = '4227876390'         / data unit checksum updated 2021-02-10T17:53:54
NPUPSCL =                88610
PUPSCL  =     0.92356229058225
   Ț     Ą  Ì                 Ô  Ô  Ì  o  k      êÿÿÿ               ÿo       @*``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
 
@`
 
@`
`
 
@`
 
@`
`````````````````````````````````` @` @` @ @ @ @ @ @ @ @ @` @` @``````````````````````````````````````
 
@`
 
@`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@3   ÿ      Ą * ÿ      AĄ  ÿ 
   
  @À #@Ÿ  ÿ 
   
  @ž 4@”  ÿ 
   
  @± A@Ż  ÿ 
   
  @Ź K@Ș  ÿ 
   
  @š T@„  ÿ 
   
  @€ \@Ą  ÿ 
   
  @  c@  ÿ 
   
  @ j@  ÿ 
   
  @ q@  ÿ 
   
  @ w@  ÿ 
   
  @ |@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ą@~  ÿ 
   
  @ Š@|  ÿ 
   
  @} Ș@z  ÿ 
   
  @{ ź@x  ÿ 
   
  @y Č@v  ÿ 
   
  @w ”@u  ÿ 
   
  @u č@s  ÿ 
   
  @t Œ@q  ÿ 
   
  @r ż@p  ÿ 
   
  @p Ă@n  ÿ 
   
  @o Æ@l  ÿ 
   
  @m É@k  ÿ 
   
  @l Ì@i  ÿ 
   
  @j Ï@h  ÿ 
   
  @i Ò@f  ÿ 
   
  @g Ő@e  ÿ 
   
  @f Ś@d  ÿ 
   
  @e Ú@b  ÿ 
   
  @c Ę@a  ÿ 
   
  @b ß@`  ÿ 
   
  @a â@^  ÿ 
   
  @_ ć@]  ÿ 
   
  @^ ç@\  ÿ 
   
  @] ê@Z  ÿ 
   
  @\ ì@Y  ÿ 
   
  @[ î@X  ÿ 
   
  @Y ń@W  ÿ 
   
  @X ó@V  ÿ 
   
  @W ő@U  ÿ 
   
  @V ś@T  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S ț@P  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@L  ÿ 
   
  @N@K  ÿ 
   
  @M
@J  ÿ 
   
  @L@I  ÿ 
   
  @K@H  ÿ 
   
  @J@G  ÿ 
   
  @I@G  ÿ 
   
  @H@F  ÿ 
   
  @G@E  ÿ 
   
  @F@D  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@A  ÿ 
   
  @C@A  ÿ 
   
  @B@@  ÿ 
   
  @A!@?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?%@=  ÿ 
   
  @>'@<  ÿ 
   
  @>(@;  ÿ 
   
  @=*@:  ÿ 
   
  @<+@:  ÿ 
   
  @;-@9  ÿ 
   
  @;.@8  ÿ 
   
  @:/@8  ÿ 
   
  @91@7  ÿ 
   
  @92@6  ÿ 
   
  @83@6  ÿ 
   
  @75@5  ÿ 
   
  @76@4  ÿ 
   
  @67@4  ÿ 
   
  @68@3  ÿ 
   
  @5:@2  ÿ 
   
  @4;@2  ÿ 
   
  @4<@1  ÿ 
   
  @3=@1  ÿ 
   
  @3>@0  ÿ 
   
  @2@@/  ÿ 
   
  @1A@/  ÿ 
   
  @1B@.  ÿ 
   
  @0C@.  ÿ 
   
  @0D@-  ÿ 
   
  @/E@-  ÿ 
   
  @/F@,  ÿ 
   
  @.G@,  ÿ 
   
  @.H@+  ÿ 
   
  @-I@+  ÿ 
   
  @-J@*  ÿ 
   
  @,K@*  ÿ 
   
  @,L@)  ÿ 
   
  @+M@)  ÿ 
   
  @+N@(  ÿ 
   
  @*O@(  ÿ 
   
  @*P@'  ÿ 
   
  @)Q@'  ÿ 
   
  @)R@&  ÿ 
   
  @(S@&  ÿ 
   
  @(T@%  ÿ 
   
  @'U@%  ÿ 
   
  @'V@$  ÿ 
   
  @&W@$  ÿ 
   
  @&X@#  ÿ 
   
  @%Y@#  ÿ 
   
  @%Z@"  ÿ 
   
  @$[@"  ÿ 
   
  @$\@!  ÿ 
   
  @#]@!  ÿ 
   
  @#^@   ÿ      @" „@ „@   ÿ      @" Ą@ ą@  ÿ      @" @$ @  ÿ      @! @) @  ÿ      @! @. @  ÿ      @! @2 @  ÿ      @  @6 @  ÿ      @  @: @  ÿ      @  @= @  ÿ      @  @@ @  ÿ      @ @C @  ÿ      @ @E @  ÿ      @ @H @  ÿ      @ @J @  ÿ      @ @L @  ÿ      @ @N @  ÿ      @ @P @  ÿ      @ @R @  ÿ      @ @T @  ÿ      @ @V @  ÿ      @ @W @  ÿ      @ @Y @  ÿ      @ @[ @  ÿ      @ @\ @  ÿ      @ @] @  ÿ      @ @_ @  ÿ      @ @` @  ÿ      @ @a @  ÿ      @ @b @  ÿ      @ @c @  ÿ      @ @d @  ÿ      @ @e @  ÿ      @ @f @  ÿ      @ @g @  ÿ      @ @h @  ÿ      @ @i @  ÿ      @ @j @  ÿ      @ @k @  ÿ      @ @l @  ÿ      @ @m @  ÿ      @ @n @  ÿ      @ @o @  ÿ      @ @o @  ÿ      @ @p @  ÿ      @ @o @  ÿ      @ @o @  ÿ      @ @n @  ÿ      @ @m @  ÿ      @ @l @  ÿ      @ @k @  ÿ      @ @j @  ÿ      @ @i @  ÿ      @ @h @  ÿ      @ @g @  ÿ      @ @f @  ÿ      @ @e @  ÿ      @ @d @  ÿ      @ @c @  ÿ      @ @b @  ÿ      @ @a @  ÿ      @ @` @  ÿ      @ @_ @  ÿ      @ @] @  ÿ      @ @\ @  ÿ      @ @[ @  ÿ      @ @Y @  ÿ      @ @X @  ÿ      @ @V @  ÿ      @ @T @  ÿ      @ @R @  ÿ      @ @Q @  ÿ      @ @O @  ÿ      @ @M @  ÿ      @ @J @  ÿ      @ @H @  ÿ      @ @E @  ÿ      @ @C @  ÿ      @  @@ @  ÿ      @  @= @  ÿ      @  @: @  ÿ      @  @7 @  ÿ      @! @3 @  ÿ      @! @/ @  ÿ      @! @* @  ÿ      @" @% @  ÿ      @" Ą@ Ą@  ÿ      @" „@ €@   ÿ      @# Ź@ Ź@   ÿ 
   
  @#^@   ÿ 
   
  @#]@!  ÿ 
   
  @$\@!  ÿ 
   
  @$[@"  ÿ 
   
  @%Z@"  ÿ 
   
  @%Y@#  ÿ 
   
  @&X@#  ÿ 
   
  @&W@$  ÿ 
   
  @'V@$  ÿ 
   
  @'U@%  ÿ 
   
  @(T@%  ÿ 
   
  @(S@&  ÿ 
   
  @)R@&  ÿ 
   
  @)Q@'  ÿ 
   
  @*P@'  ÿ 
   
  @*O@(  ÿ 
   
  @+N@(  ÿ 
   
  @+M@)  ÿ 
   
  @,L@)  ÿ 
   
  @,K@*  ÿ 
   
  @-J@*  ÿ 
   
  @-I@+  ÿ 
   
  @.H@+  ÿ 
   
  @.G@,  ÿ 
   
  @/F@,  ÿ 
   
  @/E@-  ÿ 
   
  @0D@-  ÿ 
   
  @0C@.  ÿ 
   
  @1B@.  ÿ 
   
  @1A@/  ÿ 
   
  @2@@/  ÿ 
   
  @3>@0  ÿ 
   
  @3=@1  ÿ 
   
  @4<@1  ÿ 
   
  @4;@2  ÿ 
   
  @5:@2  ÿ 
   
  @68@3  ÿ 
   
  @67@4  ÿ 
   
  @76@4  ÿ 
   
  @84@5  ÿ 
   
  @83@6  ÿ 
   
  @92@6  ÿ 
   
  @:0@7  ÿ 
   
  @:/@8  ÿ 
   
  @;.@8  ÿ 
   
  @<,@9  ÿ 
   
  @<+@:  ÿ 
   
  @=*@:  ÿ 
   
  @>(@;  ÿ 
   
  @?&@<  ÿ 
   
  @?%@=  ÿ 
   
  @@$@=  ÿ 
   
  @A"@>  ÿ 
   
  @B @?  ÿ 
   
  @B@@  ÿ 
   
  @C@A  ÿ 
   
  @D@A  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @G@D  ÿ 
   
  @G@E  ÿ 
   
  @H@F  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@H  ÿ 
   
  @L@I  ÿ 
   
  @M
@J  ÿ 
   
  @N@K  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S ț@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ű@S  ÿ 
   
  @W ő@U  ÿ 
   
  @X ó@V  ÿ 
   
  @Z đ@W  ÿ 
   
  @[ î@X  ÿ 
   
  @\ ì@Y  ÿ 
   
  @] ê@Z  ÿ 
   
  @^ ç@\  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @b à@_  ÿ 
   
  @d Ü@a  ÿ 
   
  @e Ú@b  ÿ 
   
  @f Ű@c  ÿ 
   
  @h Ô@e  ÿ 
   
  @i Ò@f  ÿ 
   
  @k Î@h  ÿ 
   
  @l Ì@i  ÿ 
   
  @n È@k  ÿ 
   
  @o Æ@l  ÿ 
   
  @q Â@n  ÿ 
   
  @r À@o  ÿ 
   
  @t Œ@q  ÿ 
   
  @v ž@s  ÿ 
   
  @w ”@u  ÿ 
   
  @y Č@v  ÿ 
   
  @{ ź@x  ÿ 
   
  @} Ș@z  ÿ 
   
  @ Š@|  ÿ 
   
  @ ą@~  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ |@  ÿ 
   
  @ v@  ÿ 
   
  @ p@  ÿ 
   
  @ j@  ÿ 
   
  @  d@  ÿ 
   
  @€ \@Ą  ÿ 
   
  @š T@„  ÿ 
   
  @Ź L@©  ÿ 
   
  @± B@ź  ÿ 
   
  @· 5@”  ÿ 
   
  @ż %@œ 3 ÿ      AĄ