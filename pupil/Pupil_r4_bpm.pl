  �V  F�  
�$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1279278404
$MTIME = 1279278404
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-07-15T17:49:48' / Date FITS file was generated
IRAF-TLM= '2020-07-15T17:49:29' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'RAW     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  90. / [s] Requested exposure duration
EXPTIME =                  90. / [s] Exposure duration
EXPDUR  =                  90. / [s] Exposure duration
DARKTIME=           91.1449299 / [s] Dark time
OBSID   = 'ct4m20131122t014745' / Unique Observation ID
DATE-OBS= '2013-11-22T01:47:45.893212' /  UTC epoch
TIME-OBS= '01:47:45.893212'    / Time of observation start (UTC)
MJD-OBS =       56618.07483673 / MJD of observation start
MJD-END =      56618.075878397 / Exposure end
OPENSHUT= '2013-11-22T01:47:45.979160' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               257231 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'survey  '           / Current observing orogram
OBSERVER= 'Pujol, Reil, March' / Observer name(s)
PROPOSER= 'Frieman '           / Proposal Principle Investigator
DTPI    = 'Frieman           ' /  Principal Investigator
PROPID  = '2012B-0001'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
HEX     =                 2575 / DES Hex number
TILING  =                    3 / DES Tiling number
SEQID   = '68+37 enqueued on 2013-11-22 01:33:05Z by SurveyTactician' / Sequence
SEQNUM  =                    1 / Number of image in sequence
SEQTOT  =                    1 / Total number of images in sequence
AOS     =                    T / AOS data available if true
BCAM    =                    T / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'r DECam SDSS c0002 6415.0 1480.0' / Unique filter identifier
FILTPOS = 'cassette_4'         / Filter position in FCM
INSTANCE= 'DECam_20131121'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '00:25:00.977'       / [HH:MM:SS] Target RA
DEC     = '02:49:45.869'       / [DD:MM:SS] Target DEC
TELRA   = '00:25:01.031'       / [HH:MM:SS] Telescope RA
TELDEC  = '02:49:46.999'       / [DD:MM:SS] Telescope DEC
HA      = '00:43:36.740'       / [HH:MM:SS] Telescope hour angle
ZD      =                34.67 / [deg] Telescope zenith distance
AZ      =               340.69 / [deg] Telescope azimuth angle
DOMEAZ  =                 340. / [deg] Dome azimuth angle
TELFOCUS= '1319.36,-3113.66,2277.68,118.39,-72.43, 0.00' / DECam hexapod setting
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    T / RASICAM local sky clear flag
WINDSPD =                9.012 / [m/s] Wind speed
WINDDIR =                 182. / [deg] Wind direction (from North)
HUMIDITY=                  23. / [%] Ambient relative humidity (outside)
PRESSURE=                 780. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
OUTTEMP =                 14.8 / [deg C] Outside temperature
AIRMASS =                 1.22 / Airmass
GSKYVAR =                 0.03 / RASICAM global sky standard deviation
GSKYHOT =                 0.02 / RASICAM global sky fraction above threshold
LSKYVAR =                 0.01 / RASICAM local sky standard deviation
LSKYHOT =                   0. / RASICAM local sky fraction above threshold
LSKYPOW =                   0. / RASICAM local sky normalized power
MSURTEMP=               12.012 / [deg C] Mirror surface average temperature
MAIRTEMP=                 12.8 / [deg C] Mirror temperature above surface
UPTRTEMP=               15.246 / [deg C] Upper truss average temperature
LWTRTEMP=                -999. / [deg C] Lower truss average temperature
PMOSTEMP=                 13.4 / [deg C] Mirror top surface temperature
UTN-TEMP=               15.175 / [deg C] Upper truss temperature north
UTS-TEMP=                15.23 / [deg C] Upper truss temperature south
UTW-TEMP=               15.315 / [deg C] Upper truss temperature west
UTE-TEMP=               15.265 / [deg C] Upper truss temperature east
PMN-TEMP=                 11.4 / [deg C] Mirror north edge temperature
PMS-TEMP=                 12.2 / [deg C] Mirror south edge temperature
PMW-TEMP=                 11.8 / [deg C] Mirror west edge temperature
PME-TEMP=                12.65 / [deg C] Mirror east edge temperature
DOMELOW =                15.43 / [deg C] Low dome temperature
DOMEHIGH=                -999. / [deg C] High dome temperature
DOMEFLOR=                   7. / [deg C] Dome floor temperature
G-MEANX =              -0.0203 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0212 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[0.24,1.53,-8.80,-0.43,-0.05,0.16,0.16,-0.05,-0.47,]' / Mean Wavefron
DONUTFS3= '[-0.21,1.16,8.78,-0.43,0.03,0.12,0.37,0.15,-0.67,]' / Mean Wavefront
DONUTFS2= '[0.53,0.14,-8.77,-0.19,0.02,0.14,0.16,0.36,-0.15,]' / Mean Wavefront
DONUTFS1= '[-0.04,0.69,9.25,-0.15,0.04,0.23,0.25,0.30,-0.09,]' / Mean Wavefront
G-FLXVAR=          4612888.798 / [arcsec] Guider mean guide star flux variances
G-MEANXY=            -0.005206 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[-1.27,0.47,-8.83,-0.62,-0.26,0.18,0.22,0.28,-0.37,]' / Mean Wavefron
DONUTFN2= '[1.14,1.22,8.94,-0.65,-0.31,0.07,0.20,0.39,-0.26,]' / Mean Wavefront
DONUTFN3= '[-0.06,0.34,-8.80,-0.25,-0.40,0.22,0.20,0.05,0.24,]' / Mean Wavefront
DONUTFN4= '[]      '           / Mean Wavefront for Sensor FN4
HIERARCH TIME_RECORDED = '2013-11-22T01:49:38.490826'
G-FEEDBK=                   10 / [%] Guider feedback
G-CCDNUM=                    3 / Number of guide CCDs that remained active
DOXT    =                 0.13 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.2364 / [arcsec] Guider x-axis maximum offset
FADZ    =               -11.67 / [um] FA Delta focus.
FADY    =               164.02 / [um] FA Delta Y.
FADX    =               -136.9 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                 7.55 / [arcsec] FA Delta Y-theta.
DODZ    =               -11.67 / [um] Delta-Z from donut analysis
DODY    =                -0.05 / [um] Y-decenter from donut analysis
DODX    =                -0.79 / [um] X-decenter from donut analysis
BCAMAZ  =                   0. / [arcsec] BCAM hexapod rot. about z-axis
MULTIEXP=                    F / Frame contains multiple exposures if true
BCAMAX  =              -13.236 / [arcsec] BCAM hexapod rot. about x-axis
BCAMAY  =               -15.76 / [arcsec] BCAM hexapod rot. about y-axis
BCAMDY  =             1211.517 / [micron] BCAM hexapod y-offset
BCAMDX  =             -282.716 / [micron] BCAM hexapod x-offset
SKYUPDAT= '2013-11-22T01:44:50' / Time of last RASICAM exposure (UTC)
G-SEEING=                1.796 / [arcsec] Guider average seeing
G-TRANSP=                0.779 / Guider average sky transparency
G-MEANY2=             0.014128 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                 0.39 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.289 / [s] Guider avg. latency between exposures
LUTVER  = 'working-trunk'      / Hexapod Lookup Table version
FAXT    =                 8.03 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.3414 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.015284 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:19'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTSITE  = 'ct                ' /  observatory location
DTTELESC= 'ct4m              ' /  telescope identifier
DTINSTRU= 'decam             ' /  instrument identifier
DTCALDAT= '2013-11-21        ' /  calendar date from observing schedule
ODATEOBS= '                  ' /  previous DATE-OBS
DTUTC   = '2013-11-22T01:50:08' /  post exposure UTC epoch from DTS
DTOBSERV= 'NOAO              ' /  scheduling institution
DTPROPID= '2012B-0001        ' /  observing proposal ID
DTPIAFFL= '                  ' /  PI affiliation
DTTITLE = '                  ' /  title of observing proposal
DTCOPYRI= 'AURA              ' /  copyright holder of data
DTACQUIS= 'pipeline5.ctio.noao.edu' /  host name of data acquisition computer
DTACCOUN= 'sispi             ' /  observing account name
DTACQNAM= '/data_local/images/DTS/2012B-0001/DECam_00257231.fits.fz' /  file na
DTNSANAM= 'dec126949.fits    ' /  file name in NOAO Science Archive
DTQUEUE = 'des               ' /  DTS queue (12890)
DTSTATUS= 'done              ' /  data transport status
SB_HOST = 'pipeline5.ctio.noao.edu' /  iSTB client host
SB_ACCOU= 'sispi             ' /  iSTB client user account
SB_SITE = 'ct                ' /  iSTB host site
SB_LOCAL= 'dec               ' /  locale of iSTB daemon
SB_DIR1 = '20131121          ' /  level 1 directory in NSA DS
SB_DIR2 = 'ct4m              ' /  level 2 directory in NSA DS
SB_DIR3 = '2012B-0001        ' /  level 3 directory in NSA DS
SB_RECNO=               126949 /  iSTB sequence number
SB_ID   = 'dec126949         ' /  unique iSTB identifier
SB_NAME = 'dec126949.fits    ' /  name assigned by iSTB
RMCOUNT =                    0 /  remediation counter
RECNO   =               126949 /  NOAO Science Archive sequence number
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.82723501049547
RDNOISEA=        6.67373068433 / [electrons] Read noise for amp A
SATURATA=     44588.0658372524
GAINB   =     3.93196553375187
RDNOISEB=        6.52878464819 / [electrons] Read noise for amp B
SATURATB=     40704.0465999979
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =  -5.024944166180E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -7.518309987306E-03 / Projection distortion parameter
PV2_9   =   7.645285959324E-03 / Projection distortion parameter
CD1_1   =  -3.8612803798131E-6 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -1.103682655404E-02 / Projection distortion parameter
PV2_1   =   1.041075406116E+00 / Projection distortion parameter
PV2_2   =  -7.615730991121E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -4.706045845514E-02 / Projection distortion parameter
PV2_5   =   1.489192771398E-02 / Projection distortion parameter
PV2_6   =  -1.029692349197E-02 / Projection distortion parameter
PV2_7   =   1.792576002572E-02 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   4.334730613726E-03 / Projection distortion parameter
PV2_10  =  -2.834364628169E-03 / Projection distortion parameter
PV1_4   =   7.385558275568E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -4.868799610262E-03 / Projection distortion parameter
PV1_1   =   1.014905349604E+00 / Projection distortion parameter
PV1_0   =   2.480042277244E-03 / Projection distortion parameter
PV1_9   =   8.010888416329E-03 / Projection distortion parameter
PV1_8   =  -7.273053780327E-03 / Projection distortion parameter
CD1_2   =  0.00466250630767169 / Linear projection matrix
PV1_5   =  -2.019882907422E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046627527327162 / Linear projection matrix
CD2_2   =  -3.8604637917734E-6 / Linear projection matrix
PV1_10  =  -1.341296365071E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   6.255628725095E+00 / World coordinate on this axis
CRVAL2  =   2.833931608153E+00 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Wed Jul  3 20:17:36 2019' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Wed Jul  3 20:17:36 2019' / overscan corrected
BIASFIL = 'DECALS_20131121_8e4cfef-56617Z_01.fits' / Bias correction file
BAND    = 'r       '
NITE    = '20131121'
DESBIAS = 'Wed Jul  3 20:19:16 2019'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Wed Jul  3 20:19:19 2019'
DESBPM  = 'Wed Jul  3 20:19:20 2019' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20130716_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Wed Jul  3 20:19:20 2019' / Flag saturated pixels
NSATPIX =                20698 / Number of saturated pixels
FLATMEDA=     3.82723501049547
FLATMEDB=     3.93196553375187
SATURATE=     44588.0658372524
DESGAINC= 'Wed Jul  3 20:19:20 2019'
FLATFIL = 'DECALS_20131121_8e4cfef-56617rF_01.fits' / Dome flat correction file
DESFLAT = 'Wed Jul  3 20:19:22 2019'
STARFIL = 'STARFLAT_20131115_94b995c-rP_ci_01.fits' / Star flat correction file
DESSTAR = 'Wed Jul  3 20:19:24 2019'
HISTORY Wed Jul  3 20:19:25 2019 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Wed Jul 3 20:28:12 2019' / bleed trail masking
NBLEED  = 58070  / Number of bleed trail pixels.
STARMASK= 'Wed Jul 3 20:28:12 2019' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DECALS_DR9_94fbe54-fil
HISTORY r-ccd01r/DECALS_DR9_94fbe54-r-131122014745_01.fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FWHM    =             4.241051 / Median FWHM in pixels
ELLIPTIC=           0.09194347 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Wed Jul  3 20:41:18 2019' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
PHOTREF = 'PS1     '           / Photometric ref. catalog
MAGZERO =               30.182 / [mag] Magnitude zero point
MAGZPT  =               25.296 / [mag] Magnitude zero point per sec
NPHTMTCH=                 4191 / Number of phot. stars matched
RADIUS  =                 2.14 / [pix] Average radius of stars
RADSTD  =                0.064 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             325.5286
NCOMBINE=                59001
PUPILSKY=     326.408672151102
PUPILSUM=     188758.879124785
PUPILMAX=     3.67421627044678
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= '98cLA6cL36cL96cL'   / HDU checksum updated 2020-07-15T15:17:51
DATASUM = '3529639221'         / data unit checksum updated 2020-07-15T15:17:51
PUPSCL  =     0.46987778009062
   �     �  �                 	e  	e  �  9  
�      ����               ��9       @,``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
 
@`
`
 
@`
`
 
@`
 
@`
 
@`
 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@`
`````` @ @` @ @```````` @`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
 
@`
`
 
@`
`
 
@`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@7   ��      � , ��      A�  �� 
   
  @� %@�  �� 
   
  @� 4@�  �� 
   
  @� @@�  �� 
   
  @� J@�  �� 
   
  @� T@�  �� 
   
  @� \@�  �� 
   
  @� b@�  �� 
   
  @� j@�  �� 
   
  @� p@�  �� 
   
  @� v@�  �� 
   
  @� |@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@  �� 
   
  @� �@}  �� 
   
  @~ �@{  �� 
   
  @| �@y  �� 
   
  @z �@w  �� 
   
  @x �@u  �� 
   
  @w �@t  �� 
   
  @u �@r  �� 
   
  @s �@p  �� 
   
  @r �@o  �� 
   
  @p �@m  �� 
   
  @o �@l  �� 
   
  @m �@j  �� 
   
  @l �@i  �� 
   
  @j �@g  �� 
   
  @i �@f  �� 
   
  @h �@e  �� 
   
  @f �@c  �� 
   
  @e �@b  �� 
   
  @d �@a  �� 
   
  @b �@_  �� 
   
  @a �@^  �� 
   
  @` �@]  �� 
   
  @_ �@\  �� 
   
  @] �@Z  �� 
   
  @\ �@Y  �� 
   
  @[ �@X  �� 
   
  @Z �@W  �� 
   
  @Y �@V  �� 
   
  @X �@U  �� 
   
  @W �@T  �� 
   
  @V �@S  �� 
   
  @U �@R  �� 
   
  @T �@Q  �� 
   
  @S �@P  �� 
   
  @R @O  �� 
   
  @Q@N  �� 
   
  @P@M  �� 
   
  @O@L  �� 
   
  @N@K  �� 
   
  @M
@J  �� 
   
  @L@I  �� 
   
  @K@H  �� 
   
  @J@G  �� 
   
  @I@F  �� 
   
  @H@E  �� 
   
  @G@D  �� 
   
  @F@C  �� 
   
  @E@B  �� 
   
  @D@A  �� 
   
  @C@@  �� 
   
  @B @?  �� 
   
  @A"@>  �� 
   
  @@$@=  �� 
   
  @?&@<  �� 
   
  @>(@;  �� 
   
  @=*@:  �� 
   
  @<,@9  �� 
   
  @;.@8  �� 
   
  @:0@7  �� 
   
  @92@6  �� 
   
  @84@5  �� 
   
  @76@4  �� 
   
  @68@3  �� 
   
  @5:@2  �� 
   
  @4<@1  �� 
   
  @3>@0  �� 
   
  @2@@/  �� 
   
  @1B@.  �� 
   
  @0D@-  �� 
   
  @/F@,  �� 
   
  @.H@+  �� 
   
  @-J@*  �� 
   
  @,L@)  �� 
   
  @+N@(  �� 
   
  @*P@'  �� 
   
  @)R@&  �� 
   
  @(T@%  �� 
   
  @'V@$  �� 
   
  @&X@#  �� 
   
  @%Z@"  �� 
   
  @$\@!  �� 
   
  @#^@   �� 
   
  @"`@  �� 
   
  @!b@  �� 
   
  @ d@  �� 
   
  @f@  �� 
   
  @g@  �� 
   
  @h@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@P �@  �� 
   
  @h@  �� 
   
  @g@  �� 
   
  @f@  �� 
   
  @ d@  �� 
   
  @!b@  �� 
   
  @"`@  �� 
   
  @#^@   �� 
   
  @$\@!  �� 
   
  @%Z@"  �� 
   
  @&X@#  �� 
   
  @'V@$  �� 
   
  @(T@%  �� 
   
  @)R@&  �� 
   
  @*P@'  �� 
   
  @+N@(  �� 
   
  @,L@)  �� 
   
  @-J@*  �� 
   
  @.H@+  �� 
   
  @/F@,  �� 
   
  @0D@-  �� 
   
  @1B@.  �� 
   
  @2@@/  �� 
   
  @3>@0  �� 
   
  @4<@1  �� 
   
  @5:@2  �� 
   
  @68@3  �� 
   
  @76@4  �� 
   
  @84@5  �� 
   
  @92@6  �� 
   
  @:0@7  �� 
   
  @;.@8  �� 
   
  @<,@9  �� 
   
  @=*@:  �� 
   
  @>(@;  �� 
   
  @?&@<  �� 
   
  @@$@=  �� 
   
  @A"@>  �� 
   
  @B @?  �� 
   
  @C@@  �� 
   
  @D@A  �� 
   
  @E@B  �� 
   
  @F@C  �� 
   
  @G@D  �� 
   
  @H@E  �� 
   
  @I@F  �� 
   
  @J@G  �� 
   
  @K@H  �� 
   
  @L@I  �� 
   
  @M
@J  �� 
   
  @N@K  �� 
   
  @O@L  �� 
   
  @P@M  �� 
   
  @Q@N  �� 
   
  @R @O  �� 
   
  @S �@P  �� 
   
  @T �@Q  �� 
   
  @U �@R  �� 
   
  @V �@S  �� 
   
  @W �@T  �� 
   
  @X �@U  �� 
   
  @Y �@V  �� 
   
  @Z �@W  �� 
   
  @[ �@X  �� 
   
  @\ �@Y  �� 
   
  @] �@Z  �� 
   
  @_ �@\  �� 
   
  @` �@]  �� 
   
  @a �@^  �� 
   
  @b �@_  �� 
   
  @d �@a  �� 
   
  @e �@b  �� 
   
  @f �@c  �� 
   
  @h �@e  �� 
   
  @i �@f  �� 
   
  @j �@g  �� 
   
  @l �@i  �� 
   
  @m �@j  �� 
   
  @o �@l  �� 
   
  @p �@m  �� 
   
  @r �@o  �� 
   
  @s �@p  �� 
   
  @u �@r  �� 
   
  @w �@t  �� 
   
  @x �@u  �� 
   
  @z �@w  �� 
   
  @| �@y  �� 
   
  @~ �@{  �� 
   
  @� �@}  �� 
   
  @� �@  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� |@�  �� 
   
  @� v@�  �� 
   
  @� p@�  �� 
   
  @� j@�  �� 
   
  @� b@�  �� 
   
  @� \@�  �� 
   
  @� T@�  �� 
   
  @� J@�  �� 
   
  @� @@�  �� 
   
  @� 4@�  �� 
   
  @� %@� 7 ��      A�