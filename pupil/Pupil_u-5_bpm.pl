  ŠV  A  Ț$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1288789450
$MTIME = 1288789450
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-11-02T20:04:10' / Date FITS file was generated
IRAF-TLM= '2020-11-02T20:04:13' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  60. / [s] Requested exposure duration
EXPTIME =                  60. / [s] Exposure duration
EXPDUR  =                  60. / [s] Exposure duration
DARKTIME=           61.2261701 / [s] Dark time
OBSID   = 'ct4m20201009t042623' / Unique Observation ID
DATE-OBS= '2020-10-09T04:26:23.021539' / DateTime of observation start (UTC)
TIME-OBS= '04:26:23.021539'    / Time of observation start (UTC)
MJD-OBS =       59131.18498868 / MJD of observation start
MJD-END =      59131.185683125 / Exposure end
OPENSHUT= '2020-10-09T04:26:23.187180' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               944036 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'derositas'          / Current observing orogram
OBSERVER= 'Vivas, Zenteno'     / Observer name(s)
PROPOSER= 'Zenteno '           / Proposal Principle Investigator
DTPI    = 'Zenteno '           / Proposal Principle Investigator (iSTB)
PROPID  = '2020B-0241'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
SEQID   = 'DEROSITAS scheduled: 2020/10/09 04:24:00.0000' / Sequence name
SEQNUM  =                    1 / Number of image in sequence
SEQTOT  =                    2 / Total number of images in sequence
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    F / Cloud camera (RASICAM) available if true
FILTER  = 'u DECam c0006 3500.0 1000.0' / Unique filter identifier
FILTPOS = 'cassette_5'         / Filter position in FCM
INSTANCE= 'DECam_20201008'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '01:08:00.240'       / [HH:MM:SS] Target RA
DEC     = '-55:42:03.600'      / [DD:MM:SS] Target DEC
TELRA   = '01:08:00.290'       / [HH:MM:SS] Telescope RA
TELDEC  = '-55:42:00.893'      / [DD:MM:SS] Telescope DEC
HA      = '-00:12:47.190'      / [HH:MM:SS] Telescope hour angle
ZD      =                25.53 / [deg] Telescope zenith distance
AZ      =             175.7464 / [deg] Telescope azimuth angle
DOMEAZ  =              188.948 / [deg] Dome azimuth angle
ZPDELRA =              -9.6479 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                  6.2 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '98.21,2903.80,2572.58,-281.52,-117.53, 0.00' / DECam hexapod settings
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =                   0. / [m/s] Wind speed
WINDDIR = 'NaN     '           / [deg] Wind direction (from North)
HUMIDITY= 'NaN     '           / [%] Ambient relative humidity (outside)
PRESSURE= 'NaN     '           / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE= 'NaN     '           / [arcsec] DIMM2 Seeing
MASS2   = 'NaN     '           /  MASS(2) FSEE
ASTIG1  =                 0.09 / 4MAPS correction 1
ASTIG2  =                 0.01 / 4MAPS correction 2
OUTTEMP = 'NaN     '           / [deg C] Outside temperature
AIRMASS =                 1.11 / Airmass
GSKYVAR = 'NaN     '           / RASICAM global sky standard deviation
GSKYHOT = 'NaN     '           / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=                13.05 / [deg C] Mirror surface average temperature
MAIRTEMP=                 13.3 / [deg C] Mirror temperature above surface
UPTRTEMP=               14.082 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 12.5 / [deg C] Mirror top surface temperature
UTN-TEMP=                13.56 / [deg C] Upper truss temperature north
UTS-TEMP=                 14.7 / [deg C] Upper truss temperature south
UTW-TEMP=               13.855 / [deg C] Upper truss temperature west
UTE-TEMP=               14.215 / [deg C] Upper truss temperature east
PMN-TEMP=                 12.5 / [deg C] Mirror north edge temperature
PMS-TEMP=                 13.3 / [deg C] Mirror south edge temperature
PMW-TEMP=                 12.8 / [deg C] Mirror west edge temperature
PME-TEMP=                 13.6 / [deg C] Mirror east edge temperature
DOMELOW = 'NaN     '           / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                  8.5 / [deg C] Dome floor temperature
G-MEANX =              -0.0047 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0566 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.33,0.70,-8.93,-0.26,-0.20,0.02,-0.51,-0.02,-0.45,]' / Mean Wavefro
DONUTFS3= '[]      '           / Mean Wavefront for Sensor FS3
DONUTFS2= '[1.36,-0.62,-8.92,0.31,-0.05,0.25,-0.42,0.32,-0.11,]' / Mean Wavefron
DONUTFS1= '[2.50,0.35,8.62,0.34,0.14,0.37,-0.27,0.19,0.17,]' / Mean Wavefront fo
G-FLXVAR=           624595.296 / [arcsec] Guider mean guide star flux variances
G-MEANXY=             -0.00431 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[0.17,0.96,-8.80,-0.32,0.15,0.37,0.23,0.21,-0.22,]' / Mean Wavefront
DONUTFN2= '[2.20,0.87,8.65,-0.21,0.08,0.17,0.23,0.14,-0.03,]' / Mean Wavefront f
DONUTFN3= '[]      '           / Mean Wavefront for Sensor FN3
DONUTFN4= '[2.40,0.49,8.58,0.08,-0.06,0.02,0.28,-0.12,0.22,]' / Mean Wavefront f
HIERARCH TIME_RECORDED = '2020-10-09T04:27:43.873935'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    4 / Number of guide CCDs that remained active
DOXT    =                -0.01 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.3295 / [arcsec] Guider x-axis maximum offset
FADZ    =                22.91 / [um] FA Delta focus.
FADY    =              -534.31 / [um] FA Delta Y.
FADX    =              -288.13 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =               -14.18 / [arcsec] FA Delta Y-theta.
DODZ    =                22.91 / [um] Delta-Z from donut analysis
DODY    =                -1.66 / [um] Y-decenter from donut analysis
DODX    =                -0.46 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
G-SEEING=                1.924 / [arcsec] Guider average seeing
G-TRANSP=                 0.52 / Guider average sky transparency
G-MEANY2=             0.015824 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                 0.01 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.319 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                22.63 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.3208 / [arcsec] Guider y-axis maximum offset
G-MEANX2=              0.02302 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:86'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTPROPID= '2020B-0241'
DTCALDAT= '2020-10-08'
DTSITE  = 'ct      '
DTTELESC= 'ct4m    '
DTACQNAM= '/data_local/images/DTS/2020B-0241/DECam_00944036.fits.fz'
DTINSTRU= 'decam   '
ODATEOBS= '2020-10-09T04:26:23.021539'
DTNSANAM= 'c4d_201009_042623_ori.fits.fz'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.22714590950167
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     48375.6955458227
GAINB   =     3.13872616877654
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     45210.9242315057
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 0x0 4.010000' / Monsoon module
SLOT02  = 'DESCB 0x0 4.010000' / Monsoon module
SLOT03  = 'CCD12 0x0 4.080000' / Monsoon module
SLOT04  = 'CCD12 0x0 4.080000' / Monsoon module
SLOT05  = 'CCD12 0x0 4.080000' / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =   2.087520149181E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -1.015554463106E-02 / Projection distortion parameter
PV2_9   =   8.962633753844E-03 / Projection distortion parameter
CD1_1   =  -5.4755258843559E-6 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -3.113151624030E-03 / Projection distortion parameter
PV2_1   =   1.013972447269E+00 / Projection distortion parameter
PV2_2   =  -1.021859645336E-02 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -1.626422093058E-02 / Projection distortion parameter
PV2_5   =   2.024751775407E-02 / Projection distortion parameter
PV2_6   =  -1.122433215372E-02 / Projection distortion parameter
PV2_7   =   6.422711612000E-03 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   1.904351960838E-03 / Projection distortion parameter
PV2_10  =  -2.654354928400E-03 / Projection distortion parameter
PV1_4   =   7.875355112150E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -2.495424047903E-03 / Projection distortion parameter
PV1_1   =   1.014821823296E+00 / Projection distortion parameter
PV1_0   =   1.606715902238E-03 / Projection distortion parameter
PV1_9   =   7.872055622797E-03 / Projection distortion parameter
PV1_8   =  -7.273769983855E-03 / Projection distortion parameter
CD1_2   =  0.00466284333699905 / Linear projection matrix
PV1_5   =  -1.979570504053E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046626708509696 / Linear projection matrix
CD2_2   =  -5.4366549976160E-6 / Linear projection matrix
PV1_10  =  -5.311615314207E-04 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   1.699867876722E+01 / World coordinate on this axis
CRVAL2  =  -5.570359508890E+01 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Sun Oct 11 13:41:26 2020' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Sun Oct 11 13:41:26 2020' / overscan corrected
BIASFIL = 'DEC20A_AZ20201008_98d1ca4-59131Z_01.fits' / Bias correction file
BAND    = 'u       '
NITE    = '20201008'
DESBIAS = 'Sun Oct 11 13:46:16 2020'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Sun Oct 11 13:46:17 2020'
DESBPM  = 'Sun Oct 11 13:46:17 2020' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Sun Oct 11 13:46:17 2020' / Flag saturated pixels
NSATPIX =                   13 / Number of saturated pixels
FLATMEDA=     3.22714590950167
FLATMEDB=     3.13872616877654
SATURATE=     48375.6955458227
DESGAINC= 'Sun Oct 11 13:46:17 2020'
FLATFIL = 'DEC20A_AZ20201008_98d1ca4-59131uF_01.fits' / Dome flat correction fil
DESFLAT = 'Sun Oct 11 13:46:17 2020'
STARFIL = 'STARFLAT_20191115_961e5e2-uP_ci_01.fits' / Star flat correction file
DESSTAR = 'Sun Oct 11 13:46:17 2020'
HISTORY Sun Oct 11 13:46:17 2020 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Sun Oct 11 13:56:02 2020' / bleed trail masking
NBLEED  = 75  / Number of bleed trail pixels.
STARMASK= 'Sun Oct 11 13:56:02 2020' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC20A_AZ20201008_98d1
HISTORY ca4-filu-ccd01u/DEC20A_AZ20201008_98d1ca4-u-201009042623_01.fits omiblee
HISTORY d_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Oct 11 13:56 Bad pixel file is omibleed_01_bpm.pl'
FWHM    =             5.924587 / Median FWHM in pixels
ELLIPTIC=            0.1456683 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Sun Oct 11 14:09:25 2020' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
PHOTREF = 'Gaia    '           / Photometric ref. catalog
MAGZERO =               25.993 / [mag] Magnitude zero point
MAGZPT  =               21.548 / [mag] Magnitude zero point per sec
NPHTMTCH=                 1692 / Number of phot. stars matched
RADIUS  =                 3.01 / [pix] Average radius of stars
RADSTD  =                0.183 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             3.789862
NCOMBINE=                 6600
PUPILSKY=     4.18733232401053
PUPILSUM=     11198.8173751042
PUPILMAX=    0.255183398723602
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'blSPblQPblQPblQP'   / HDU checksum updated 2020-11-02T19:31:44
DATASUM = '523321450'          / data unit checksum updated 2020-11-02T19:31:44
NPUPSCL =                76607
PUPSCL  =        7.47567974487
   Ț     Ą  Ì                 
t  
t  Ì  B  Ț      êÿÿÿ               ÿB       @6``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
 
@`
`
 
@`
`
 
@`
 
@`
 
@`
 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
``````````````````` @`` @` @ @`` @ @ @ @ @ @
` @
 @ @ @ @ @`` @ @` @`` @```````````````````` @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@`
 
@`
 
@`
 
@`
`
 
@`
`
 
@`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@A   ÿ      Ą 6 ÿ      AĄ  ÿ 
   
  @À $@œ  ÿ 
   
  @ž 4@”  ÿ 
   
  @ł >@°  ÿ 
   
  @ź H@«  ÿ 
   
  @© R@Š  ÿ 
   
  @Š X@Ł  ÿ 
   
  @ą `@  ÿ 
   
  @ f@  ÿ 
   
  @ l@  ÿ 
   
  @ r@  ÿ 
   
  @ x@  ÿ 
   
  @ |@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @  @  ÿ 
   
  @ €@}  ÿ 
   
  @ Š@|  ÿ 
   
  @} Ș@z  ÿ 
   
  @{ ź@x  ÿ 
   
  @y Č@v  ÿ 
   
  @x Ž@u  ÿ 
   
  @v ž@s  ÿ 
   
  @u ș@r  ÿ 
   
  @s Ÿ@p  ÿ 
   
  @r À@o  ÿ 
   
  @p Ä@m  ÿ 
   
  @o Æ@l  ÿ 
   
  @n È@k  ÿ 
   
  @l Ì@i  ÿ 
   
  @k Î@h  ÿ 
   
  @j Đ@g  ÿ 
   
  @h Ô@e  ÿ 
   
  @g Ö@d  ÿ 
   
  @f Ű@c  ÿ 
   
  @e Ú@b  ÿ 
   
  @c Ț@`  ÿ 
   
  @b à@_  ÿ 
   
  @a â@^  ÿ 
   
  @` ä@]  ÿ 
   
  @_ æ@\  ÿ 
   
  @^ è@[  ÿ 
   
  @] ê@Z  ÿ 
   
  @\ ì@Y  ÿ 
   
  @[ î@X  ÿ 
   
  @Z đ@W  ÿ 
   
  @Y ò@V  ÿ 
   
  @X ô@U  ÿ 
   
  @W ö@T  ÿ 
   
  @V ű@S  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S ț@P  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@L  ÿ 
   
  @N@K  ÿ 
   
  @M
@J  ÿ 
   
  @L@I  ÿ 
   
  @K@H  ÿ 
   
  @J@G  ÿ 
   
  @I@F  ÿ 
   
  @H@E  ÿ 
   
  @G@D  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@A  ÿ 
   
  @C@@  ÿ 
   
  @B @?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?&@<  ÿ 
   
  @>(@;  ÿ 
   
  @=*@:  ÿ 
   
  @<,@9  ÿ 
   
  @;.@8  ÿ 
   
  @:0@7  ÿ 
   
  @92@6  ÿ 
   
  @84@5  ÿ 
   
  @76@4  ÿ 
   
  @68@3  ÿ 
   
  @5:@2  ÿ 
   
  @4<@1  ÿ 
   
  @3>@0  ÿ 
   
  @2@@/  ÿ      @2 @ @/  ÿ      @1 @  @.  ÿ      @1 @& @.  ÿ      @1 @, @.  ÿ      @0 @0 @-  ÿ      @0 @4 @-  ÿ      @0 @8 @-  ÿ      @/ @< @,  ÿ      @/ @@ @,  ÿ      @/ @B @,  ÿ      @. @F @+  ÿ      @. @H @+  ÿ      @. @J @+  ÿ      @. }@N }@+  ÿ      @- }@P }@*  ÿ      @- |@R |@*  ÿ      @- {@T {@*  ÿ      @, {@V {@)  ÿ      @, z@X z@)  ÿ      @, y@Z y@)  ÿ      @, x@\ x@)  ÿ      @+ x@^ x@(  ÿ      @+ w@` w@(  ÿ      @+ v@b v@(  ÿ      @+ u@d u@(  ÿ      @* u@f u@'  ÿ      @* t@h t@'  ÿ      @* s@j s@'  ÿ      @) t@j t@&  ÿ      @) s@l s@&  ÿ      @) r@n r@&  ÿ      @) q@p q@&  ÿ      @( q@r q@%  ÿ      @( p@t p@% 
 ÿ      @( o@v o@%  ÿ      @( o@w o@$ 
 ÿ      @( o@v o@%  ÿ      @( p@t p@%  ÿ      @( q@r q@%  ÿ      @) q@p q@&  ÿ      @) r@n r@&  ÿ      @) s@l s@&  ÿ      @) t@j t@&  ÿ      @* s@j s@'  ÿ      @* t@h t@'  ÿ      @* u@f u@'  ÿ      @+ u@d u@(  ÿ      @+ v@b v@(  ÿ      @+ w@` w@(  ÿ      @+ x@^ x@(  ÿ      @, x@\ x@)  ÿ      @, y@Z y@)  ÿ      @, z@X z@)  ÿ      @, {@V {@)  ÿ      @- {@T {@*  ÿ      @- |@R |@*  ÿ      @- }@P }@*  ÿ      @. }@N }@+  ÿ      @. @J @+  ÿ      @. @H @+  ÿ      @. @F @+  ÿ      @/ @B @,  ÿ      @/ @@ @,  ÿ      @/ @< @,  ÿ      @0 @8 @-  ÿ      @0 @4 @-  ÿ      @0 @0 @-  ÿ      @1 @, @.  ÿ      @1 @& @.  ÿ      @1 @  @.  ÿ      @2 @ @/  ÿ 
   
  @2@@/  ÿ 
   
  @3>@0  ÿ 
   
  @4<@1  ÿ 
   
  @5:@2  ÿ 
   
  @68@3  ÿ 
   
  @76@4  ÿ 
   
  @84@5  ÿ 
   
  @92@6  ÿ 
   
  @:0@7  ÿ 
   
  @;.@8  ÿ 
   
  @<,@9  ÿ 
   
  @=*@:  ÿ 
   
  @>(@;  ÿ 
   
  @?&@<  ÿ 
   
  @@$@=  ÿ 
   
  @A"@>  ÿ 
   
  @B @?  ÿ 
   
  @C@@  ÿ 
   
  @D@A  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @G@D  ÿ 
   
  @H@E  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@H  ÿ 
   
  @L@I  ÿ 
   
  @M
@J  ÿ 
   
  @N@K  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S ț@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ű@S  ÿ 
   
  @W ö@T  ÿ 
   
  @X ô@U  ÿ 
   
  @Y ò@V  ÿ 
   
  @Z đ@W  ÿ 
   
  @[ î@X  ÿ 
   
  @\ ì@Y  ÿ 
   
  @] ê@Z  ÿ 
   
  @^ è@[  ÿ 
   
  @_ æ@\  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @b à@_  ÿ 
   
  @c Ț@`  ÿ 
   
  @e Ú@b  ÿ 
   
  @f Ű@c  ÿ 
   
  @g Ö@d  ÿ 
   
  @h Ô@e  ÿ 
   
  @j Đ@g  ÿ 
   
  @k Î@h  ÿ 
   
  @l Ì@i  ÿ 
   
  @n È@k  ÿ 
   
  @o Æ@l  ÿ 
   
  @p Ä@m  ÿ 
   
  @r À@o  ÿ 
   
  @s Ÿ@p  ÿ 
   
  @u ș@r  ÿ 
   
  @v ž@s  ÿ 
   
  @x Ž@u  ÿ 
   
  @y Č@v  ÿ 
   
  @{ ź@x  ÿ 
   
  @} Ș@z  ÿ 
   
  @ Š@|  ÿ 
   
  @ €@}  ÿ 
   
  @  @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ |@  ÿ 
   
  @ x@  ÿ 
   
  @ r@  ÿ 
   
  @ l@  ÿ 
   
  @ f@  ÿ 
   
  @ą `@  ÿ 
   
  @Š X@Ł  ÿ 
   
  @© R@Š  ÿ 
   
  @ź H@«  ÿ 
   
  @ł >@°  ÿ 
   
  @ž 4@”  ÿ 
   
  @À $@œ A ÿ      AĄ