  ĤV  ;­  Ç$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1279030564
$MTIME = 1279981136
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-07-12T21:14:57' / Date FITS file was generated
IRAF-TLM= '2020-07-12T21:14:44' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                 180. / [s] Requested exposure duration
EXPTIME =                 180. / [s] Exposure duration
EXPDUR  =                 180. / [s] Exposure duration
DARKTIME=            181.10709 / [s] Dark time
OBSID   = 'ct4m20180512t231313' / Unique Observation ID
DATE-OBS= '2018-05-12T23:13:13.114315' / DateTime of observation start (UTC)
TIME-OBS= '23:13:13.114315'    / Time of observation start (UTC)
MJD-OBS =        58250.9675129 / MJD of observation start
MJD-END =      58250.969596233 / Exposure end
OPENSHUT= '2018-05-12T23:13:13.164490' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               745652 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'The DECam Legacy Survey of the SDSS Equatorial Sky' / Current observi
OBSERVER= 'Elodie Savery, Alfredo Zenteno' / Observer name(s)
PROPOSER= 'Schlegel'           / Proposal Principle Investigator
DTPI    = 'Schlegel'           / Proposal Principle Investigator (iSTB)
PROPID  = '2014B-0404'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    3 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'z DECam SDSS c0004 9260.0 1520.0' / Unique filter identifier
FILTPOS = 'cassette_1'         / Filter position in FCM
INSTANCE= 'DECam_20180512'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '09:29:30.721'       / [HH:MM:SS] Target RA
DEC     = '17:13:58.800'       / [DD:MM:SS] Target DEC
TELRA   = '09:29:32.827'       / [HH:MM:SS] Telescope RA
TELDEC  = '17:13:57.497'       / [DD:MM:SS] Telescope DEC
HA      = '00:22:16.550'       / [HH:MM:SS] Telescope hour angle
ZD      =                 47.6 / [deg] Telescope zenith distance
AZ      =             352.9238 / [deg] Telescope azimuth angle
DOMEAZ  =               354.03 / [deg] Dome azimuth angle
ZPDELRA =            -226.0903 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                  6.5 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-358.03,-2884.33,2252.39,89.58,-112.36,-0.00' / DECam hexapod setting
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =                5.472 / [m/s] Wind speed
WINDDIR =                 352. / [deg] Wind direction (from North)
HUMIDITY=                  18. / [%] Ambient relative humidity (outside)
PRESSURE=                 779. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                1.532 / [arcsec] DIMM2 Seeing
MASS2   =                 1.84 /  MASS(2) FSEE
ASTIG1  =                -0.02 / 4MAPS correction 1
ASTIG2  =                -0.16 / 4MAPS correction 2
OUTTEMP =                 14.1 / [deg C] Outside temperature
AIRMASS =                 1.48 / Airmass
GSKYVAR =                0.064 / RASICAM global sky standard deviation
GSKYHOT =                 0.21 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               11.425 / [deg C] Mirror surface average temperature
MAIRTEMP=                 11.9 / [deg C] Mirror temperature above surface
UPTRTEMP=               14.049 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                11.75 / [deg C] Mirror top surface temperature
UTN-TEMP=                13.62 / [deg C] Upper truss temperature north
UTS-TEMP=                14.02 / [deg C] Upper truss temperature south
UTW-TEMP=               14.505 / [deg C] Upper truss temperature west
UTE-TEMP=                14.05 / [deg C] Upper truss temperature east
PMN-TEMP=                 11.2 / [deg C] Mirror north edge temperature
PMS-TEMP=                 11.1 / [deg C] Mirror south edge temperature
PMW-TEMP=                 11.5 / [deg C] Mirror west edge temperature
PME-TEMP=                 11.9 / [deg C] Mirror east edge temperature
DOMELOW =                14.51 / [deg C] Low dome temperature
DOMEHIGH=                 14.8 / [deg C] High dome temperature
DOMEFLOR=                  5.8 / [deg C] Dome floor temperature
DONUTFS4= '[-0.45,2.35,-8.88,-0.22,-0.46,-0.06,0.39,-0.05,-0.25,]' / Mean Wavefr
DONUTFS3= '[-0.47,1.83,8.67,-0.16,-0.31,0.01,0.12,0.04,-0.34,]' / Mean Wavefront
DONUTFS2= '[2.33,-0.74,-8.76,0.14,-0.23,-0.18,0.21,0.28,-0.03,]' / Mean Wavefron
DONUTFS1= '[0.78,1.71,8.72,0.15,-0.19,0.06,0.14,0.22,0.11,]' / Mean Wavefront fo
DONUTFN1= '[0.42,0.02,-8.92,-0.11,-0.56,-0.16,-0.09,0.20,-0.22,]' / Mean Wavefro
DONUTFN2= '[3.07,0.85,8.74,-0.36,-0.50,-0.04,0.01,0.17,-0.08,]' / Mean Wavefront
DONUTFN3= '[1.03,1.21,-8.86,0.13,-0.54,-0.09,-0.08,0.00,0.28,]' / Mean Wavefront
DONUTFN4= '[]      '           / Mean Wavefront for Sensor FN4
HIERARCH TIME_RECORDED = '2018-05-12T23:16:35.981212'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    0 / Number of guide CCDs that remained active
DOXT    =                  0.4 / [arcsec] X-theta from donut analysis
FADZ    =                15.05 / [um] FA Delta focus.
FADY    =               370.66 / [um] FA Delta Y.
FADX    =               141.23 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                  7.4 / [arcsec] FA Delta Y-theta.
DODZ    =                15.05 / [um] Delta-Z from donut analysis
DODY    =                -0.96 / [um] Y-decenter from donut analysis
DODX    =                -1.03 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2018-05-12T23:13:03' / Time of last RASICAM exposure (UTC)
DOYT    =                 0.06 / [arcsec] Y-theta from donut analysis
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =               -18.82 / [arcsec] FA Delta X-theta.
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:65'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTNSANAM= 'c4d_180512_231313_ori.fits.fz'
DTPROPID= '2014B-0404'
ODATEOBS= '2018-05-12T23:13:13.114315'
DTACQNAM= '/data_local/images/DTS/2014B-0404/DECam_00745652.fits.fz'
DTINSTRU= 'decam   '
DTTELESC= 'ct4m    '
DTSITE  = 'ct      '
DTCALDAT= '2018-05-12'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.84339635888481
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     40619.1330329766
GAINB   =     3.80848410326242
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     37260.1557870339
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =   1.292520097439E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -1.085639046593E-02 / Projection distortion parameter
PV2_9   =   6.447478695312E-03 / Projection distortion parameter
CD1_1   =  -8.5732394860160E-7 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -5.860827794559E-03 / Projection distortion parameter
PV2_1   =   1.022926622776E+00 / Projection distortion parameter
PV2_2   =  -9.956860588882E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -2.552664171085E-02 / Projection distortion parameter
PV2_5   =   2.040879532332E-02 / Projection distortion parameter
PV2_6   =  -9.423264847503E-03 / Projection distortion parameter
PV2_7   =   9.366132237292E-03 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   1.072966892153E-02 / Projection distortion parameter
PV2_10  =  -2.902607846633E-03 / Projection distortion parameter
PV1_4   =   7.975619468366E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -1.059469368434E-02 / Projection distortion parameter
PV1_1   =   1.015157181805E+00 / Projection distortion parameter
PV1_0   =   4.303028018024E-03 / Projection distortion parameter
PV1_9   =   7.785488836336E-03 / Projection distortion parameter
PV1_8   =  -7.287409639159E-03 / Projection distortion parameter
CD1_2   =  0.00466208765316865 / Linear projection matrix
PV1_5   =  -2.013663951635E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046630646006938 / Linear projection matrix
CD2_2   =  -1.1239608430061E-6 / Linear projection matrix
PV1_10  =  -3.686927828818E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   1.423800629035E+02 / World coordinate on this axis
CRVAL2  =   1.723332306099E+01 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Thu Jul 25 05:50:48 2019' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Thu Jul 25 05:50:48 2019' / overscan corrected
BIASFIL = 'DEC18A_20180507_918b673-58249Z_01.fits' / Bias correction file
BAND    = 'z       '
NITE    = '20180512'
DESBIAS = 'Thu Jul 25 06:06:36 2019'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Thu Jul 25 06:06:36 2019'
DESBPM  = 'Thu Jul 25 06:06:36 2019' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Thu Jul 25 06:06:36 2019' / Flag saturated pixels
NSATPIX =                 7167 / Number of saturated pixels
FLATMEDA=     3.84339635888481
FLATMEDB=     3.80848410326242
SATURATE=     40619.1330329766
DESGAINC= 'Thu Jul 25 06:06:36 2019'
FLATFIL = 'DEC18A_20180507_918b673-58249zF_01.fits' / Dome flat correction file
DESFLAT = 'Thu Jul 25 06:06:40 2019'
STARFIL = 'STARFLAT_20180327_94aa91d-zP_ci_01.fits' / Star flat correction file
DESSTAR = 'Thu Jul 25 06:06:43 2019'
HISTORY Thu Jul 25 06:06:43 2019 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Thu Jul 25 07:13:19 2019' / bleed trail masking
NBLEED  = 14585  / Number of bleed trail pixels.
STARMASK= 'Thu Jul 25 07:13:19 2019' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DECALS_DR9_952905e-fil
HISTORY z-ccd01z/DECALS_DR9_952905e-z-180512231313_01.fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FWHM    =             5.857277 / Median FWHM in pixels
ELLIPTIC=            0.0898135 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Thu Jul 25 07:32:22 2019' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =              5917.16
NCOMBINE=                60000
PUPILSKY=     5861.18360219615
PUPILSUM=     4731381.98057375
PUPILMAX=     194.269561767578
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'ePhmhMejeMejeMej'   / HDU checksum updated 2020-07-12T20:37:56
DATASUM = '1716355640'         / data unit checksum updated 2020-07-12T20:37:56
PUPSCL  =    0.010190891707101
    Ŝ     Ħ  Ì                 A  A  Ì   ^  Ç      ê˙˙˙               ˙ ^       @Ş @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
`````` @ @` @ @````````` @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
`
 @G0@_   ˙      Ħ Ş ˙      AĦ  ˙ 
   
  @P@M  ˙ 
   
  @O@L  ˙ 
   
  @N@K  ˙ 
   
  @M
@J  ˙ 
   
  @L@I  ˙ 
   
  @K@H  ˙ 
   
  @J@G  ˙ 
   
  @I@F  ˙ 
   
  @H@E  ˙ 
   
  @G@D  ˙ 
   
  @F@C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F ~@ ~@C  ˙      @F }@ }@C  ˙      @F }@ }@B  ˙      @F }@ }@C  ˙      @F ~@ ~@C  ˙      @F ~@ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @F @ @C  ˙      @FP @PP @C  ˙      @F @ @C  ˙ 
   
  @G@D  ˙ 
   
  @H@E  ˙ 
   
  @I@F  ˙ 
   
  @J@G  ˙ 
   
  @K@H  ˙ 
   
  @L@I  ˙ 
   
  @M
@J  ˙ 
   
  @N@K  ˙ 
   
  @O@L  ˙ 
   
  @P@M  ˙ 
   
  @Q@N  ˙ 
   
  @R @O  ˙ 
   
  @S ŝ@P  ˙ 
   
  @T ü@Q  ˙ 
   
  @U ú@R  ˙ 
   
  @V ĝ@S  ˙ 
   
  @W ö@T  ˙ 
   
  @X ô@U _ ˙      AĦ G ˙      AĦ