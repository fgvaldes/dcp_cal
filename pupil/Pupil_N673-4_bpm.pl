  ŠV  =d  /$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1297931689
$MTIME = 1297931689
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2021-02-16T15:34:49' / Date FITS file was generated
IRAF-TLM= '2021-02-16T15:34:52' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                1200. / [s] Requested exposure duration
EXPTIME =                1200. / [s] Exposure duration
EXPDUR  =                1200. / [s] Exposure duration
DARKTIME=           1201.21697 / [s] Dark time
OBSID   = 'ct4m20210208t075142' / Unique Observation ID
DATE-OBS= '2021-02-08T07:51:42.852707' / DateTime of observation start (UTC)
TIME-OBS= '07:51:42.852707'    / Time of observation start (UTC)
MJD-OBS =       59253.32757931 / MJD of observation start
MJD-END =      59253.341468199 / Exposure end
OPENSHUT= '2021-02-08T07:51:43.015810' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               964395 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'A 100 Deg^2 DECam Narrow-band Survey for the LSST Era: Tracing the &'
CONTINUE= '        '           /   '&' / Current observing orogram
OBSERVER= 'Soo Lee '           / Observer name(s)
PROPOSER= 'Lee     '           / Proposal Principle Investigator
DTPI    = 'Lee     '           / Proposal Principle Investigator (iSTB)
PROPID  = '2020B-0201'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'N673 DECam c0011 6730.0 100.0' / Unique filter identifier
INSTANCE= 'DECam_20210207'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '10:03:53.921'       / [HH:MM:SS] Target RA
DEC     = '02:40:05.070'       / [DD:MM:SS] Target DEC
TELRA   = '10:03:54.310'       / [HH:MM:SS] Telescope RA
TELDEC  = '02:40:04.501'       / [DD:MM:SS] Telescope DEC
HA      = '02:18:06.070'       / [HH:MM:SS] Telescope hour angle
ZD      =                46.41 / [deg] Telescope zenith distance
AZ      =             308.6609 / [deg] Telescope azimuth angle
DOMEAZ  =              311.627 / [deg] Dome azimuth angle
ZPDELRA =             -13.5861 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                 25.2 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '1173.93,-1096.81,2523.30,-69.08,-40.64, 0.00' / DECam hexapod setting
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =               21.726 / [m/s] Wind speed
WINDDIR =                  40. / [deg] Wind direction (from North)
HUMIDITY=                  55. / [%] Ambient relative humidity (outside)
PRESSURE=                 778. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                0.675 / [arcsec] DIMM2 Seeing
MASS2   =                 0.52 /  MASS(2) FSEE
ASTIG1  =                 0.09 / 4MAPS correction 1
ASTIG2  =                  -0. / 4MAPS correction 2
OUTTEMP =                 13.5 / [deg C] Outside temperature
AIRMASS =                 1.45 / Airmass
GSKYVAR =                0.042 / RASICAM global sky standard deviation
GSKYHOT =                0.308 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               14.762 / [deg C] Mirror surface average temperature
MAIRTEMP=                 14.6 / [deg C] Mirror temperature above surface
UPTRTEMP=               13.811 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 14.7 / [deg C] Mirror top surface temperature
UTN-TEMP=               13.855 / [deg C] Upper truss temperature north
UTS-TEMP=               13.545 / [deg C] Upper truss temperature south
UTW-TEMP=                13.76 / [deg C] Upper truss temperature west
UTE-TEMP=               14.085 / [deg C] Upper truss temperature east
PMN-TEMP=                14.45 / [deg C] Mirror north edge temperature
PMS-TEMP=                 14.6 / [deg C] Mirror south edge temperature
PMW-TEMP=                  15. / [deg C] Mirror west edge temperature
PME-TEMP=                  15. / [deg C] Mirror east edge temperature
DOMELOW =                14.53 / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                 11.4 / [deg C] Dome floor temperature
G-MEANX =              -0.0083 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0335 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[0.34,1.18,-8.72,-0.09,-0.01,-0.06,0.12,-0.07,-0.56,]' / Mean Wavefro
DONUTFS3= '[-1.03,1.45,8.85,-0.15,-0.26,-0.10,-0.03,0.04,-0.41,]' / Mean Wavefro
DONUTFS2= '[1.13,1.56,-8.70,0.19,0.04,-0.26,0.23,0.33,-0.10,]' / Mean Wavefront
DONUTFS1= '[1.41,1.16,8.88,0.21,0.11,-0.02,-0.03,0.19,0.12,]' / Mean Wavefront f
G-FLXVAR=           135089.029 / [arcsec] Guider mean guide star flux variances
G-MEANXY=             0.000891 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[-0.50,0.66,-8.78,-0.20,0.24,-0.09,-0.12,0.16,-0.33,]' / Mean Wavefro
DONUTFN2= '[1.82,1.28,8.91,-0.14,-0.29,-0.08,-0.10,0.20,-0.21,]' / Mean Wavefron
DONUTFN3= '[0.54,0.21,-8.68,-0.04,0.04,-0.11,-0.20,0.09,0.28,]' / Mean Wavefront
DONUTFN4= '[1.83,1.51,8.82,0.11,-0.21,-0.06,-0.06,-0.07,0.13,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2021-02-08T08:12:05.656764'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    3 / Number of guide CCDs that remained active
DOXT    =                 0.04 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.2639 / [arcsec] Guider x-axis maximum offset
FADZ    =               -12.52 / [um] FA Delta focus.
FADY    =               108.17 / [um] FA Delta Y.
FADX    =                359.6 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                -0.56 / [arcsec] FA Delta Y-theta.
DODZ    =               -12.52 / [um] Delta-Z from donut analysis
DODY    =                -0.69 / [um] Y-decenter from donut analysis
DODX    =                -1.13 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2021-02-08T07:51:38' / Time of last RASICAM exposure (UTC)
G-SEEING=                1.597 / [arcsec] Guider average seeing
G-TRANSP=                0.822 / Guider average sky transparency
G-MEANY2=             0.014018 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                 0.01 / [arcsec] Y-theta from donut analysis
G-LATENC=                 1.89 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                -3.79 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.4667 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.006061 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:89'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTACQNAM= '/data_local/images/DTS/2020B-0201/DECam_00964395.fits.fz'
DTCALDAT= '2021-02-07'
DTNSANAM= 'c4d_210208_075142_ori.fits.fz'
DTPROPID= '2020B-0201'
DTINSTRU= 'decam   '
DTSITE  = 'ct      '
ODATEOBS= '2021-02-08T07:51:42.852707'
DTTELESC= 'ct4m    '
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.48076457247849
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     44850.9012170386
GAINB   =     3.41893283283234
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     41505.5568326103
CRPIX1  =             209.3625 / Coordinate Reference axis 1
CRPIX2  =        230.270828125 / Coordinate Reference axis 2
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 0x0 4.010000' / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =                2000. / [yr] Equinox of WCS
PV1_7   =    0.000618368874624 / PV distortion coefficient
CUNIT1  = 'deg     '
PV2_8   =    -0.00842887692623 / PV distortion coefficient
PV2_9   =     0.00790302236959 / PV distortion coefficient
CD1_1   =  -1.1679624622720E-5 / World coordinate transformation matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =    -0.00441101222605 / PV distortion coefficient
PV2_1   =        1.01700592995 / PV distortion coefficient
PV2_2   =    -0.00863195988986 / PV distortion coefficient
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =     -0.0181936225912 / PV distortion coefficient
PV2_5   =      0.0168279891847 / PV distortion coefficient
PV2_6   =      -0.010365069688 / PV distortion coefficient
PV2_7   =     0.00641705596954 / PV distortion coefficient
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =       0.010153274645 / PV distortion coefficient
PV2_10  =    -0.00261077202228 / PV distortion coefficient
PV1_4   =     0.00862489761958 / PV distortion coefficient
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =    -0.00978790747587 / PV distortion coefficient
PV1_1   =        1.01430869337 / PV distortion coefficient
PV1_0   =     0.00392185213728 / PV distortion coefficient
PV1_9   =     0.00654393666949 / PV distortion coefficient
PV1_8   =    -0.00736713022181 / PV distortion coefficient
CD1_2   =  0.00466262971156481 / World coordinate transformation matrix
PV1_5   =      -0.017864081795 / PV distortion coefficient
CUNIT2  = 'deg     '
CD2_1   =  -0.0046623936290495 / World coordinate transformation matrix
CD2_2   =  -1.1659626630144E-5 / World coordinate transformation matrix
PV1_10  =    -0.00361076208606 / PV distortion coefficient
CTYPE2  = 'DEC--TPV'           / Coordinate type
CTYPE1  = 'RA---TPV'           / Coordinate type
CRVAL1  =           150.976292 / [deg] WCS Reference Coordinate (RA)
CRVAL2  =             2.667917 / [deg] WCS Reference Coordinate (DEC)
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Tue Feb 16 03:19:40 2021' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Tue Feb 16 03:19:40 2021' / overscan corrected
BIASFIL = 'DEC21A_ODIN20210207_99cf626-59252Z_01.fits' / Bias correction file
BAND    = 'N673    '
NITE    = '20210207'
DESBIAS = 'Tue Feb 16 03:28:02 2021'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Tue Feb 16 03:28:05 2021'
DESBPM  = 'Tue Feb 16 03:28:07 2021' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Tue Feb 16 03:28:07 2021' / Flag saturated pixels
NSATPIX =                  810 / Number of saturated pixels
FLATMEDA=     3.48076457247849
FLATMEDB=     3.41893283283234
SATURATE=     44850.9012170386
DESGAINC= 'Tue Feb 16 03:28:07 2021'
FLATFIL = 'DEC21A_ODIN20210207_99cf626-59252N673F_01.fits' / Dome flat correctio
DESFLAT = 'Tue Feb 16 03:28:07 2021'
STARFIL = 'STARFLAT_20210128_99bacc2-N673P_ci_01.fits' / Star flat correction fi
DESSTAR = 'Tue Feb 16 03:28:08 2021'
HISTORY Tue Feb 16 03:28:08 2021 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
AMPMTCH = 'B 1.0029 (0.0005,0.0050)' / Amp match
DESBLEED= 'Tue Feb 16 03:50:41 2021' / bleed trail masking
NBLEED  = 3547  / Number of bleed trail pixels.
STARMASK= 'Tue Feb 16 03:50:41 2021' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC21A_ODIN20210207_99
HISTORY de95e-filN673-ccd01N673/DEC21A_ODIN20210207_99de95e-N673-210208075142_01
HISTORY .fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Feb 16  3:50 Bad pixel file is omibleed_01_bpm.pl'
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =              224.512
NCOMBINE=                 4080
PUPILSKY=     224.807324319201
PUPILSUM=      588941.31081243
PUPILMAX=      14.465124130249
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= '4aKQ7UKO4aKO4UKO'   / HDU checksum updated 2021-02-16T15:34:44
DATASUM = '2089398296'         / data unit checksum updated 2021-02-16T15:34:44
NPUPSCL =                82297
PUPSCL  =     0.14124934195463
   Ț     Ą  Ì                 ș  ș  Ì  M  /      êÿÿÿ               ÿM       @9``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
 
@`
 
@ 
@`
 
@`
`
`
``````````````````````` @`` @ @ @	 @ @`` @` @``````````````````````` 
@`
 
@`
 
@`
 
@ 
@ 
@`
 
@`
 
@ 
@`
`
`
 
@ 
@ 
@ 
@ 
@ 
@`
`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@C   ÿ      Ą 9 ÿ      AĄ  ÿ 
   
  @Ă @Â  ÿ 
   
  @ș .@č  ÿ 
   
  @ł ;@ł  ÿ 
   
  @ź E@ź  ÿ 
   
  @Ș N@©  ÿ 
   
  @Š V@„  ÿ 
   
  @ą ]@ą  ÿ 
   
  @ d@  ÿ 
   
  @ j@  ÿ 
   
  @ p@  ÿ 
   
  @ v@  ÿ 
   
  @ z@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ Ą@  ÿ 
   
  @ €@~  ÿ 
   
  @} š@|  ÿ 
   
  @{ Ź@z  ÿ 
   
  @y Ż@y  ÿ 
   
  @x Č@w  ÿ 
   
  @v ¶@u  ÿ 
   
  @u ž@t  ÿ 
   
  @s Œ@r  ÿ 
   
  @r Ÿ@q  ÿ 
   
  @p Á@p  ÿ 
   
  @o Ä@n  ÿ 
   
  @m Ç@m  ÿ 
   
  @l Ê@k  ÿ 
   
  @k Ì@j  ÿ 
   
  @j Î@i  ÿ 
   
  @h Ń@h  ÿ 
   
  @g Ô@f  ÿ 
   
  @f Ö@e  ÿ 
   
  @e Ű@d  ÿ 
   
  @c Û@c  ÿ 
   
  @b Ę@b  ÿ 
   
  @a à@`  ÿ 
   
  @` â@_  ÿ 
   
  @_ ä@^  ÿ 
   
  @^ æ@]  ÿ 
   
  @] è@\  ÿ 
   
  @\ ê@[  ÿ 
   
  @[ ì@Z  ÿ 
   
  @Z î@Y  ÿ 
   
  @Y đ@X  ÿ 
   
  @X ò@W  ÿ 
   
  @W ô@V  ÿ 
   
  @V ö@U  ÿ 
   
  @U ű@T  ÿ 
   
  @T ú@S  ÿ 
   
  @S ü@R  ÿ 
   
  @R ę@R  ÿ 
   
  @Q ÿ@Q  ÿ 
   
  @Q @P  ÿ 
   
  @P@O  ÿ 
   
  @O@N  ÿ 
   
  @N@M  ÿ 
   
  @M@M  ÿ 
   
  @M@L  ÿ 
   
  @L
@K  ÿ 
   
  @K@J  ÿ 
   
  @J@J  ÿ 
   
  @J@I  ÿ 
   
  @I@H  ÿ 
   
  @H@G  ÿ 
   
  @G@G  ÿ 
   
  @G@F  ÿ 
   
  @F@E  ÿ 
   
  @E@E  ÿ 
   
  @E@D  ÿ 
   
  @D@C  ÿ 
   
  @C@C  ÿ 
   
  @C@B  ÿ 
   
  @B@A  ÿ 
   
  @A@A  ÿ 
   
  @A @@  ÿ 
   
  @@!@@  ÿ 
   
  @@"@?  ÿ 
   
  @?$@>  ÿ 
   
  @>&@=  ÿ 
   
  @='@=  ÿ 
   
  @=(@<  ÿ 
   
  @<)@<  ÿ 
   
  @<*@;  ÿ 
   
  @;+@;  ÿ 
   
  @;,@:  ÿ 
   
  @:-@:  ÿ 
   
  @:.@9  ÿ 
   
  @9/@9  ÿ 
   
  @90@8  ÿ 
   
  @81@8  ÿ 
   
  @82@7  ÿ 
   
  @74@6  ÿ 
   
  @66@5  ÿ 
   
  @57@5  ÿ 
   
  @58@4  ÿ 
   
  @4:@3  ÿ 
   
  @3;@3  ÿ 
   
  @3<@2  ÿ 
   
  @2=@2  ÿ 
   
  @2>@1  ÿ 
   
  @1?@1  ÿ 
   
  @1@@0  ÿ 
   
  @0A@0  ÿ 
   
  @0B@/  ÿ 
   
  @/D@.  ÿ 
   
  @.E@.  ÿ 
   
  @.F@-  ÿ 
   
  @-G@-  ÿ 
   
  @-H@,  ÿ      @- @ @,  ÿ      @- @ @,  ÿ      @- @ @,  ÿ      @, @  @,  ÿ      @, @$ @+  ÿ      @, @' @+  ÿ      @, @* @+  ÿ      @, @- @+  ÿ      @+ @0 @+  ÿ      @+ @2 @+  ÿ      @+ @4 @*  ÿ      @+ @6 @*  ÿ      @+ @8 @*  ÿ      @+ @: @*  ÿ      @* @; @*  ÿ      @* @< @*  ÿ      @* @> @*  ÿ      @* @? @)  ÿ      @* @@ @)  ÿ      @* @A @)  ÿ      @* @B @)  ÿ      @* @C @)  ÿ      @* @D @)  ÿ      @* @E @)  ÿ      @) @F @)  ÿ      @) @G @)  ÿ      @) @H @)  ÿ      @) @H @(  ÿ      @) @I @( 	 ÿ      @) @J @(  ÿ      @) @I @(  ÿ      @) @H @(  ÿ      @) @H @)  ÿ      @) @G @)  ÿ      @) @F @)  ÿ      @) @E @)  ÿ      @* @D @)  ÿ      @* @C @)  ÿ      @* @B @)  ÿ      @* @A @)  ÿ      @* @@ @)  ÿ      @* @> @)  ÿ      @* @= @*  ÿ      @* @< @*  ÿ      @+ @: @*  ÿ      @+ @8 @*  ÿ      @+ @7 @*  ÿ      @+ @5 @*  ÿ      @+ @3 @*  ÿ      @+ @0 @+  ÿ      @+ @. @+  ÿ      @, @, @+  ÿ      @, @) @+  ÿ      @, @& @+  ÿ      @, @" @,  ÿ      @, @ @,  ÿ      @- @ @,  ÿ      @- @ @,  ÿ      @-  @ Ą@,  ÿ 
   
  @-G@-  ÿ 
   
  @.F@-  ÿ 
   
  @.E@.  ÿ 
   
  @/D@.  ÿ 
   
  @/C@/  ÿ 
   
  @0B@/  ÿ 
   
  @0A@0  ÿ 
   
  @1@@0  ÿ 
   
  @2>@1  ÿ 
   
  @3<@2  ÿ 
   
  @3;@3  ÿ 
   
  @4:@3  ÿ 
   
  @49@4  ÿ 
   
  @58@4  ÿ 
   
  @66@5  ÿ 
   
  @65@6  ÿ 
   
  @74@6  ÿ 
   
  @73@7  ÿ 
   
  @82@7  ÿ 
   
  @90@8  ÿ 
   
  @:.@9  ÿ 
   
  @;,@:  ÿ 
   
  @<*@;  ÿ 
   
  @=(@<  ÿ 
   
  @>&@=  ÿ 
   
  @>%@>  ÿ 
   
  @?$@>  ÿ 
   
  @?#@?  ÿ 
   
  @@"@?  ÿ 
   
  @A @@  ÿ 
   
  @B@A  ÿ 
   
  @B@B  ÿ 
   
  @C@B  ÿ 
   
  @D@C  ÿ 
   
  @D@D  ÿ 
   
  @E@D  ÿ 
   
  @F@E  ÿ 
   
  @F@F  ÿ 
   
  @G@F  ÿ 
   
  @H@G  ÿ 
   
  @H@H  ÿ 
   
  @I@H  ÿ 
   
  @J@I  ÿ 
   
  @K@J  ÿ 
   
  @K@K  ÿ 
   
  @L
@K  ÿ 
   
  @M@L  ÿ 
   
  @N@M  ÿ 
   
  @N@N  ÿ 
   
  @O@O  ÿ 
   
  @P@O  ÿ 
   
  @Q @P  ÿ 
   
  @R ț@Q  ÿ 
   
  @S ü@R  ÿ 
   
  @T ú@S  ÿ 
   
  @T ù@T  ÿ 
   
  @U ś@U  ÿ 
   
  @V ő@V  ÿ 
   
  @W ó@W  ÿ 
   
  @X ń@X  ÿ 
   
  @Y đ@X  ÿ 
   
  @Z î@Y  ÿ 
   
  @[ ì@Z  ÿ 
   
  @\ é@\  ÿ 
   
  @] ç@]  ÿ 
   
  @^ ć@^  ÿ 
   
  @_ ă@_  ÿ 
   
  @a à@`  ÿ 
   
  @b Ț@a  ÿ 
   
  @c Ü@b  ÿ 
   
  @d Ú@c  ÿ 
   
  @e Ű@d  ÿ 
   
  @f Ő@f  ÿ 
   
  @h Ò@g  ÿ 
   
  @i Đ@h  ÿ 
   
  @j Î@i  ÿ 
   
  @k Ë@k  ÿ 
   
  @m È@l  ÿ 
   
  @n Æ@m  ÿ 
   
  @o Ă@o  ÿ 
   
  @q À@p  ÿ 
   
  @r œ@r  ÿ 
   
  @t ș@s  ÿ 
   
  @u ·@u  ÿ 
   
  @w Ž@v  ÿ 
   
  @y °@x  ÿ 
   
  @z ź@y  ÿ 
   
  @| Ș@{  ÿ 
   
  @~ Š@}  ÿ 
   
  @ Ł@  ÿ 
   
  @  @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ~@  ÿ 
   
  @ x@  ÿ 
   
  @ s@  ÿ 
   
  @ m@  ÿ 
   
  @ g@  ÿ 
   
  @  a@   ÿ 
   
  @€ Z@Ł  ÿ 
   
  @š R@§  ÿ 
   
  @Ź J@«  ÿ 
   
  @± @@°  ÿ 
   
  @¶ 6@”  ÿ 
   
  @œ '@œ  ÿ 
   
  @Ê @Ê C ÿ      AĄ