# Linearity

CLASS = "'pupil'"
DIR = "'pupil'"
DATATYPE = "'image'"
QUALITY = 0

MJD,FILTER,MATCH,VALUE

56475,'z DECam SDSS c0004 9260.0 1520.0','cassette_1','Pupil_z1.fits'
56475,'g DECam SDSS c0001 4720.0 1520.0','cassette_2','Pupil_g2.fits'
56475,'z DECam SDSS c0004 9260.0 1520.0','cassette_3','Pupil_z3.fits'
56475,'r DECam SDSS c0002 6415.0 1480.0','cassette_4','Pupil_r4.fits'
56475,'Y DECam c0005 10095.0 1130.0','cassette_4','Pupil_Y-4.fits'
56475,'u DECam c0006 3500.0 1000.0','cassette_5','Pupil_u-5.fits'
56475,'VR DECam c0007 6300.0 2600.0','cassette_6','Pupil_VR-6.fits'
56475,'r DECam SDSS c0002 6415.0 1480.0','cassette_7','Pupil_r7.fits'
56475,'r DECam SDSS c0002 6415.0 1480.0','cassette_8','Pupil_r8.fits'
56475,'N662 DECam c0009 6620.0 172.0','cassette_8','Pupil_N662-8.fits'
56475,'N964 DECam c0008 9645.0 94.0','cassette_8','Pupil_N964-8.fits'
56475,'i DECam SDSS c0003 7835.0 1470.0','cassette_3','Pupil_i-3.fits'
56475,'N501 DECam c0010 5010.0 75.0','cassette_6','Pupil_N501-6.fits'
56475,'N673 DECam c0011 6730.0 100.0','cassette_4','Pupil_N673-4.fits'
59261,'N964 DECam c0008 9645.0 94.0','cassette_8','PUPIL_20210215_99ec7c1-N964-8.fits'
59272,'N708 DECam c0012 7080.0 400.0','cassette_4','PUPIL_20210226_99fc07b-N708-4.fits'
59470,'N419 DECam c0013 4194.0 75.0','cassette_4','PUPIL_20210923_9bb8bb6-N419.fits'


56475,'N662 DECam c0009 6620.0 172.0',NULL,'Pupil_N662-8.fits'
56475,'VR DECam c0007 6300.0 2600.0',NULL,'Pupil_VR-6.fits'
56475,'N501 DECam c0010 5010.0 75.0',NULL,'Pupil_N501-6.fits'
56475,'N673 DECam c0011 6730.0 100.0',NULL,'Pupil_N673-4.fits'
56475,'N964 DECam c0008 9645.0 94.0',NULL,'Pupil_N964-8.fits'
59261,'N964 DECam c0008 9645.0 94.0',NULL,'PUPIL_20210215_99ec7c1-N964-8.fits'
59272,'N708 DECam c0012 7080.0 400.0',NULL,'PUPIL_20210226_99fc07b-N708-4.fits'
59470,'N419 DECam c0013 4194.0 75.0',NULL,'PUPIL_20210923_9bb8bb6-N419.fits'
