  ŠV  @ÿ  
š$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1288869182
$MTIME = 1288869182
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-11-03T18:13:02' / Date FITS file was generated
IRAF-TLM= '2020-11-03T18:12:59' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                 900. / [s] Requested exposure duration
EXPTIME =                 900. / [s] Exposure duration
EXPDUR  =                 900. / [s] Exposure duration
DARKTIME=          901.3172998 / [s] Dark time
OBSID   = 'ct4m20201011t070717' / Unique Observation ID
DATE-OBS= '2020-10-11T07:07:17.766615' / DateTime of observation start (UTC)
TIME-OBS= '07:07:17.766615'    / Time of observation start (UTC)
MJD-OBS =       59133.29673341 / MJD of observation start
MJD-END =      59133.307150077 / Exposure end
OPENSHUT= '2020-10-11T07:07:17.996880' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               944707 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'The variable star population in the isolated Phoenix dwarf &'
CONTINUE= '        '           /   '&' / Current observing orogram
OBSERVER= 'Kathy Vivas'        / Observer name(s)
PROPOSER= 'Martinez-Vazquez'   / Proposal Principle Investigator
DTPI    = 'Martinez-Vazquez'   / Proposal Principle Investigator (iSTB)
PROPID  = '2020B-0922'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'i DECam SDSS c0003 7835.0 1470.0' / Unique filter identifier
FILTPOS = 'cassette_3'         / Filter position in FCM
INSTANCE= 'DECam_20201010'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '01:51:11.923'       / [HH:MM:SS] Target RA
DEC     = '-44:30:36.626'      / [DD:MM:SS] Target DEC
TELRA   = '01:51:11.992'       / [HH:MM:SS] Telescope RA
TELDEC  = '-44:30:36.994'      / [DD:MM:SS] Telescope DEC
HA      = '01:53:01.140'       / [HH:MM:SS] Telescope hour angle
ZD      =                26.37 / [deg] Telescope zenith distance
AZ      =             229.4767 / [deg] Telescope azimuth angle
DOMEAZ  =              226.952 / [deg] Dome azimuth angle
ZPDELRA =             -20.6427 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                  8.9 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '1243.69,1766.93,2444.69,-197.29,-22.84, 0.00' / DECam hexapod setting
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =                7.886 / [m/s] Wind speed
WINDDIR =                 307. / [deg] Wind direction (from North)
HUMIDITY=                  19. / [%] Ambient relative humidity (outside)
PRESSURE=                 779. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE= 'NaN     '           / [arcsec] DIMM2 Seeing
MASS2   = 'NaN     '           /  MASS(2) FSEE
ASTIG1  =                 0.03 / 4MAPS correction 1
ASTIG2  =                -0.03 / 4MAPS correction 2
OUTTEMP =                 15.2 / [deg C] Outside temperature
AIRMASS =                 1.12 / Airmass
GSKYVAR =                0.041 / RASICAM global sky standard deviation
GSKYHOT =                 0.89 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=                 14.4 / [deg C] Mirror surface average temperature
MAIRTEMP=                 14.5 / [deg C] Mirror temperature above surface
UPTRTEMP=               14.881 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 13.6 / [deg C] Mirror top surface temperature
UTN-TEMP=                 15.2 / [deg C] Upper truss temperature north
UTS-TEMP=               14.495 / [deg C] Upper truss temperature south
UTW-TEMP=                15.25 / [deg C] Upper truss temperature west
UTE-TEMP=                14.58 / [deg C] Upper truss temperature east
PMN-TEMP=                 14.4 / [deg C] Mirror north edge temperature
PMS-TEMP=                 14.5 / [deg C] Mirror south edge temperature
PMW-TEMP=                 14.1 / [deg C] Mirror west edge temperature
PME-TEMP=                 14.6 / [deg C] Mirror east edge temperature
DOMELOW =               14.575 / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                  9.3 / [deg C] Dome floor temperature
G-MEANX =               0.0446 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0485 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.28,1.92,-8.58,0.26,0.05,-0.04,0.25,-0.02,-0.22,]' / Mean Wavefront
DONUTFS3= '[0.63,1.20,9.15,0.00,0.00,-0.04,-0.03,0.02,-0.23,]' / Mean Wavefront
DONUTFS2= '[-0.06,-0.76,-8.70,0.11,0.22,-0.15,0.07,0.42,0.01,]' / Mean Wavefront
DONUTFS1= '[0.75,1.19,8.78,0.18,0.23,0.04,-0.02,0.24,0.13,]' / Mean Wavefront fo
G-FLXVAR=         26297643.644 / [arcsec] Guider mean guide star flux variances
G-MEANXY=             0.000897 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[0.17,0.69,-8.79,-0.22,-0.08,-0.12,-0.11,0.22,-0.18,]' / Mean Wavefro
DONUTFN2= '[2.35,0.89,8.73,-0.33,-0.15,0.02,-0.03,0.21,0.01,]' / Mean Wavefront
DONUTFN3= '[0.27,1.25,-8.53,0.48,-0.25,-0.05,-0.18,-0.01,0.23,]' / Mean Wavefron
DONUTFN4= '[1.23,1.17,9.43,0.16,-0.31,-0.03,-0.01,-0.07,0.04,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2020-10-11T07:22:39.991848'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    4 / Number of guide CCDs that remained active
DOXT    =                 0.03 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.4487 / [arcsec] Guider x-axis maximum offset
FADZ    =               -32.23 / [um] FA Delta focus.
FADY    =               263.72 / [um] FA Delta Y.
FADX    =               112.34 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                -1.01 / [arcsec] FA Delta Y-theta.
DODZ    =               -32.23 / [um] Delta-Z from donut analysis
DODY    =                -0.83 / [um] Y-decenter from donut analysis
DODX    =                -0.94 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2020-10-11T07:06:51' / Time of last RASICAM exposure (UTC)
G-SEEING=                1.869 / [arcsec] Guider average seeing
G-TRANSP=                0.729 / Guider average sky transparency
G-MEANY2=             0.013851 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.08 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.285 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =               -31.23 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.4804 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.013091 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:86'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTPROPID= '2020B-0922'
DTCALDAT= '2020-10-10'
DTSITE  = 'ct      '
DTTELESC= 'ct4m    '
DTACQNAM= '/data_local/images/DTS/2020B-0922/DECam_00944707.fits.fz'
DTINSTRU= 'decam   '
ODATEOBS= '2020-10-11T07:07:17.766615'
DTNSANAM= 'c4d_201011_070717_ori.fits.fz'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.63427213085208
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     42956.4497041111
GAINB   =     3.57575666900754
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     39685.2258516198
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =   9.684230777771E-05 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -7.237035926193E-03 / Projection distortion parameter
PV2_9   =   6.468132179531E-03 / Projection distortion parameter
CD1_1   =  -2.4815399663814E-6 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -8.628898741370E-04 / Projection distortion parameter
PV2_1   =   1.007043375700E+00 / Projection distortion parameter
PV2_2   =  -6.944633161438E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -8.995234096399E-03 / Projection distortion parameter
PV2_5   =   1.390665414472E-02 / Projection distortion parameter
PV2_6   =  -9.036665177649E-03 / Projection distortion parameter
PV2_7   =   3.683177040385E-03 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   3.906774330881E-03 / Projection distortion parameter
PV2_10  =  -2.540663630798E-03 / Projection distortion parameter
PV1_4   =   6.940841430470E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -4.119736356946E-03 / Projection distortion parameter
PV1_1   =   1.013864371067E+00 / Projection distortion parameter
PV1_0   =   2.170114323004E-03 / Projection distortion parameter
PV1_9   =   7.117865431169E-03 / Projection distortion parameter
PV1_8   =  -6.130507938243E-03 / Projection distortion parameter
CD1_2   =  0.00466292922994305 / Linear projection matrix
PV1_5   =  -1.808833204523E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046625594146861 / Linear projection matrix
CD2_2   =  -2.1731193029389E-6 / Linear projection matrix
PV1_10  =  -1.252913416519E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   2.780231933101E+01 / World coordinate on this axis
CRVAL2  =  -4.451067405135E+01 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Mon Oct 12 10:22:45 2020' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Mon Oct 12 10:22:45 2020' / overscan corrected
BIASFIL = 'DEC20A_AZ20201008_98d1ca4-59132Z_01.fits' / Bias correction file
BAND    = 'i       '
NITE    = '20201010'
DESBIAS = 'Mon Oct 12 10:27:27 2020'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Mon Oct 12 10:27:28 2020'
DESBPM  = 'Mon Oct 12 10:27:28 2020' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Mon Oct 12 10:27:28 2020' / Flag saturated pixels
NSATPIX =                24823 / Number of saturated pixels
FLATMEDA=     3.63427213085208
FLATMEDB=     3.57575666900754
SATURATE=     42956.4497041111
DESGAINC= 'Mon Oct 12 10:27:28 2020'
FLATFIL = 'DEC20A_AZ20201008_98d1ca4-59132iF_01.fits' / Dome flat correction fil
DESFLAT = 'Mon Oct 12 10:27:28 2020'
STARFIL = 'STARFLAT_20201010_98d2bf0-iP_ci_01.fits' / Star flat correction file
DESSTAR = 'Mon Oct 12 10:27:28 2020'
HISTORY Mon Oct 12 10:27:28 2020 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Mon Oct 12 10:43:00 2020' / bleed trail masking
NBLEED  = 48614  / Number of bleed trail pixels.
STARMASK= 'Mon Oct 12 10:43:00 2020' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC20B_CMV20201008_98d
HISTORY 410b-fili-ccd01i/DEC20B_CMV20201008_98d410b-i-201011070717_01.fits omibl
HISTORY eed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Oct 12 10:43 Bad pixel file is omibleed_01_bpm.pl'
FWHM    =             5.091173 / Median FWHM in pixels
ELLIPTIC=            0.1245608 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Mon Oct 12 11:01:39 2020' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
PHOTREF = 'Gaia    '           / Photometric ref. catalog
MAGZERO =               33.080 / [mag] Magnitude zero point
MAGZPT  =               25.694 / [mag] Magnitude zero point per sec
NPHTMTCH=                 3896 / Number of phot. stars matched
RADIUS  =                 2.34 / [pix] Average radius of stars
RADSTD  =                0.051 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             8698.167
NCOMBINE=                31380
PUPILSKY=     8662.38515006506
PUPILSUM=    -1657714.84311704
PUPILMAX=     10.9006280899048
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'NfEMOdCKNdCKNdCK'   / HDU checksum updated 2020-11-03T17:51:45
DATASUM = '555831887'          / data unit checksum updated 2020-11-03T17:51:45
NPUPSCL =                22717
PUPSCL  =     0.19096207073159
    Ț     Ą  Ì                 	X  	X  Ì  (  
š      êÿÿÿ               ÿ(       @A``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
 
@`
`
`
`
`
`
 
@`
 
@`
 
@`
 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
````````````````` @ @ @ @ @ @` @ @ @ @ @ @`````````````````` @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
 
@`
`
`
`
`
`
 
@`
`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@L   ÿ      Ą A ÿ      AĄ  ÿ 
   
  @Á "@Ÿ  ÿ 
   
  @č 2@¶  ÿ 
   
  @Ž <@±  ÿ 
   
  @Ż F@Ź  ÿ 
   
  @« N@š  ÿ 
   
  @§ V@€  ÿ 
   
  @€ \@Ą  ÿ 
   
  @Ą b@  ÿ 
   
  @ h@  ÿ 
   
  @ n@  ÿ 
   
  @ t@  ÿ 
   
  @ x@  ÿ 
   
  @ ~@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @  @  ÿ 
   
  @ €@}  ÿ 
   
  @~ š@{  ÿ 
   
  @} Ș@z  ÿ 
   
  @{ ź@x  ÿ 
   
  @z °@w  ÿ 
   
  @x Ž@u  ÿ 
   
  @w ¶@t  ÿ 
   
  @u ș@r  ÿ 
   
  @t Œ@q  ÿ 
   
  @s ż@o  ÿ 
   
  @q Â@n  ÿ 
   
  @p Ä@m  ÿ 
   
  @o Æ@l  ÿ 
   
  @m Ê@j  ÿ 
   
  @l Ì@i  ÿ 
   
  @k Î@h  ÿ 
   
  @j Đ@g  ÿ 
   
  @i Ò@f  ÿ 
   
  @h Ô@e  ÿ 
   
  @f Ű@c  ÿ 
   
  @e Ú@b  ÿ 
   
  @d Ü@a  ÿ 
   
  @c Ț@`  ÿ 
   
  @b à@_  ÿ 
   
  @a â@^  ÿ 
   
  @` ä@]  ÿ 
   
  @_ æ@\  ÿ 
   
  @^ è@[  ÿ 
   
  @] ê@Z  ÿ 
   
  @\ ì@Y  ÿ 
   
  @[ î@X  ÿ 
   
  @Z đ@W  ÿ 
   
  @Y ò@V  ÿ 
   
  @X ô@U  ÿ 
   
  @W ö@T  ÿ 
   
  @V ű@S  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S ț@P  ÿ 
   
  @S ÿ@O  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@L  ÿ 
   
  @N@K  ÿ 
   
  @M
@J  ÿ 
   
  @L@I  ÿ 
   
  @K@H  ÿ 
   
  @J@G  ÿ 
   
  @I@F  ÿ 
   
  @H@E  ÿ 
   
  @G@D  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@A  ÿ 
   
  @C@@  ÿ 
   
  @B @?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?&@<  ÿ 
   
  @>(@;  ÿ 
   
  @=*@:  ÿ 
   
  @<,@9  ÿ 
   
  @;.@8  ÿ 
   
  @:0@7  ÿ 
   
  @92@6  ÿ 
   
  @84@5  ÿ      @7 @ @4  ÿ      @7 @ @4  ÿ      @7 @  @4  ÿ      @7 @$ @4  ÿ      @6 @( @3  ÿ      @6 @, @3  ÿ      @6 @. @3  ÿ      @6 @1 @3  ÿ      @6 @4 @3  ÿ      @5 @6 @2  ÿ      @5 @8 @2  ÿ      @5 @: @2  ÿ      @5 @< @2  ÿ      @5 ~@> ~@2  ÿ      @4 ~@@ ~@1  ÿ      @4 ~@A }@1  ÿ      @4 }@B }@1  ÿ      @4 |@D |@1  ÿ      @4 {@F {@1  ÿ      @4 z@H z@1  ÿ      @3 z@J z@0  ÿ      @3 y@L y@0  ÿ      @3 x@N x@0  ÿ      @3 w@P w@0  ÿ      @3 w@Q w@/  ÿ      @3 w@P w@0  ÿ      @3 x@N x@0  ÿ      @3 y@L y@0  ÿ      @3 z@J z@0  ÿ      @4 z@H z@1  ÿ      @4 {@F {@1  ÿ      @4 |@D |@1  ÿ      @4 }@B }@1  ÿ      @4 ~@A }@1  ÿ      @4 ~@@ ~@1  ÿ      @5 ~@> ~@2  ÿ      @5 @< @2  ÿ      @5 @: @2  ÿ      @5 @8 @2  ÿ      @5 @6 @2  ÿ      @6 @4 @3  ÿ      @6 @1 @3  ÿ      @6 @. @3  ÿ      @6 @, @3  ÿ      @6 @( @3  ÿ      @7 @$ @4  ÿ      @7 @  @4  ÿ      @7 @ @4  ÿ      @7 @ @4  ÿ 
   
  @84@5  ÿ 
   
  @92@6  ÿ 
   
  @:0@7  ÿ 
   
  @;.@8  ÿ 
   
  @<,@9  ÿ 
   
  @=*@:  ÿ 
   
  @>(@;  ÿ 
   
  @?&@<  ÿ 
   
  @@$@=  ÿ 
   
  @A"@>  ÿ 
   
  @B @?  ÿ 
   
  @C@@  ÿ 
   
  @D@A  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @G@D  ÿ 
   
  @H@E  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@H  ÿ 
   
  @L@I  ÿ 
   
  @M
@J  ÿ 
   
  @N@K  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S ÿ@O  ÿ 
   
  @S ț@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ű@S  ÿ 
   
  @W ö@T  ÿ 
   
  @X ô@U  ÿ 
   
  @Y ò@V  ÿ 
   
  @Z đ@W  ÿ 
   
  @[ î@X  ÿ 
   
  @\ ì@Y  ÿ 
   
  @] ê@Z  ÿ 
   
  @^ è@[  ÿ 
   
  @_ æ@\  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @b à@_  ÿ 
   
  @c Ț@`  ÿ 
   
  @d Ü@a  ÿ 
   
  @e Ú@b  ÿ 
   
  @f Ű@c  ÿ 
   
  @h Ô@e  ÿ 
   
  @i Ò@f  ÿ 
   
  @j Đ@g  ÿ 
   
  @k Î@h  ÿ 
   
  @l Ì@i  ÿ 
   
  @m Ê@j  ÿ 
   
  @o Æ@l  ÿ 
   
  @p Ä@m  ÿ 
   
  @q Â@n  ÿ 
   
  @s ż@o  ÿ 
   
  @t Œ@q  ÿ 
   
  @u ș@r  ÿ 
   
  @w ¶@t  ÿ 
   
  @x Ž@u  ÿ 
   
  @z °@w  ÿ 
   
  @{ ź@x  ÿ 
   
  @} Ș@z  ÿ 
   
  @~ š@{  ÿ 
   
  @ €@}  ÿ 
   
  @  @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ~@  ÿ 
   
  @ x@  ÿ 
   
  @ t@  ÿ 
   
  @ n@  ÿ 
   
  @ h@  ÿ 
   
  @Ą b@  ÿ 
   
  @€ \@Ą  ÿ 
   
  @§ V@€  ÿ 
   
  @« N@š  ÿ 
   
  @Ż F@Ź  ÿ 
   
  @Ž <@±  ÿ 
   
  @č 2@¶  ÿ 
   
  @Á "@Ÿ L ÿ      AĄ