  ŠV  =  
$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1299144356
$MTIME = 1299144356
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2021-03-02T16:25:56' / Date FITS file was generated
IRAF-TLM= '2021-03-02T16:25:59' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                 600. / [s] Requested exposure duration
EXPTIME =                 600. / [s] Exposure duration
EXPDUR  =                 600. / [s] Exposure duration
DARKTIME=          601.2583699 / [s] Dark time
OBSID   = 'ct4m20210227t022058' / Unique Observation ID
DATE-OBS= '2021-02-27T02:20:58.197735' / DateTime of observation start (UTC)
TIME-OBS= '02:20:58.197735'    / Time of observation start (UTC)
MJD-OBS =       59272.09789581 / MJD of observation start
MJD-END =      59272.104840254 / Exposure end
OPENSHUT= '2021-02-27T02:20:58.356870' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               968906 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'Merian  '           / Current observing orogram
OBSERVER= 'Alfredo Zenteno'    / Observer name(s)
PROPOSER= 'LeathaudGreene'     / Proposal Principle Investigator
DTPI    = 'LeathaudGreene'     / Proposal Principle Investigator (iSTB)
PROPID  = '2020B-0288'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
SEQID   = 'Engineering run test' / Sequence name
SEQNUM  =                    2 / Number of image in sequence
SEQTOT  =                    6 / Total number of images in sequence
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'N708 DECam c0012 7080.0 400.0' / Unique filter identifier
INSTANCE= 'DECam_20210226'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '08:42:10.116'       / [HH:MM:SS] Target RA
DEC     = '-02:20:18.132'      / [DD:MM:SS] Target DEC
TELRA   = '08:42:10.130'       / [HH:MM:SS] Telescope RA
TELDEC  = '-02:20:20.299'      / [DD:MM:SS] Telescope DEC
HA      = '-00:37:05.130'      / [HH:MM:SS] Telescope hour angle
ZD      =                29.11 / [deg] Telescope zenith distance
AZ      =              19.3959 / [deg] Telescope azimuth angle
DOMEAZ  =                20.03 / [deg] Dome azimuth angle
ZPDELRA =              18.0241 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                -28.8 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-1051.66,-975.25,2167.10,-65.93,-185.96,-0.00' / DECam hexapod settin
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =                9.012 / [m/s] Wind speed
WINDDIR =                  17. / [deg] Wind direction (from North)
HUMIDITY=                  51. / [%] Ambient relative humidity (outside)
PRESSURE=                 778. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                1.981 / [arcsec] DIMM2 Seeing
MASS2   =                 1.14 /  MASS(2) FSEE
ASTIG1  =                -0.15 / 4MAPS correction 1
ASTIG2  =                -0.08 / 4MAPS correction 2
OUTTEMP =                 17.6 / [deg C] Outside temperature
AIRMASS =                 1.14 / Airmass
GSKYVAR =                0.042 / RASICAM global sky standard deviation
GSKYHOT =                0.074 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               17.325 / [deg C] Mirror surface average temperature
MAIRTEMP=                 17.5 / [deg C] Mirror temperature above surface
UPTRTEMP=               17.636 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 16.8 / [deg C] Mirror top surface temperature
UTN-TEMP=                17.78 / [deg C] Upper truss temperature north
UTS-TEMP=               17.395 / [deg C] Upper truss temperature south
UTW-TEMP=               17.905 / [deg C] Upper truss temperature west
UTE-TEMP=               17.465 / [deg C] Upper truss temperature east
PMN-TEMP=                 17.1 / [deg C] Mirror north edge temperature
PMS-TEMP=                 17.5 / [deg C] Mirror south edge temperature
PMW-TEMP=                 17.2 / [deg C] Mirror west edge temperature
PME-TEMP=                 17.5 / [deg C] Mirror east edge temperature
DOMELOW =                18.16 / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                  13. / [deg C] Dome floor temperature
G-MEANX =              -0.0092 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0846 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[]      '           / Mean Wavefront for Sensor FS4
DONUTFS3= '[0.35,2.21,8.83,-0.07,-0.16,0.03,-0.09,0.01,-0.28,]' / Mean Wavefront
DONUTFS2= '[0.92,1.11,-8.77,0.14,0.17,-0.02,-0.03,0.31,-0.10,]' / Mean Wavefront
DONUTFS1= '[1.59,0.95,8.85,0.30,0.15,0.09,-0.08,0.15,0.12,]' / Mean Wavefront fo
G-FLXVAR=         32659758.468 / [arcsec] Guider mean guide star flux variances
G-MEANXY=            -0.005831 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[-0.09,0.47,-8.83,-0.10,-0.02,0.04,-0.15,0.23,-0.27,]' / Mean Wavefro
DONUTFN2= '[]      '           / Mean Wavefront for Sensor FN2
DONUTFN3= '[0.35,1.52,-8.78,0.06,-0.04,0.04,-0.21,0.03,0.27,]' / Mean Wavefront
DONUTFN4= '[1.25,1.03,9.35,0.03,-0.25,0.04,-0.09,-0.09,0.04,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2021-02-27T02:31:20.826009'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    3 / Number of guide CCDs that remained active
DOXT    =                 0.02 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.7125 / [arcsec] Guider x-axis maximum offset
FADZ    =               -18.81 / [um] FA Delta focus.
FADY    =               -196.8 / [um] FA Delta Y.
FADX    =              -202.16 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =               -11.99 / [arcsec] FA Delta Y-theta.
DODZ    =               -18.81 / [um] Delta-Z from donut analysis
DODY    =                -0.73 / [um] Y-decenter from donut analysis
DODX    =                -1.21 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2021-02-27T02:20:32' / Time of last RASICAM exposure (UTC)
G-SEEING=                2.131 / [arcsec] Guider average seeing
G-TRANSP=                0.907 / Guider average sky transparency
G-MEANY2=             0.031029 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.06 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.302 / [s] Guider avg. latency between exposures
LUTVER  = 'not available'      / Hexapod Lookup Table version
FAXT    =                -3.82 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.4974 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.046375 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:90'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTACQNAM= '/data_local/images/DTS/2020B-0288/DECam_00968906.fits.fz'
DTTELESC= 'ct4m    '
DTSITE  = 'ct      '
DTCALDAT= '2021-02-26'
DTNSANAM= 'c4d_210227_022058_ori.fits.fz'
DTINSTRU= 'decam   '
DTPROPID= '2020B-0288'
ODATEOBS= '2021-02-27T02:20:58.197735'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =      3.5598967563014
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     43853.9201238516
GAINB   =     3.49589239681363
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     40591.8417653074
CRPIX1  =             209.3625 / Coordinate Reference axis 1
CRPIX2  =        230.270828125 / Coordinate Reference axis 2
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 0x0 4.010000' / Monsoon module
SLOT02  = 'DESCB 0x0 4.010000' / Monsoon module
SLOT03  = 'CCD12 0x0 4.080000' / Monsoon module
SLOT04  = 'CCD12 0x0 4.080000' / Monsoon module
SLOT05  = 'CCD12 0x0 4.080000' / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =                2000. / [yr] Equinox of WCS
PV1_7   =    0.000618368874624 / PV distortion coefficient
CUNIT1  = 'deg     '
PV2_8   =    -0.00842887692623 / PV distortion coefficient
PV2_9   =     0.00790302236959 / PV distortion coefficient
CD1_1   =  -1.1679624622720E-5 / World coordinate transformation matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =    -0.00441101222605 / PV distortion coefficient
PV2_1   =        1.01700592995 / PV distortion coefficient
PV2_2   =    -0.00863195988986 / PV distortion coefficient
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =     -0.0181936225912 / PV distortion coefficient
PV2_5   =      0.0168279891847 / PV distortion coefficient
PV2_6   =      -0.010365069688 / PV distortion coefficient
PV2_7   =     0.00641705596954 / PV distortion coefficient
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =       0.010153274645 / PV distortion coefficient
PV2_10  =    -0.00261077202228 / PV distortion coefficient
PV1_4   =     0.00862489761958 / PV distortion coefficient
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =    -0.00978790747587 / PV distortion coefficient
PV1_1   =        1.01430869337 / PV distortion coefficient
PV1_0   =     0.00392185213728 / PV distortion coefficient
PV1_9   =     0.00654393666949 / PV distortion coefficient
PV1_8   =    -0.00736713022181 / PV distortion coefficient
CD1_2   =  0.00466262971156481 / World coordinate transformation matrix
PV1_5   =      -0.017864081795 / PV distortion coefficient
CUNIT2  = 'deg     '
CD2_1   =  -0.0046623936290495 / World coordinate transformation matrix
CD2_2   =  -1.1659626630144E-5 / World coordinate transformation matrix
PV1_10  =    -0.00361076208606 / PV distortion coefficient
CTYPE2  = 'DEC--TPV'           / Coordinate type
CTYPE1  = 'RA---TPV'           / Coordinate type
CRVAL1  =           130.542208 / [deg] WCS Reference Coordinate (RA)
CRVAL2  =            -2.338972 / [deg] WCS Reference Coordinate (DEC)
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Mon Mar  1 17:42:00 2021' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Mon Mar  1 17:42:00 2021' / overscan corrected
BIASFIL = 'DEC21A_AR20210226_99f6312-59271Z_01.fits' / Bias correction file
BAND    = 'N708    '
NITE    = '20210226'
DESBIAS = 'Mon Mar  1 17:57:56 2021'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Mon Mar  1 17:57:57 2021'
DESBPM  = 'Mon Mar  1 17:57:57 2021' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Mon Mar  1 17:57:57 2021' / Flag saturated pixels
NSATPIX =                 8230 / Number of saturated pixels
FLATMEDA=      3.5598967563014
FLATMEDB=     3.49589239681363
SATURATE=     43853.9201238516
DESGAINC= 'Mon Mar  1 17:57:57 2021'
FLATFIL = 'DEC21A_AR20210226_99f6312-59271N708F_01.fits' / Dome flat correction
DESFLAT = 'Mon Mar  1 17:57:57 2021'
STARFIL = 'STARFLAT_20210226_99f7f11-N708P_ci_01.fits' / Star flat correction fi
DESSTAR = 'Mon Mar  1 17:57:57 2021'
HISTORY Mon Mar  1 17:57:57 2021 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
AMPMTCH = 'B 1.0053 (0.0005,0.0200)' / Amp match
DESBLEED= 'Mon Mar 1 20:31:04 2021' / bleed trail masking
NBLEED  = 8519  / Number of bleed trail pixels.
STARMASK= 'Mon Mar 1 20:31:04 2021' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/PUPIL_20210226_99fc07b
HISTORY -filN708-ccd01N708/PUPIL_20210226_99fc07b-N708-210227022058_01.fits omib
HISTORY leed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Mar  1 20:31 Bad pixel file is omibleed_01_bpm.pl'
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             8451.882
NCOMBINE=                 5040
PUPILSKY=     7693.55526583249
PUPILSUM=    -3311731.64787346
PUPILMAX=     30.7133655548096
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'PJCDSHCDPHCDPHCD'   / HDU checksum updated 2021-03-02T16:22:30
DATASUM = '1744167956'         / data unit checksum updated 2021-03-02T16:22:30
NPUPSCL =                28658
PUPSCL  =    0.079523495216662
    Ț     Ą  Ì                 	P  	P  Ì  %  
      êÿÿÿ               ÿ%       @F``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
 
@`
`
`
`
`
`
 
@`
 
@`
 
@`
 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
````````````````````` @` @`` @ @` @` @ @ @``````````````` @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
 
@`
`
`
`
`
`
 
@`
`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@Q   ÿ      Ą F ÿ      AĄ  ÿ 
   
  @Á "@Ÿ  ÿ 
   
  @ș 0@·  ÿ 
   
  @Ž <@±  ÿ 
   
  @° D@­  ÿ 
   
  @« N@š  ÿ 
   
  @š T@„  ÿ 
   
  @€ \@Ą  ÿ 
   
  @Ą b@  ÿ 
   
  @ h@  ÿ 
   
  @ l@  ÿ 
   
  @ r@  ÿ 
   
  @ v@  ÿ 
   
  @ |@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ą@~  ÿ 
   
  @ €@}  ÿ 
   
  @~ š@{  ÿ 
   
  @} Ș@z  ÿ 
   
  @{ ź@x  ÿ 
   
  @z °@w  ÿ 
   
  @x Ž@u  ÿ 
   
  @w ¶@t  ÿ 
   
  @v č@r  ÿ 
   
  @t Œ@q  ÿ 
   
  @s Ÿ@p  ÿ 
   
  @r À@o  ÿ 
   
  @p Ä@m  ÿ 
   
  @o Æ@l  ÿ 
   
  @n È@k  ÿ 
   
  @m Ê@j  ÿ 
   
  @l Ì@i  ÿ 
   
  @k Î@h  ÿ 
   
  @i Ò@f  ÿ 
   
  @h Ô@e  ÿ 
   
  @g Ö@d  ÿ 
   
  @f Ű@c  ÿ 
   
  @e Ú@b  ÿ 
   
  @d Ü@a  ÿ 
   
  @c Ț@`  ÿ 
   
  @b à@_  ÿ 
   
  @a â@^  ÿ 
   
  @` ä@]  ÿ 
   
  @_ æ@\  ÿ 
   
  @^ è@[  ÿ 
   
  @] ê@Z  ÿ 
   
  @\ ì@Y  ÿ 
   
  @[ î@X  ÿ 
   
  @Z đ@W  ÿ 
   
  @Y ò@V  ÿ 
   
  @X ô@U  ÿ 
   
  @W ö@T  ÿ 
   
  @W ś@S  ÿ 
   
  @V ű@S  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S ț@P  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@L  ÿ 
   
  @N@K  ÿ 
   
  @M
@J  ÿ 
   
  @L@I  ÿ 
   
  @K@H  ÿ 
   
  @J@G  ÿ 
   
  @I@F  ÿ 
   
  @H@E  ÿ 
   
  @G@D  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@A  ÿ 
   
  @C@@  ÿ 
   
  @B @?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?&@<  ÿ 
   
  @>(@;  ÿ 
   
  @=*@:  ÿ 
   
  @<,@9  ÿ 
   
  @;.@8  ÿ      @; @ @8  ÿ      @; @ @8  ÿ      @: @ @7  ÿ      @: @ @7  ÿ      @: @  @7  ÿ      @: @# @7  ÿ      @: @& @7  ÿ      @: @( @7  ÿ      @9 @+ @6  ÿ      @9 @- @6  ÿ      @9 @/ @6  ÿ      @9 @1 @6  ÿ      @9 @2 @6  ÿ      @9 ~@4 @6  ÿ      @9 }@6 @6  ÿ      @8 }@7 @5  ÿ      @8 }@8 @5  ÿ      @8 |@9 @5  ÿ      @8 |@: ~@5  ÿ      @8 {@; ~@5  ÿ      @8 {@< }@5  ÿ      @8 z@= }@5  ÿ      @8 z@> |@5  ÿ      @8 y@? |@5  ÿ      @8 y@@ {@5  ÿ      @8 x@A {@5  ÿ      @8 x@A |@4  ÿ      @8 x@A {@5  ÿ      @8 y@@ {@5  ÿ      @8 y@? |@5  ÿ      @8 z@> |@5  ÿ      @8 z@= }@5  ÿ      @8 {@< }@5  ÿ      @8 |@: ~@5  ÿ      @8 }@8 @5  ÿ      @9 }@6 @6  ÿ      @9 }@5 @6  ÿ      @9 ~@4 @6  ÿ      @9 @2 @6  ÿ      @9 @0 @6  ÿ      @9 @. @6  ÿ      @9 @, @6  ÿ      @: @* @7  ÿ      @: @( @7  ÿ      @: @$ @7  ÿ      @: @" @7  ÿ      @: @ @7  ÿ      @: @ @7  ÿ      @; @ @8  ÿ      @; @ @8  ÿ 
   
  @;.@8  ÿ 
   
  @<,@9  ÿ 
   
  @=*@:  ÿ 
   
  @>(@;  ÿ 
   
  @?&@<  ÿ 
   
  @@$@=  ÿ 
   
  @A"@>  ÿ 
   
  @B @?  ÿ 
   
  @C@@  ÿ 
   
  @D@A  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @G@D  ÿ 
   
  @H@E  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@H  ÿ 
   
  @L@I  ÿ 
   
  @M
@J  ÿ 
   
  @N@K  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S ț@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ű@S  ÿ 
   
  @W ś@S  ÿ 
   
  @W ö@T  ÿ 
   
  @X ô@U  ÿ 
   
  @Y ò@V  ÿ 
   
  @Z đ@W  ÿ 
   
  @[ î@X  ÿ 
   
  @\ ì@Y  ÿ 
   
  @] ê@Z  ÿ 
   
  @^ è@[  ÿ 
   
  @_ æ@\  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @b à@_  ÿ 
   
  @c Ț@`  ÿ 
   
  @d Ü@a  ÿ 
   
  @e Ú@b  ÿ 
   
  @f Ű@c  ÿ 
   
  @g Ö@d  ÿ 
   
  @h Ô@e  ÿ 
   
  @i Ò@f  ÿ 
   
  @k Î@h  ÿ 
   
  @l Ì@i  ÿ 
   
  @m Ê@j  ÿ 
   
  @n È@k  ÿ 
   
  @o Æ@l  ÿ 
   
  @p Ä@m  ÿ 
   
  @r À@o  ÿ 
   
  @s Ÿ@p  ÿ 
   
  @t Œ@q  ÿ 
   
  @v č@r  ÿ 
   
  @w ¶@t  ÿ 
   
  @x Ž@u  ÿ 
   
  @z °@w  ÿ 
   
  @{ ź@x  ÿ 
   
  @} Ș@z  ÿ 
   
  @~ š@{  ÿ 
   
  @ €@}  ÿ 
   
  @ ą@~  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ |@  ÿ 
   
  @ v@  ÿ 
   
  @ r@  ÿ 
   
  @ l@  ÿ 
   
  @ h@  ÿ 
   
  @Ą b@  ÿ 
   
  @€ \@Ą  ÿ 
   
  @š T@„  ÿ 
   
  @« N@š  ÿ 
   
  @° D@­  ÿ 
   
  @Ž <@±  ÿ 
   
  @ș 0@·  ÿ 
   
  @Á "@Ÿ Q ÿ      AĄ