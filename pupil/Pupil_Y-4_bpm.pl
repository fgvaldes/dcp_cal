  ŠV  @ą  	Ê$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1288889916
$MTIME = 1288889916
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-11-03T23:58:36' / Date FITS file was generated
IRAF-TLM= '2020-11-03T23:58:38' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  30. / [s] Requested exposure duration
EXPTIME =                  30. / [s] Exposure duration
EXPDUR  =                  30. / [s] Exposure duration
DARKTIME=           31.2646699 / [s] Dark time
OBSID   = 'ct4m20201011t001917' / Unique Observation ID
DATE-OBS= '2020-10-11T00:19:17.131350' / DateTime of observation start (UTC)
TIME-OBS= '00:19:17.131350'    / Time of observation start (UTC)
MJD-OBS =       59133.01339272 / MJD of observation start
MJD-END =      59133.013739942 / Exposure end
OPENSHUT= '2020-10-11T00:19:17.306850' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               944542 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'DECam Engineering'  / Current observing orogram
OBSERVER= 'Kathy Vivas'        / Observer name(s)
PROPOSER= 'Walker  '           / Proposal Principle Investigator
DTPI    = 'Walker  '           / Proposal Principle Investigator (iSTB)
PROPID  = '2013A-9999'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'Y DECam c0005 10095.0 1130.0' / Unique filter identifier
FILTPOS = 'cassette_4'         / Filter position in FCM
INSTANCE= 'DECam_20201010'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '20:39:37.487'       / [HH:MM:SS] Target RA
DEC     = '-34:57:49.000'      / [DD:MM:SS] Target DEC
TELRA   = '20:39:37.487'       / [HH:MM:SS] Telescope RA
TELDEC  = '-34:57:49.496'      / [DD:MM:SS] Telescope DEC
HA      = '00:15:10.490'       / [HH:MM:SS] Telescope hour angle
ZD      =                 5.68 / [deg] Telescope zenith distance
AZ      =             212.7622 / [deg] Telescope azimuth angle
DOMEAZ  =              207.481 / [deg] Dome azimuth angle
ZPDELRA =              -23.686 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                  8.9 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-74.16,1371.54,2402.55,-161.28,-108.33, 0.00' / DECam hexapod setting
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    T / RASICAM local sky clear flag
WINDSPD =               10.461 / [m/s] Wind speed
WINDDIR =                  36. / [deg] Wind direction (from North)
HUMIDITY=                  25. / [%] Ambient relative humidity (outside)
PRESSURE=                 780. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE= 'NaN     '           / [arcsec] DIMM2 Seeing
MASS2   = 'NaN     '           /  MASS(2) FSEE
ASTIG1  =                 0.05 / 4MAPS correction 1
ASTIG2  =                 -0.1 / 4MAPS correction 2
OUTTEMP =                 14.7 / [deg C] Outside temperature
AIRMASS =                   1. / Airmass
GSKYVAR =                0.043 / RASICAM global sky standard deviation
GSKYHOT =                0.937 / RASICAM global sky fraction above threshold
LSKYVAR =                   0. / RASICAM local sky standard deviation
LSKYHOT =                   0. / RASICAM local sky fraction above threshold
LSKYPOW =                   0. / RASICAM local sky normalized power
MSURTEMP=               14.137 / [deg C] Mirror surface average temperature
MAIRTEMP=                 14.4 / [deg C] Mirror temperature above surface
UPTRTEMP=               14.526 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 13.2 / [deg C] Mirror top surface temperature
UTN-TEMP=                14.48 / [deg C] Upper truss temperature north
UTS-TEMP=                14.29 / [deg C] Upper truss temperature south
UTW-TEMP=               15.215 / [deg C] Upper truss temperature west
UTE-TEMP=                14.12 / [deg C] Upper truss temperature east
PMN-TEMP=                 14.2 / [deg C] Mirror north edge temperature
PMS-TEMP=                 14.4 / [deg C] Mirror south edge temperature
PMW-TEMP=                 13.3 / [deg C] Mirror west edge temperature
PME-TEMP=                14.65 / [deg C] Mirror east edge temperature
DOMELOW =                14.78 / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                  9.4 / [deg C] Dome floor temperature
G-MEANX =               0.0697 / [arcsec] Guider x-axis mean offset
G-MEANY =              -0.0226 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[0.75,0.58,-8.95,-0.02,0.07,0.08,0.37,0.05,-0.44,]' / Mean Wavefront
DONUTFS3= '[1.53,1.14,8.29,0.18,-0.11,0.12,-0.04,0.07,-0.38,]' / Mean Wavefront
DONUTFS2= '[0.32,0.42,-8.95,0.29,0.10,-0.15,0.31,0.40,-0.15,]' / Mean Wavefront
DONUTFS1= '[1.23,1.87,8.53,0.31,0.23,0.23,-0.09,0.32,0.10,]' / Mean Wavefront fo
G-FLXVAR=         95942231.775 / [arcsec] Guider mean guide star flux variances
G-MEANXY=             0.000989 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[0.06,1.08,-9.04,-0.22,-0.09,-0.21,-0.28,0.22,-0.29,]' / Mean Wavefro
DONUTFN2= '[1.83,2.03,8.82,-0.19,0.12,0.01,-0.04,0.33,-0.13,]' / Mean Wavefront
DONUTFN3= '[0.77,0.90,-8.95,0.07,0.05,-0.05,-0.33,0.09,0.19,]' / Mean Wavefront
DONUTFN4= '[1.83,1.49,8.53,0.13,-0.11,-0.02,0.00,-0.04,0.14,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2020-10-11T00:20:10.125567'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    4 / Number of guide CCDs that remained active
DOXT    =                -0.03 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.1802 / [arcsec] Guider x-axis maximum offset
FADZ    =                36.88 / [um] FA Delta focus.
FADY    =               669.88 / [um] FA Delta Y.
FADX    =              -156.99 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                11.43 / [arcsec] FA Delta Y-theta.
DODZ    =                36.88 / [um] Delta-Z from donut analysis
DODY    =                -1.04 / [um] Y-decenter from donut analysis
DODX    =                -1.19 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2020-10-11T00:18:58' / Time of last RASICAM exposure (UTC)
G-SEEING=                2.093 / [arcsec] Guider average seeing
G-TRANSP=                0.967 / Guider average sky transparency
G-MEANY2=             0.003337 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.07 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.301 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                13.37 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.0632 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.005124 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:86'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTPROPID= '2013A-9999'
DTCALDAT= '2020-10-10'
DTSITE  = 'ct      '
DTTELESC= 'ct4m    '
DTACQNAM= '/data_local/images/DTS/2013A-9999/DECam_00944542.fits.fz'
DTINSTRU= 'decam   '
ODATEOBS= '2020-10-11T00:19:17.131350'
DTNSANAM= 'c4d_201011_001917_ori.fits.fz'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.73718035295033
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     41773.5868371335
GAINB   =      3.6834192395016
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     38525.2673597918
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =   1.932236029844E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -6.655370181518E-03 / Projection distortion parameter
PV2_9   =   8.643641600054E-03 / Projection distortion parameter
CD1_1   =  -1.7215388102323E-5 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -8.558545586958E-03 / Projection distortion parameter
PV2_1   =   1.032433081204E+00 / Projection distortion parameter
PV2_2   =  -7.585279369799E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -3.699277982408E-02 / Projection distortion parameter
PV2_5   =   1.426765389030E-02 / Projection distortion parameter
PV2_6   =  -1.046953612904E-02 / Projection distortion parameter
PV2_7   =   1.400636377016E-02 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   6.853267257648E-03 / Projection distortion parameter
PV2_10  =  -1.935408576487E-03 / Projection distortion parameter
PV1_4   =   7.635260941264E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -6.373080723715E-03 / Projection distortion parameter
PV1_1   =   1.012882441531E+00 / Projection distortion parameter
PV1_0   =   2.767789005453E-03 / Projection distortion parameter
PV1_9   =   5.219928260352E-03 / Projection distortion parameter
PV1_8   =  -6.752955294821E-03 / Projection distortion parameter
CD1_2   =  0.00466238759942721 / Linear projection matrix
PV1_5   =  -1.524922747481E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046622237647642 / Linear projection matrix
CD2_2   =  -1.7092419694003E-5 / Linear projection matrix
PV1_10  =  -2.548734163024E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   3.099069814941E+02 / World coordinate on this axis
CRVAL2  =  -3.496811994266E+01 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Mon Oct 12 20:41:08 2020' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Mon Oct 12 20:41:08 2020' / overscan corrected
BIASFIL = 'DEC20A_AZ20201008_98d1ca4-59132Z_01.fits' / Bias correction file
BAND    = 'Y       '
NITE    = '20201010'
DESBIAS = 'Mon Oct 12 20:44:21 2020'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Mon Oct 12 20:44:22 2020'
DESBPM  = 'Mon Oct 12 20:44:22 2020' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Mon Oct 12 20:44:22 2020' / Flag saturated pixels
NSATPIX =                   81 / Number of saturated pixels
FLATMEDA=     3.73718035295033
FLATMEDB=      3.6834192395016
SATURATE=     41773.5868371335
DESGAINC= 'Mon Oct 12 20:44:22 2020'
FLATFIL = 'DEC20A_AZ20201008_98d1ca4-59131YF_01.fits' / Dome flat correction fil
DESFLAT = 'Mon Oct 12 20:44:22 2020'
STARFIL = 'STARFLAT_20201010_98d2bf0-YP_ci_01.fits' / Star flat correction file
DESSTAR = 'Mon Oct 12 20:44:22 2020'
HISTORY Mon Oct 12 20:44:23 2020 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Mon Oct 12 20:48:42 2020' / bleed trail masking
NBLEED  = 282  / Number of bleed trail pixels.
STARMASK= 'Mon Oct 12 20:48:42 2020' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC20B_AW20201010_98d4
HISTORY 98e-filY-ccd01Y/DEC20B_AW20201010_98d498e-Y-201011001917_01.fits omiblee
HISTORY d_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Oct 12 20:48 Bad pixel file is omibleed_01_bpm.pl'
FWHM    =             4.269145 / Median FWHM in pixels
ELLIPTIC=           0.05915314 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Mon Oct 12 21:04:09 2020' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
PHOTREF = 'Gaia    '           / Photometric ref. catalog
MAGZERO =               27.905 / [mag] Magnitude zero point
MAGZPT  =               24.212 / [mag] Magnitude zero point per sec
NPHTMTCH=                18634 / Number of phot. stars matched
RADIUS  =                 2.55 / [pix] Average radius of stars
RADSTD  =                0.123 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             629.4929
NCOMBINE=                 7800
PUPILSKY=     628.927278629955
PUPILSUM=     180379.505590988
PUPILMAX=     6.23971128463745
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'hPOljPLlhPLlhPLl'   / HDU checksum updated 2020-11-03T23:42:31
DATASUM = '2387835004'         / data unit checksum updated 2020-11-03T23:42:31
NPUPSCL =                73835
PUPSCL  =     0.34361297358888
   Ț     Ą  Ì                     Ì    	Ê      êÿÿÿ               ÿ       @A``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
 
@`
`
`
`
`
`
 
@`
 
@`
 
@`
 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
```````` @ @ @` @ @ @````````` @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
 
@`
`
`
`
`
`
 
@`
`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@L   ÿ      Ą A ÿ      AĄ  ÿ 
   
  @Á "@Ÿ  ÿ 
   
  @č 2@¶  ÿ 
   
  @Ž <@±  ÿ 
   
  @Ż F@Ź  ÿ 
   
  @« N@š  ÿ 
   
  @§ V@€  ÿ 
   
  @€ \@Ą  ÿ 
   
  @Ą b@  ÿ 
   
  @ h@  ÿ 
   
  @ n@  ÿ 
   
  @ t@  ÿ 
   
  @ x@  ÿ 
   
  @ ~@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @  @  ÿ 
   
  @ €@}  ÿ 
   
  @~ š@{  ÿ 
   
  @} Ș@z  ÿ 
   
  @{ ź@x  ÿ 
   
  @z °@w  ÿ 
   
  @x Ž@u  ÿ 
   
  @w ¶@t  ÿ 
   
  @u ș@r  ÿ 
   
  @t Œ@q  ÿ 
   
  @s ż@o  ÿ 
   
  @q Â@n  ÿ 
   
  @p Ä@m  ÿ 
   
  @o Æ@l  ÿ 
   
  @m Ê@j  ÿ 
   
  @l Ì@i  ÿ 
   
  @k Î@h  ÿ 
   
  @j Đ@g  ÿ 
   
  @i Ò@f  ÿ 
   
  @h Ô@e  ÿ 
   
  @f Ű@c  ÿ 
   
  @e Ú@b  ÿ 
   
  @d Ü@a  ÿ 
   
  @c Ț@`  ÿ 
   
  @b à@_  ÿ 
   
  @a â@^  ÿ 
   
  @` ä@]  ÿ 
   
  @_ æ@\  ÿ 
   
  @^ è@[  ÿ 
   
  @] ê@Z  ÿ 
   
  @\ ì@Y  ÿ 
   
  @[ î@X  ÿ 
   
  @Z đ@W  ÿ 
   
  @Y ò@V  ÿ 
   
  @X ô@U  ÿ 
   
  @W ö@T  ÿ 
   
  @V ű@S  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S ț@P  ÿ 
   
  @S ÿ@O  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@L  ÿ 
   
  @N@K  ÿ 
   
  @M
@J  ÿ 
   
  @L@I  ÿ 
   
  @K@H  ÿ 
   
  @J@G  ÿ 
   
  @I@F  ÿ 
   
  @H@E  ÿ 
   
  @G@D  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@A  ÿ 
   
  @C@@  ÿ 
   
  @B @?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?&@<  ÿ 
   
  @>(@;  ÿ 
   
  @=*@:  ÿ 
   
  @<,@9  ÿ 
   
  @;.@8  ÿ 
   
  @:0@7  ÿ 
   
  @92@6  ÿ 
   
  @84@5  ÿ 
   
  @76@4  ÿ 
   
  @68@3  ÿ 
   
  @5:@2  ÿ 
   
  @4<@1  ÿ      @4 @ @1  ÿ      @4 @ @1  ÿ      @3 @ @0  ÿ      @3 @ @0  ÿ      @3 @ @0  ÿ      @3 @ @0  ÿ      @3 @  @0  ÿ      @3 @! @0  ÿ      @3 @" @0  ÿ      @3 @$ @0  ÿ      @3 @& @0  ÿ      @3 @( @0  ÿ      @3 @) @/  ÿ      @3 @( @0  ÿ      @3 @& @0  ÿ      @3 @$ @0  ÿ      @3 @" @0  ÿ      @3 @! @0  ÿ      @3 @  @0  ÿ      @3 @ @0  ÿ      @3 @ @0  ÿ      @3 @ @0  ÿ      @3 @ @0  ÿ      @4 @ @1  ÿ      @4 @ @1  ÿ 
   
  @4<@1  ÿ 
   
  @5:@2  ÿ 
   
  @68@3  ÿ 
   
  @76@4  ÿ 
   
  @84@5  ÿ 
   
  @92@6  ÿ 
   
  @:0@7  ÿ 
   
  @;.@8  ÿ 
   
  @<,@9  ÿ 
   
  @=*@:  ÿ 
   
  @>(@;  ÿ 
   
  @?&@<  ÿ 
   
  @@$@=  ÿ 
   
  @A"@>  ÿ 
   
  @B @?  ÿ 
   
  @C@@  ÿ 
   
  @D@A  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @G@D  ÿ 
   
  @H@E  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@H  ÿ 
   
  @L@I  ÿ 
   
  @M
@J  ÿ 
   
  @N@K  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S ÿ@O  ÿ 
   
  @S ț@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ű@S  ÿ 
   
  @W ö@T  ÿ 
   
  @X ô@U  ÿ 
   
  @Y ò@V  ÿ 
   
  @Z đ@W  ÿ 
   
  @[ î@X  ÿ 
   
  @\ ì@Y  ÿ 
   
  @] ê@Z  ÿ 
   
  @^ è@[  ÿ 
   
  @_ æ@\  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @b à@_  ÿ 
   
  @c Ț@`  ÿ 
   
  @d Ü@a  ÿ 
   
  @e Ú@b  ÿ 
   
  @f Ű@c  ÿ 
   
  @h Ô@e  ÿ 
   
  @i Ò@f  ÿ 
   
  @j Đ@g  ÿ 
   
  @k Î@h  ÿ 
   
  @l Ì@i  ÿ 
   
  @m Ê@j  ÿ 
   
  @o Æ@l  ÿ 
   
  @p Ä@m  ÿ 
   
  @q Â@n  ÿ 
   
  @s ż@o  ÿ 
   
  @t Œ@q  ÿ 
   
  @u ș@r  ÿ 
   
  @w ¶@t  ÿ 
   
  @x Ž@u  ÿ 
   
  @z °@w  ÿ 
   
  @{ ź@x  ÿ 
   
  @} Ș@z  ÿ 
   
  @~ š@{  ÿ 
   
  @ €@}  ÿ 
   
  @  @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ~@  ÿ 
   
  @ x@  ÿ 
   
  @ t@  ÿ 
   
  @ n@  ÿ 
   
  @ h@  ÿ 
   
  @Ą b@  ÿ 
   
  @€ \@Ą  ÿ 
   
  @§ V@€  ÿ 
   
  @« N@š  ÿ 
   
  @Ż F@Ź  ÿ 
   
  @Ž <@±  ÿ 
   
  @č 2@¶  ÿ 
   
  @Á "@Ÿ L ÿ      AĄ