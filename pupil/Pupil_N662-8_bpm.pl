  ŠV  @û  §$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1288783829
$MTIME = 1288783829
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-11-02T18:30:29' / Date FITS file was generated
IRAF-TLM= '2020-11-02T18:30:32' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                 360. / [s] Requested exposure duration
EXPTIME =                 360. / [s] Exposure duration
EXPDUR  =                 360. / [s] Exposure duration
DARKTIME=            361.21889 / [s] Dark time
OBSID   = 'ct4m20201022t235948' / Unique Observation ID
DATE-OBS= '2020-10-22T23:59:48.289546' / DateTime of observation start (UTC)
TIME-OBS= '23:59:48.289546'    / Time of observation start (UTC)
MJD-OBS =       59144.99986446 / MJD of observation start
MJD-END =      59145.004031127 / Exposure end
OPENSHUT= '2020-10-22T23:59:48.446890' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               947709 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'WILDTHING: Wide-area Investigation of Lyalpha emission Dissipating &'
CONTINUE= '        '           /   '&' / Current observing orogram
OBSERVER= 'Guaita  '           / Observer name(s)
PROPOSER= 'Guaita  '           / Proposal Principle Investigator
DTPI    = 'Guaita  '           / Proposal Principle Investigator (iSTB)
PROPID  = '2020B-0911'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'N662 DECam c0009 6620.0 172.0' / Unique filter identifier
INSTANCE= 'DECam_20201022'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '22:37:12.000'       / [HH:MM:SS] Target RA
DEC     = '00:51:00.000'       / [DD:MM:SS] Target DEC
TELRA   = '22:37:11.968'       / [HH:MM:SS] Telescope RA
TELDEC  = '00:51:00.500'       / [DD:MM:SS] Telescope DEC
HA      = '-01:14:26.190'      / [HH:MM:SS] Telescope hour angle
ZD      =                35.85 / [deg] Telescope zenith distance
AZ      =              33.1517 / [deg] Telescope azimuth angle
DOMEAZ  =               32.885 / [deg] Dome azimuth angle
ZPDELRA =            -121.5822 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                  4.1 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-1908.91,-2177.48,2558.61,23.42,-167.91, 0.00' / DECam hexapod settin
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    F / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =               17.864 / [m/s] Wind speed
WINDDIR =                  45. / [deg] Wind direction (from North)
HUMIDITY=                  29. / [%] Ambient relative humidity (outside)
PRESSURE=                 779. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                0.826 / [arcsec] DIMM2 Seeing
MASS2   =                 0.35 /  MASS(2) FSEE
ASTIG1  =                -0.17 / 4MAPS correction 1
ASTIG2  =                 0.07 / 4MAPS correction 2
OUTTEMP =                 15.6 / [deg C] Outside temperature
AIRMASS =                 1.23 / Airmass
GSKYVAR =                0.041 / RASICAM global sky standard deviation
GSKYHOT =                0.919 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               13.975 / [deg C] Mirror surface average temperature
MAIRTEMP=                 14.5 / [deg C] Mirror temperature above surface
UPTRTEMP=               14.916 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 13.5 / [deg C] Mirror top surface temperature
UTN-TEMP=               14.855 / [deg C] Upper truss temperature north
UTS-TEMP=               14.725 / [deg C] Upper truss temperature south
UTW-TEMP=               15.265 / [deg C] Upper truss temperature west
UTE-TEMP=                14.82 / [deg C] Upper truss temperature east
PMN-TEMP=                 13.6 / [deg C] Mirror north edge temperature
PMS-TEMP=                 14.5 / [deg C] Mirror south edge temperature
PMW-TEMP=                 13.5 / [deg C] Mirror west edge temperature
PME-TEMP=                 14.3 / [deg C] Mirror east edge temperature
DOMELOW =                16.01 / [deg C] Low dome temperature
DOMEHIGH=                  13. / [deg C] High dome temperature
DOMEFLOR=                11.15 / [deg C] Dome floor temperature
G-MEANX =               0.0126 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0283 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.60,1.40,-8.05,0.13,-0.05,-0.16,0.18,-0.04,-0.36,]' / Mean Wavefron
DONUTFS3= '[-0.17,1.97,9.42,0.24,-0.09,-0.09,0.07,0.00,-0.37,]' / Mean Wavefront
DONUTFS2= '[0.40,0.05,-8.00,0.28,0.10,-0.16,0.25,0.27,-0.15,]' / Mean Wavefront
DONUTFS1= '[0.43,1.59,9.46,0.31,0.13,0.03,0.11,0.20,0.17,]' / Mean Wavefront for
G-FLXVAR=          1496593.394 / [arcsec] Guider mean guide star flux variances
G-MEANXY=            -0.000531 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[0.65,0.15,-8.01,-0.22,-0.53,-0.18,0.02,0.15,-0.21,]' / Mean Wavefron
DONUTFN2= '[2.29,1.73,10.38,0.31,-0.39,-0.08,-0.01,0.53,-0.15,]' / Mean Wavefron
DONUTFN3= '[1.93,2.02,-7.99,0.20,-0.81,-0.15,-0.02,-0.02,0.34,]' / Mean Wavefron
DONUTFN4= '[]      '           / Mean Wavefront for Sensor FN4
HIERARCH TIME_RECORDED = '2020-10-23T00:06:10.178321'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    4 / Number of guide CCDs that remained active
DOXT    =                 0.24 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.2596 / [arcsec] Guider x-axis maximum offset
FADZ    =              -145.38 / [um] FA Delta focus.
FADY    =               869.85 / [um] FA Delta Y.
FADX    =               526.31 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =               -35.54 / [arcsec] FA Delta Y-theta.
DODZ    =              -145.38 / [um] Delta-Z from donut analysis
DODY    =                -1.02 / [um] Y-decenter from donut analysis
DODX    =                -1.27 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2020-10-22T23:59:43' / Time of last RASICAM exposure (UTC)
G-SEEING=                1.772 / [arcsec] Guider average seeing
G-TRANSP=                0.589 / Guider average sky transparency
G-MEANY2=               0.0129 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.18 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.196 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =               -60.78 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.3166 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.006928 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:86'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTPROPID= '2020B-0911'
DTCALDAT= '2020-10-22'
DTSITE  = 'ct      '
DTTELESC= 'ct4m    '
DTACQNAM= '/data_local/images/DTS/2020B-0911/DECam_00947709.fits.fz'
DTINSTRU= 'decam   '
ODATEOBS= '2020-10-22T23:59:48.289546'
DTNSANAM= 'c4d_201022_235948_ori.fits.fz'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.50873705288982
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=     44493.3392405174
GAINB   =     3.45929783753557
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     41021.2469884044
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =  -5.024944166180E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -7.518309987306E-03 / Projection distortion parameter
PV2_9   =   7.645285959324E-03 / Projection distortion parameter
CD1_1   =  -1.1916174631418E-5 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -1.104079077126E-02 / Projection distortion parameter
PV2_1   =   1.041064622071E+00 / Projection distortion parameter
PV2_2   =  -7.605057551051E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -4.706984248726E-02 / Projection distortion parameter
PV2_5   =   1.490790717727E-02 / Projection distortion parameter
PV2_6   =  -1.030039529491E-02 / Projection distortion parameter
PV2_7   =   1.792576002572E-02 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   4.342211163794E-03 / Projection distortion parameter
PV2_10  =  -2.834364628169E-03 / Projection distortion parameter
PV1_4   =   7.393554740353E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -4.868666988397E-03 / Projection distortion parameter
PV1_1   =   1.014904653685E+00 / Projection distortion parameter
PV1_0   =   2.469765949961E-03 / Projection distortion parameter
PV1_9   =   8.010888416329E-03 / Projection distortion parameter
PV1_8   =  -7.273053780327E-03 / Projection distortion parameter
CD1_2   =  0.00466236092206017 / Linear projection matrix
PV1_5   =  -2.019691919136E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046625261402445 / Linear projection matrix
CD2_2   =  -1.1277919662131E-5 / Linear projection matrix
PV1_10  =  -1.341296365071E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   3.393008975211E+02 / World coordinate on this axis
CRVAL2  =   8.508410217883E-01 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Sat Oct 24 11:32:58 2020' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Sat Oct 24 11:32:58 2020' / overscan corrected
BIASFIL = 'DEC20B_DT20201021_98e983c-59143Z_01.fits' / Bias correction file
BAND    = 'N662    '
NITE    = '20201022'
DESBIAS = 'Sat Oct 24 11:42:06 2020'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Sat Oct 24 11:42:07 2020'
DESBPM  = 'Sat Oct 24 11:42:07 2020' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Sat Oct 24 11:42:07 2020' / Flag saturated pixels
NSATPIX =                 1871 / Number of saturated pixels
FLATMEDA=     3.50873705288982
FLATMEDB=     3.45929783753557
SATURATE=     44493.3392405174
DESGAINC= 'Sat Oct 24 11:42:07 2020'
FLATFIL = 'DEC20B_DT20201021_98e983c-59143N662F_01.fits' / Dome flat correction
DESFLAT = 'Sat Oct 24 11:42:07 2020'
STARFIL = 'STARFLAT_20201010_98d2bf0-N662P_ci_01.fits' / Star flat correction fi
DESSTAR = 'Sat Oct 24 11:42:08 2020'
HISTORY Sat Oct 24 11:42:08 2020 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Sat Oct 24 11:51:50 2020' / bleed trail masking
NBLEED  = 4186  / Number of bleed trail pixels.
STARMASK= 'Sat Oct 24 11:51:50 2020' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC20B_LG20201022_98ed
HISTORY 7fa-filN662-ccd01N662/DEC20B_LG20201022_98ed7fa-N662-201022235948_01.fit
HISTORY s omibleed_01.fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Oct 24 11:51 Bad pixel file is omibleed_01_bpm.pl'
FWHM    =             4.425412 / Median FWHM in pixels
ELLIPTIC=           0.09441072 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Sat Oct 24 12:20:43 2020' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
ASTRMREF= 'Gaia-2  '           / Astrometric ref. catalog
PHOTREF = 'PS1     '           / Photometric ref. catalog
MAGZERO =               29.565 / [mag] Magnitude zero point
MAGZPT  =               23.174 / [mag] Magnitude zero point per sec
NPHTMTCH=                 9217 / Number of phot. stars matched
RADIUS  =                 2.36 / [pix] Average radius of stars
RADSTD  =                0.061 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             508.0149
NCOMBINE=                 4980
PUPILSKY=     503.420141120466
PUPILSUM=     679376.700225669
PUPILMAX=     13.8429594039917
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'R2hVT2eUR2eUR2eU'   / HDU checksum updated 2020-11-02T17:34:30
DATASUM = '2535904121'         / data unit checksum updated 2020-11-02T17:34:30
NPUPSCL =                88684
PUPSCL  =     0.12886529852097
   Ț     Ą  Ì                     Ì  c  §      êÿÿÿ               ÿc       @0``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
 
@`
`
`
 
@`
 
@`
 
@`
 
@`
 
@ 
@`
`
``````````````````````````` @` @` @` @ @ @` @ @` @`````````````````````````````` 
@ 
@`
 
@`
 
@`
 
@`
 
@`
`
 
@`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@9   ÿ      Ą 0 ÿ      AĄ  ÿ 
   
  @Ć @Ă  ÿ 
   
  @» -@č  ÿ 
   
  @Ž ;@Č  ÿ 
   
  @Ż E@­  ÿ 
   
  @Ș O@š  ÿ 
   
  @Š W@€  ÿ 
   
  @ą _@   ÿ 
   
  @ f@  ÿ 
   
  @ l@  ÿ 
   
  @ r@  ÿ 
   
  @ x@  ÿ 
   
  @ }@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ Ą@  ÿ 
   
  @ „@}  ÿ 
   
  @} ©@{  ÿ 
   
  @{ ­@y  ÿ 
   
  @z °@w  ÿ 
   
  @x Ž@u  ÿ 
   
  @v ·@t  ÿ 
   
  @u ș@r  ÿ 
   
  @s Ÿ@p  ÿ 
   
  @q Á@o  ÿ 
   
  @p Ä@m  ÿ 
   
  @n Ç@l  ÿ 
   
  @m Ê@j  ÿ 
   
  @k Í@i  ÿ 
   
  @j Ï@h  ÿ 
   
  @i Ò@f  ÿ 
   
  @g Ő@e  ÿ 
   
  @f Ś@d  ÿ 
   
  @e Ú@b  ÿ 
   
  @c Ę@a  ÿ 
   
  @b ß@`  ÿ 
   
  @a â@^  ÿ 
   
  @` ä@]  ÿ 
   
  @_ æ@\  ÿ 
   
  @] é@[  ÿ 
   
  @\ ë@Z  ÿ 
   
  @[ í@Y  ÿ 
   
  @Z ï@X  ÿ 
   
  @Y ò@V  ÿ 
   
  @X ô@U  ÿ 
   
  @W ö@T  ÿ 
   
  @V ű@S  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S ț@P  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@M  ÿ 
   
  @N@L  ÿ 
   
  @M	@K  ÿ 
   
  @L@J  ÿ 
   
  @K@I  ÿ 
   
  @K@H  ÿ 
   
  @J@G  ÿ 
   
  @I@F  ÿ 
   
  @H@F  ÿ 
   
  @G@E  ÿ 
   
  @F@D  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@B  ÿ 
   
  @C@A  ÿ 
   
  @C@@  ÿ 
   
  @B @?  ÿ 
   
  @A!@?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?%@=  ÿ 
   
  @>'@<  ÿ 
   
  @>(@;  ÿ 
   
  @=)@;  ÿ 
   
  @<+@:  ÿ 
   
  @<,@9  ÿ 
   
  @;-@9  ÿ 
   
  @;.@8  ÿ 
   
  @:/@8  ÿ 
   
  @91@7  ÿ 
   
  @92@6  ÿ 
   
  @83@6  ÿ 
   
  @84@5  ÿ 
   
  @75@5  ÿ 
   
  @76@4  ÿ 
   
  @68@3  ÿ 
   
  @59@3  ÿ 
   
  @5:@2  ÿ 
   
  @4;@2  ÿ 
   
  @4<@1  ÿ 
   
  @3=@1  ÿ 
   
  @3>@0  ÿ 
   
  @2?@0  ÿ 
   
  @2@@/  ÿ 
   
  @1A@/  ÿ 
   
  @1B@.  ÿ 
   
  @0C@.  ÿ 
   
  @0D@-  ÿ 
   
  @/E@-  ÿ 
   
  @/F@,  ÿ 
   
  @.G@,  ÿ 
   
  @.H@+  ÿ 
   
  @-I@+  ÿ 
   
  @-J@*  ÿ 
   
  @,K@*  ÿ 
   
  @,L@)  ÿ 
   
  @+M@)  ÿ 
   
  @+N@(  ÿ 
   
  @*O@(  ÿ 
   
  @*P@'  ÿ 
   
  @)Q@'  ÿ 
   
  @)R@&  ÿ 
   
  @(S@&  ÿ 
   
  @(T@%  ÿ 
   
  @'U@%  ÿ 
   
  @'V@$  ÿ 
   
  @&W@$  ÿ      @& €@ €@$  ÿ      @&  @  @#  ÿ      @& @ @#  ÿ      @% @# @#  ÿ      @% @( @#  ÿ      @% @, @"  ÿ      @% @/ @"  ÿ      @$ @2 @"  ÿ      @$ @5 @"  ÿ      @$ @8 @!  ÿ      @$ @: @!  ÿ      @$ @= @!  ÿ      @# @? @!  ÿ      @# @A @!  ÿ      @# @C @!  ÿ      @# @E @   ÿ      @# @F @   ÿ      @" @H @   ÿ      @" @J @   ÿ      @" @K @   ÿ      @" @L @   ÿ      @" @N @  ÿ      @" @O @  ÿ      @" @P @  ÿ      @" @Q @  ÿ      @! @R @  ÿ      @! @S @  ÿ      @! @T @  ÿ      @! @U @  ÿ      @! @V @  ÿ      @! @W @  ÿ      @! @X @  ÿ      @! @Y @  ÿ      @  @Y @  ÿ      @  @Z @  ÿ      @  @[ @  ÿ      @  @Z @  ÿ      @  @Y @  ÿ      @! @Y @  ÿ      @! @X @  ÿ      @! @W @  ÿ      @! @V @  ÿ      @! @U @  ÿ      @! @T @  ÿ      @! @S @  ÿ      @! @R @  ÿ      @! @Q @  ÿ      @" @P @  ÿ      @" @O @  ÿ      @" @N @  ÿ      @" @L @   ÿ      @" @K @   ÿ      @" @J @   ÿ      @" @H @   ÿ      @# @G @   ÿ      @# @E @   ÿ      @# @C @!  ÿ      @# @A @!  ÿ      @# @? @!  ÿ      @$ @= @!  ÿ      @$ @; @!  ÿ      @$ @8 @!  ÿ      @$ @6 @"  ÿ      @$ @3 @"  ÿ      @% @0 @"  ÿ      @% @, @"  ÿ      @% @( @#  ÿ      @% @$ @#  ÿ      @& @ @#  ÿ      @& @  @#  ÿ      @& €@ Ł@$  ÿ 
   
  @&W@$  ÿ 
   
  @'V@$  ÿ 
   
  @'U@%  ÿ 
   
  @(T@%  ÿ 
   
  @(S@&  ÿ 
   
  @)R@&  ÿ 
   
  @)Q@'  ÿ 
   
  @*P@'  ÿ 
   
  @*O@(  ÿ 
   
  @+N@(  ÿ 
   
  @+M@)  ÿ 
   
  @,L@)  ÿ 
   
  @,K@*  ÿ 
   
  @-J@*  ÿ 
   
  @-I@+  ÿ 
   
  @.H@+  ÿ 
   
  @.G@,  ÿ 
   
  @/F@,  ÿ 
   
  @/E@-  ÿ 
   
  @0D@-  ÿ 
   
  @0C@.  ÿ 
   
  @1B@.  ÿ 
   
  @1A@/  ÿ 
   
  @2@@/  ÿ 
   
  @2?@0  ÿ 
   
  @3>@0  ÿ 
   
  @3=@1  ÿ 
   
  @4<@1  ÿ 
   
  @4;@2  ÿ 
   
  @5:@2  ÿ 
   
  @59@3  ÿ 
   
  @68@3  ÿ 
   
  @67@4  ÿ 
   
  @75@5  ÿ 
   
  @84@5  ÿ 
   
  @83@6  ÿ 
   
  @92@6  ÿ 
   
  @91@7  ÿ 
   
  @:0@7  ÿ 
   
  @;.@8  ÿ 
   
  @;-@9  ÿ 
   
  @<,@9  ÿ 
   
  @<+@:  ÿ 
   
  @=)@;  ÿ 
   
  @>(@;  ÿ 
   
  @>'@<  ÿ 
   
  @?%@=  ÿ 
   
  @@$@=  ÿ 
   
  @@#@>  ÿ 
   
  @A!@?  ÿ 
   
  @B @?  ÿ 
   
  @C@@  ÿ 
   
  @C@A  ÿ 
   
  @D@B  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @F@D  ÿ 
   
  @G@E  ÿ 
   
  @H@F  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@H  ÿ 
   
  @K@I  ÿ 
   
  @L@J  ÿ 
   
  @M	@K  ÿ 
   
  @N@L  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S ț@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ű@S  ÿ 
   
  @W ö@T  ÿ 
   
  @X ô@U  ÿ 
   
  @Y ò@V  ÿ 
   
  @Z đ@W  ÿ 
   
  @[ í@Y  ÿ 
   
  @\ ë@Z  ÿ 
   
  @] é@[  ÿ 
   
  @^ ç@\  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @b ß@`  ÿ 
   
  @c Ę@a  ÿ 
   
  @e Ú@b  ÿ 
   
  @f Ű@c  ÿ 
   
  @g Ő@e  ÿ 
   
  @i Ò@f  ÿ 
   
  @j Đ@g  ÿ 
   
  @k Í@i  ÿ 
   
  @m Ê@j  ÿ 
   
  @n Ç@l  ÿ 
   
  @p Ä@m  ÿ 
   
  @q Á@o  ÿ 
   
  @s Ÿ@p  ÿ 
   
  @t »@r  ÿ 
   
  @v ·@t  ÿ 
   
  @x Ž@u  ÿ 
   
  @y ±@w  ÿ 
   
  @{ ­@y  ÿ 
   
  @} ©@{  ÿ 
   
  @ Š@|  ÿ 
   
  @ ą@~  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ~@  ÿ 
   
  @ x@  ÿ 
   
  @ s@  ÿ 
   
  @ m@  ÿ 
   
  @ f@  ÿ 
   
  @ą _@   ÿ 
   
  @Š X@Ł  ÿ 
   
  @Ș P@§  ÿ 
   
  @Ż F@Ź  ÿ 
   
  @Ž <@±  ÿ 
   
  @ș /@ž  ÿ 
   
  @Ä @Â 9 ÿ      AĄ