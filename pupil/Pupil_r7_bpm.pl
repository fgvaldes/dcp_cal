  �V  >�  
�$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1279297594
$MTIME = 1279297594
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-07-15T23:25:12' / Date FITS file was generated
IRAF-TLM= '2020-07-15T23:24:56' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  86. / [s] Requested exposure duration
EXPTIME =                  86. / [s] Exposure duration
EXPDUR  =                  86. / [s] Exposure duration
DARKTIME=             86.96891 / [s] Dark time
OBSID   = 'ct4m20180808t050503' / Unique Observation ID
DATE-OBS= '2018-08-08T05:05:03.807680' / DateTime of observation start (UTC)
TIME-OBS= '05:05:03.807680'    / Time of observation start (UTC)
MJD-OBS =       58338.21184963 / MJD of observation start
MJD-END =         58338.212845 / Exposure end
OPENSHUT= '2018-08-08T05:05:03.694510' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               762881 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'The DECam Legacy Survey of the SDSS Equatorial Sky' / Current observi
OBSERVER= 'S. BenZvi, R. Demina' / Observer name(s)
PROPOSER= 'Schlegel'           / Proposal Principle Investigator
DTPI    = 'Schlegel'           / Proposal Principle Investigator (iSTB)
PROPID  = '2014B-0404'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'r DECam SDSS c0002 6415.0 1480.0' / Unique filter identifier
FILTPOS = 'cassette_7'         / Filter position in FCM
INSTANCE= 'DECam_20180807'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '00:02:20.641'       / [HH:MM:SS] Target RA
DEC     = '-06:49:15.600'      / [DD:MM:SS] Target DEC
TELRA   = '00:02:20.288'       / [HH:MM:SS] Telescope RA
TELDEC  = '-06:49:15.899'      / [DD:MM:SS] Telescope DEC
HA      = '-02:34:46.970'      / [HH:MM:SS] Telescope hour angle
ZD      =                43.25 / [deg] Telescope zenith distance
AZ      =              65.1505 / [deg] Telescope azimuth angle
DOMEAZ  =               66.615 / [deg] Dome azimuth angle
ZPDELRA =            -106.8311 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                 58.4 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-2486.39,-1348.02,2723.27, 6.16,-231.38,-0.00' / DECam hexapod settin
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =               11.748 / [m/s] Wind speed
WINDDIR =                  19. / [deg] Wind direction (from North)
HUMIDITY=                  20. / [%] Ambient relative humidity (outside)
PRESSURE=                 780. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                1.734 / [arcsec] DIMM2 Seeing
MASS2   =                 1.74 /  MASS(2) FSEE
ASTIG1  =                -0.02 / 4MAPS correction 1
ASTIG2  =                 0.06 / 4MAPS correction 2
OUTTEMP =                  12. / [deg C] Outside temperature
AIRMASS =                 1.37 / Airmass
GSKYVAR =                0.011 / RASICAM global sky standard deviation
GSKYHOT =                   0. / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=                  10. / [deg C] Mirror surface average temperature
MAIRTEMP=                 10.8 / [deg C] Mirror temperature above surface
UPTRTEMP=               11.509 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                  9.2 / [deg C] Mirror top surface temperature
UTN-TEMP=               11.865 / [deg C] Upper truss temperature north
UTS-TEMP=               11.505 / [deg C] Upper truss temperature south
UTW-TEMP=               11.275 / [deg C] Upper truss temperature west
UTE-TEMP=                11.39 / [deg C] Upper truss temperature east
PMN-TEMP=                  9.6 / [deg C] Mirror north edge temperature
PMS-TEMP=                 10.8 / [deg C] Mirror south edge temperature
PMW-TEMP=                  9.4 / [deg C] Mirror west edge temperature
PME-TEMP=                 10.2 / [deg C] Mirror east edge temperature
DOMELOW =               12.975 / [deg C] Low dome temperature
DOMEHIGH=                  12. / [deg C] High dome temperature
DOMEFLOR=                  8.4 / [deg C] Dome floor temperature
G-MEANX =               0.0196 / [arcsec] Guider x-axis mean offset
G-MEANY =              -0.0294 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.47,1.30,-8.83,-0.00,-0.08,0.01,-0.08,-0.02,-0.39,]' / Mean Wavefro
DONUTFS3= '[0.59,1.91,8.69,0.02,-0.14,0.00,-0.06,0.02,-0.29,]' / Mean Wavefront
DONUTFS2= '[1.98,1.08,-8.76,0.29,0.04,0.03,-0.05,0.30,-0.09,]' / Mean Wavefront
DONUTFS1= '[1.69,1.30,8.75,0.29,0.08,0.05,-0.08,0.19,0.11,]' / Mean Wavefront fo
G-FLXVAR=          1988856.687 / [arcsec] Guider mean guide star flux variances
G-MEANXY=            -0.008573 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[]      '           / Mean Wavefront for Sensor FN1
DONUTFN2= '[2.06,0.88,8.67,-0.17,-0.05,-0.03,-0.05,0.11,-0.03,]' / Mean Wavefron
DONUTFN3= '[1.13,0.80,-8.92,0.04,-0.09,0.00,-0.10,-0.03,0.21,]' / Mean Wavefront
DONUTFN4= '[1.78,0.98,8.64,0.16,-0.02,-0.06,-0.04,-0.14,0.21,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2018-08-08T05:06:51.996258'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    3 / Number of guide CCDs that remained active
DOXT    =                 0.04 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.7116 / [arcsec] Guider x-axis maximum offset
FADZ    =                11.26 / [um] FA Delta focus.
FADY    =              -159.01 / [um] FA Delta Y.
FADX    =                11.49 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                  6.2 / [arcsec] FA Delta Y-theta.
DODZ    =                11.26 / [um] Delta-Z from donut analysis
DODY    =                -1.53 / [um] Y-decenter from donut analysis
DODX    =                -1.18 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2018-08-08T05:04:52' / Time of last RASICAM exposure (UTC)
G-SEEING=                2.857 / [arcsec] Guider average seeing
G-TRANSP=                0.618 / Guider average sky transparency
G-MEANY2=             0.042723 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.09 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.299 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                 8.37 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.4434 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.043553 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:66'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTCALDAT= '2018-08-07'
DTNSANAM= 'c4d_180808_050503_ori.fits.fz'
ODATEOBS= '2018-08-08T05:05:03.807680'
DTACQNAM= '/data_local/images/DTS/2014B-0404/DECam_00762881.fits.fz'
DTTELESC= 'ct4m    '
DTPROPID= '2014B-0404'
DTSITE  = 'ct      '
DTINSTRU= 'decam   '
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     3.54390133758183
RDNOISEA=              6.13928 / [electrons] Read noise for amp A
SATURATA=      44051.855040221
GAINB   =     3.50451453086672
RDNOISEB=             5.981542 / [electrons] Read noise for amp B
SATURATB=     40491.9739239617
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =  -5.024944166180E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -7.518309987306E-03 / Projection distortion parameter
PV2_9   =   7.645285959324E-03 / Projection distortion parameter
CD1_1   =  -7.5979756635264E-6 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -1.103551878067E-02 / Projection distortion parameter
PV2_1   =   1.041072620936E+00 / Projection distortion parameter
PV2_2   =  -7.610739061812E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -4.707894081445E-02 / Projection distortion parameter
PV2_5   =   1.492610667561E-02 / Projection distortion parameter
PV2_6   =  -1.029847301094E-02 / Projection distortion parameter
PV2_7   =   1.792576002572E-02 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   4.351740570279E-03 / Projection distortion parameter
PV2_10  =  -2.834364628169E-03 / Projection distortion parameter
PV1_4   =   7.423515243876E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -4.848133285721E-03 / Projection distortion parameter
PV1_1   =   1.014901953466E+00 / Projection distortion parameter
PV1_0   =   2.461766338129E-03 / Projection distortion parameter
PV1_9   =   8.010888416329E-03 / Projection distortion parameter
PV1_8   =  -7.273053780327E-03 / Projection distortion parameter
CD1_2   =  0.00466369337356545 / Linear projection matrix
PV1_5   =  -2.019287925541E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046633243077267 / Linear projection matrix
CD2_2   =  -6.4776932283392E-6 / Linear projection matrix
PV1_10  =  -1.341296365071E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   5.857942487244E-01 / World coordinate on this axis
CRVAL2  =  -6.821573777539E+00 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Sun Jul 28 20:36:33 2019' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Sun Jul 28 20:36:33 2019' / overscan corrected
BIASFIL = 'DEC18B_20180807_924fb20-58337Z_01.fits' / Bias correction file
BAND    = 'r       '
NITE    = '20180807'
DESBIAS = 'Sun Jul 28 20:48:34 2019'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Sun Jul 28 20:48:35 2019'
DESBPM  = 'Sun Jul 28 20:48:36 2019' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Sun Jul 28 20:48:36 2019' / Flag saturated pixels
NSATPIX =                28568 / Number of saturated pixels
FLATMEDA=     3.54390133758183
FLATMEDB=     3.50451453086672
SATURATE=      44051.855040221
DESGAINC= 'Sun Jul 28 20:48:36 2019'
FLATFIL = 'DEC18B_20180807_924fb20-58337rF_01.fits' / Dome flat correction file
DESFLAT = 'Sun Jul 28 20:48:36 2019'
STARFIL = 'STARFLAT_20180711_94b5533-rP_ci_01.fits' / Star flat correction file
DESSTAR = 'Sun Jul 28 20:48:36 2019'
HISTORY Sun Jul 28 20:48:36 2019 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Sun Jul 28 20:56:51 2019' / bleed trail masking
NBLEED  = 60147  / Number of bleed trail pixels.
STARMASK= 'Sun Jul 28 20:56:51 2019' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DECALS_DR9_9530a56-fil
HISTORY r-ccd01r/DECALS_DR9_9530a56-r-180808050503_01.fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FWHM    =             7.277331 / Median FWHM in pixels
ELLIPTIC=            0.2941955 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Sun Jul 28 21:12:37 2019' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             280.3936
NCOMBINE=                59184
PUPILSKY=     270.238399218724
PUPILSUM=     167094.456503233
PUPILMAX=     3.14485192298889
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= 'Bg97CZ87Bf87BZ87'   / HDU checksum updated 2020-07-15T21:13:20
DATASUM = '3255360020'         / data unit checksum updated 2020-07-15T21:13:20
PUPSCL  =     0.61366258376494
NPUPSCL =               101191
   �     �  �                 	s  	s  �  :  
�      ����               ��:       @,``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
 
@`
`
 
@`
`
 
@`
 
@`
 
@`
 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@`
`````` @ @` @ @````````` @`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
 
@`
`
 
@`
`
 
@`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@7   ��      � , ��      A�  �� 
   
  @� %@�  �� 
   
  @� 4@�  �� 
   
  @� @@�  �� 
   
  @� J@�  �� 
   
  @� T@�  �� 
   
  @� \@�  �� 
   
  @� b@�  �� 
   
  @� j@�  �� 
   
  @� p@�  �� 
   
  @� v@�  �� 
   
  @� |@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@  �� 
   
  @� �@}  �� 
   
  @~ �@{  �� 
   
  @| �@y  �� 
   
  @z �@w  �� 
   
  @x �@u  �� 
   
  @w �@t  �� 
   
  @u �@r  �� 
   
  @s �@p  �� 
   
  @r �@o  �� 
   
  @p �@m  �� 
   
  @o �@l  �� 
   
  @m �@j  �� 
   
  @l �@i  �� 
   
  @j �@g  �� 
   
  @i �@f  �� 
   
  @h �@e  �� 
   
  @f �@c  �� 
   
  @e �@b  �� 
   
  @d �@a  �� 
   
  @b �@_  �� 
   
  @a �@^  �� 
   
  @` �@]  �� 
   
  @_ �@\  �� 
   
  @] �@Z  �� 
   
  @\ �@Y  �� 
   
  @[ �@X  �� 
   
  @Z �@W  �� 
   
  @Y �@V  �� 
   
  @X �@U  �� 
   
  @W �@T  �� 
   
  @V �@S  �� 
   
  @U �@R  �� 
   
  @T �@Q  �� 
   
  @S �@P  �� 
   
  @R @O  �� 
   
  @Q@N  �� 
   
  @P@M  �� 
   
  @O@L  �� 
   
  @N@K  �� 
   
  @M
@J  �� 
   
  @L@I  �� 
   
  @K@H  �� 
   
  @J@G  �� 
   
  @I@F  �� 
   
  @H@E  �� 
   
  @G@D  �� 
   
  @F@C  �� 
   
  @E@B  �� 
   
  @D@A  �� 
   
  @C@@  �� 
   
  @B @?  �� 
   
  @A"@>  �� 
   
  @@$@=  �� 
   
  @?&@<  �� 
   
  @>(@;  �� 
   
  @=*@:  �� 
   
  @<,@9  �� 
   
  @;.@8  �� 
   
  @:0@7  �� 
   
  @92@6  �� 
   
  @84@5  �� 
   
  @76@4  �� 
   
  @68@3  �� 
   
  @5:@2  �� 
   
  @4<@1  �� 
   
  @3>@0  �� 
   
  @2@@/  �� 
   
  @1B@.  �� 
   
  @0D@-  �� 
   
  @/F@,  �� 
   
  @.H@+  �� 
   
  @-J@*  �� 
   
  @,L@)  �� 
   
  @+N@(  �� 
   
  @*P@'  �� 
   
  @)R@&  �� 
   
  @(T@%  �� 
   
  @'V@$  �� 
   
  @&X@#  �� 
   
  @%Z@"  �� 
   
  @$\@!  �� 
   
  @#^@   �� 
   
  @"`@  �� 
   
  @!b@  �� 
   
  @ d@  �� 
   
  @f@  �� 
   
  @g@  �� 
   
  @h@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @ �@ �@  ��      @P� @PP �@  ��      @ �@ �@  �� 
   
  @h@  �� 
   
  @g@  �� 
   
  @f@  �� 
   
  @ d@  �� 
   
  @!b@  �� 
   
  @"`@  �� 
   
  @#^@   �� 
   
  @$\@!  �� 
   
  @%Z@"  �� 
   
  @&X@#  �� 
   
  @'V@$  �� 
   
  @(T@%  �� 
   
  @)R@&  �� 
   
  @*P@'  �� 
   
  @+N@(  �� 
   
  @,L@)  �� 
   
  @-J@*  �� 
   
  @.H@+  �� 
   
  @/F@,  �� 
   
  @0D@-  �� 
   
  @1B@.  �� 
   
  @2@@/  �� 
   
  @3>@0  �� 
   
  @4<@1  �� 
   
  @5:@2  �� 
   
  @68@3  �� 
   
  @76@4  �� 
   
  @84@5  �� 
   
  @92@6  �� 
   
  @:0@7  �� 
   
  @;.@8  �� 
   
  @<,@9  �� 
   
  @=*@:  �� 
   
  @>(@;  �� 
   
  @?&@<  �� 
   
  @@$@=  �� 
   
  @A"@>  �� 
   
  @B @?  �� 
   
  @C@@  �� 
   
  @D@A  �� 
   
  @E@B  �� 
   
  @F@C  �� 
   
  @G@D  �� 
   
  @H@E  �� 
   
  @I@F  �� 
   
  @J@G  �� 
   
  @K@H  �� 
   
  @L@I  �� 
   
  @M
@J  �� 
   
  @N@K  �� 
   
  @O@L  �� 
   
  @P@M  �� 
   
  @Q@N  �� 
   
  @R @O  �� 
   
  @S �@P  �� 
   
  @T �@Q  �� 
   
  @U �@R  �� 
   
  @V �@S  �� 
   
  @W �@T  �� 
   
  @X �@U  �� 
   
  @Y �@V  �� 
   
  @Z �@W  �� 
   
  @[ �@X  �� 
   
  @\ �@Y  �� 
   
  @] �@Z  �� 
   
  @_ �@\  �� 
   
  @` �@]  �� 
   
  @a �@^  �� 
   
  @b �@_  �� 
   
  @d �@a  �� 
   
  @e �@b  �� 
   
  @f �@c  �� 
   
  @h �@e  �� 
   
  @i �@f  �� 
   
  @j �@g  �� 
   
  @l �@i  �� 
   
  @m �@j  �� 
   
  @o �@l  �� 
   
  @p �@m  �� 
   
  @r �@o  �� 
   
  @s �@p  �� 
   
  @u �@r  �� 
   
  @w �@t  �� 
   
  @x �@u  �� 
   
  @z �@w  �� 
   
  @| �@y  �� 
   
  @~ �@{  �� 
   
  @� �@}  �� 
   
  @� �@  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� �@�  �� 
   
  @� |@�  �� 
   
  @� v@�  �� 
   
  @� p@�  �� 
   
  @� j@�  �� 
   
  @� b@�  �� 
   
  @� \@�  �� 
   
  @� T@�  �� 
   
  @� J@�  �� 
   
  @� @@�  �� 
   
  @� 4@�  �� 
   
  @� %@� 7 ��      A�