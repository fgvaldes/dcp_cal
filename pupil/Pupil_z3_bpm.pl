  ŠV  FÆ  	$TITLE = "Sky for omimask_01.fits[0]"
$CTIME = 1279210028
$MTIME = 1279210028
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
DATE    = '2020-07-14T22:50:37' / Date FITS file was generated
IRAF-TLM= '2020-07-14T22:50:22' / Time of last modification
OBJECT  = 'Sky for omimask_01.fits[0]' / Name of the object observed
PROCTYPE= 'RAW     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_01.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  90. / [s] Requested exposure duration
EXPTIME =                  90. / [s] Exposure duration
EXPDUR  =                  90. / [s] Exposure duration
DARKTIME=           91.0889499 / [s] Dark time
OBSID   = 'ct4m20131013t033121' / Unique Observation ID
DATE-OBS= '2013-10-13T03:31:21.793793' /  UTC epoch
TIME-OBS= '03:31:21.793793'    / Time of observation start (UTC)
MJD-OBS =       56578.14678002 / MJD of observation start
MJD-END =      56578.147821687 / Exposure end
OPENSHUT= '2013-10-13T03:31:21.823030' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               243535 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'survey  '           / Current observing orogram
OBSERVER= 'JH, HW, NM'         / Observer name(s)
PROPOSER= 'Frieman '           / Proposal Principle Investigator
DTPI    = 'Frieman           ' /  Principal Investigator
PROPID  = '2012B-0001'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
HEX     =                 4251 / DES Hex number
TILING  =                    4 / DES Tiling number
SEQID   = '116+6 enqueued on 2013-10-13 03:19:01Z by SurveyTactician' / Sequence
SEQNUM  =                    1 / Number of image in sequence
SEQTOT  =                    1 / Total number of images in sequence
AOS     =                    T / AOS data available if true
BCAM    =                    T / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'z DECam SDSS c0004 9260.0 1520.0' / Unique filter identifier
FILTPOS = 'cassette_3'         / Filter position in FCM
INSTANCE= 'DECam_20131012'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '00:46:35.912'       / [HH:MM:SS] Target RA
DEC     = '01:23:32.165'       / [DD:MM:SS] Target DEC
TELRA   = '00:46:35.980'       / [HH:MM:SS] Telescope RA
TELDEC  = '01:23:32.201'       / [DD:MM:SS] Telescope DEC
HA      = '-00:32:00.850'      / [HH:MM:SS] Telescope hour angle
ZD      =                32.56 / [deg] Telescope zenith distance
AZ      =                15.07 / [deg] Telescope azimuth angle
TELFOCUS= '-380.33,-3212.13,2625.59,121.49,-111.60,-0.00' / DECam hexapod settin
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =                9.012 / [m/s] Wind speed
WINDDIR =                 360. / [deg] Wind direction (from North)
HUMIDITY=                  31. / [%] Ambient relative humidity (outside)
PRESSURE=                 777. / [Torr] Barometric pressure (outside)
DIMMSEE =                0.525 / [arcsec] DIMM Seeing
OUTTEMP =                 10.6 / [deg C] Outside temperature
AIRMASS =                 1.19 / Airmass
GSKYVAR =                 0.02 / RASICAM global sky standard deviation
GSKYHOT =                   0. / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=                 12.2 / [deg C] Mirror surface average temperature
MAIRTEMP=                 12.4 / [deg C] Mirror temperature above surface
UPTRTEMP=               11.486 / [deg C] Upper truss average temperature
LWTRTEMP=                -999. / [deg C] Lower truss average temperature
PMOSTEMP=                11.65 / [deg C] Mirror top surface temperature
UTN-TEMP=               11.535 / [deg C] Upper truss temperature north
UTS-TEMP=                11.42 / [deg C] Upper truss temperature south
UTW-TEMP=                11.58 / [deg C] Upper truss temperature west
UTE-TEMP=                11.41 / [deg C] Upper truss temperature east
PMN-TEMP=                 11.8 / [deg C] Mirror north edge temperature
PMS-TEMP=                 12.1 / [deg C] Mirror south edge temperature
PMW-TEMP=                 12.6 / [deg C] Mirror west edge temperature
PME-TEMP=                 12.3 / [deg C] Mirror east edge temperature
DOMELOW =                12.23 / [deg C] Low dome temperature
DOMEHIGH=                -999. / [deg C] High dome temperature
DOMEFLOR=                 13.1 / [deg C] Dome floor temperature
G-MEANX =               0.0001 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0316 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[0.10,1.00,-8.77,-0.52,0.04,0.14,0.47,-0.12,-0.53,]' / Mean Wavefront
DONUTFS3= '[-0.82,1.14,8.94,-0.43,0.05,0.21,0.41,0.09,-0.55,]' / Mean Wavefront
DONUTFS2= '[-0.21,0.23,-8.53,-0.30,0.17,-0.01,0.58,0.32,-0.13,]' / Mean Wavefron
DONUTFS1= '[-0.07,1.47,8.92,-0.28,0.24,0.17,0.35,0.24,0.02,]' / Mean Wavefront f
G-FLXVAR=         49089120.605 / [arcsec] Guider mean guide star flux variances
G-MEANXY=             0.003246 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[-0.53,1.22,-8.63,-0.52,-0.13,-0.01,0.02,0.29,-0.39,]' / Mean Wavefro
DONUTFN2= '[2.19,1.45,8.86,-1.12,-0.41,0.05,0.03,0.11,-0.12,]' / Mean Wavefront
DONUTFN3= '[0.49,0.38,-8.61,-0.33,-0.35,0.12,-0.23,0.01,0.26,]' / Mean Wavefront
DONUTFN4= '[2.14,1.35,8.97,-0.34,-1.33,0.04,0.05,-0.12,0.15,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2013-10-13T03:33:15.500773'
G-FEEDBK=                   10 / [%] Guider feedback
G-CCDNUM=                    4 / Number of guide CCDs that remained active
DOXT    =                 0.22 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.1845 / [arcsec] Guider x-axis maximum offset
FADZ    =               -24.61 / [um] FA Delta focus.
FADY    =               -31.88 / [um] FA Delta Y.
FADX    =                -9.71 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                -20.5 / [arcsec] FA Delta Y-theta.
DODZ    =               -24.61 / [um] Delta-Z from donut analysis
DODY    =                -0.41 / [um] Y-decenter from donut analysis
DODX    =                -1.03 / [um] X-decenter from donut analysis
BCAMAZ  =                   0. / [arcsec] BCAM hexapod rot. about z-axis
MULTIEXP=                    F / Frame contains multiple exposures if true
BCAMAX  =              -24.796 / [arcsec] BCAM hexapod rot. about x-axis
BCAMAY  =              -27.446 / [arcsec] BCAM hexapod rot. about y-axis
BCAMDY  =              910.417 / [micron] BCAM hexapod y-offset
BCAMDX  =              396.274 / [micron] BCAM hexapod x-offset
SKYUPDAT= '2013-10-13T03:27:46' / Time of last RASICAM exposure (UTC)
G-SEEING=                1.523 / [arcsec] Guider average seeing
G-TRANSP=                1.021 / Guider average sky transparency
G-MEANY2=             0.012122 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                 0.48 / [arcsec] Y-theta from donut analysis
G-LATENC=                  1.3 / [s] Guider avg. latency between exposures
MANIFEST=                    F
LUTVER  = 'working-trunk'      / Hexapod Lookup Table version
FAXT    =                -0.14 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.2727 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.006373 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:19'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTSITE  = 'ct                ' /  observatory location
DTTELESC= 'ct4m              ' /  telescope identifier
DTINSTRU= 'decam             ' /  instrument identifier
DTCALDAT= '2013-10-12        ' /  calendar date from observing schedule
ODATEOBS= '                  ' /  previous DATE-OBS
DTUTC   = '2013-10-13T03:33:45' /  post exposure UTC epoch from DTS
DTOBSERV= 'NOAO              ' /  scheduling institution
DTPROPID= '2012B-0001        ' /  observing proposal ID
DTPIAFFL= '                  ' /  PI affiliation
DTTITLE = '                  ' /  title of observing proposal
DTCOPYRI= 'AURA              ' /  copyright holder of data
DTACQUIS= 'pipeline2.ctio.noao.edu' /  host name of data acquisition computer
DTACCOUN= 'sispi             ' /  observing account name
DTACQNAM= '/data_local/images/DTS/2012B-0001/DECam_00243535.fits.fz' /  file na
DTNSANAM= 'dec114946.fits    ' /  file name in NOAO Science Archive
DTQUEUE = 'des               ' /  DTS queue (1724)
DTSTATUS= 'done              ' /  data transport status
SB_HOST = 'pipeline2.ctio.noao.edu' /  iSTB client host
SB_ACCOU= 'sispi             ' /  iSTB client user account
SB_SITE = 'ct                ' /  iSTB host site
SB_LOCAL= 'dec               ' /  locale of iSTB daemon
SB_DIR1 = '20131012          ' /  level 1 directory in NSA DS
SB_DIR2 = 'ct4m              ' /  level 2 directory in NSA DS
SB_DIR3 = '2012B-0001        ' /  level 3 directory in NSA DS
SB_RECNO=               114946 /  iSTB sequence number
SB_ID   = 'dec114946         ' /  unique iSTB identifier
SB_NAME = 'dec114946.fits    ' /  name assigned by iSTB
RMCOUNT =                    0 /  remediation counter
RECNO   =               114946 /  NOAO Science Archive sequence number
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DETSEC  = '[2049:4096,8193:12288]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[3073:4096,8193:12288]' / Detector display tile for amp A
CCDSECA = '[1025:2048,1:4096]' / CCD section from amp A
AMPSECA = '[2048:1025,4096:1]' / CCD section in read order for amp A
DATASECA= '[1025:2048,1:4096]' / Data section from amp A
DETSECB = '[2049:3072,8193:12288]' / Detector display tile for amp B
CCDSECB = '[1:1024,1:4096]'    / CCD section from amp B
AMPSECB = '[1:1024,4096:1]'    / CCD section in read order for amp B
DATASECB= '[1:1024,1:4096]'    / Data section from amp B
DETECTOR= 'S3-06_123195-15-3'  / Detector Identifier
CCDNUM  =                    1 / CCD number
DETPOS  = 'S29     '           / detector position ID
GAINA   =     4.22924692588568
RDNOISEA=        6.67373068433 / [electrons] Read noise for amp A
SATURATA=     40349.7382898429
GAINB   =      4.3537493649292
RDNOISEB=        6.52878464819 / [electrons] Read noise for amp B
SATURATB=     36760.7078176455
CRPIX1  =             210.2375 / Reference pixel on this axis
CRPIX2  =        231.052078125 / Reference pixel on this axis
FPA     = 'DECAM_BKP3'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 12 5.210000'    / Monsoon module
SLOT01  = 'DESCB 11 4.010000'  / Monsoon module
SLOT02  = 'DESCB 12 4.010000'  / Monsoon module
SLOT03  = 'CCD12 29 4.080000'  / Monsoon module
SLOT04  = 'CCD12 15 4.080000'  / Monsoon module
SLOT05  = 'CCD12 28 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =   1.292520097439E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =  -1.085639046593E-02 / Projection distortion parameter
PV2_9   =   6.447478695312E-03 / Projection distortion parameter
CD1_1   =  -5.1713608030163E-6 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
PV2_0   =  -5.849774124871E-03 / Projection distortion parameter
PV2_1   =   1.022929739495E+00 / Projection distortion parameter
PV2_2   =  -9.977105369175E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -2.553062556249E-02 / Projection distortion parameter
PV2_5   =   2.040206230505E-02 / Projection distortion parameter
PV2_6   =  -9.427490587599E-03 / Projection distortion parameter
PV2_7   =   9.366132237292E-03 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   1.074194108158E-02 / Projection distortion parameter
PV2_10  =  -2.902607846633E-03 / Projection distortion parameter
PV1_4   =   7.981234793806E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =  -1.059990345521E-02 / Projection distortion parameter
PV1_1   =   1.015158686165E+00 / Projection distortion parameter
PV1_0   =   4.293383147323E-03 / Projection distortion parameter
PV1_9   =   7.785488836336E-03 / Projection distortion parameter
PV1_8   =  -7.287409639159E-03 / Projection distortion parameter
CD1_2   =  0.00466247324120065 / Linear projection matrix
PV1_5   =  -2.011852297597E-02 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -0.0046626715123117 / Linear projection matrix
CD2_2   =  -5.0532776598112E-6 / Linear projection matrix
PV1_10  =  -3.686927828818E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   1.165594106117E+01 / World coordinate on this axis
CRVAL2  =   1.396621201554E+00 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Wed Jul  3 21:11:57 2019' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Wed Jul  3 21:11:57 2019' / overscan corrected
BIASFIL = 'FV_20131031_8ed2628-56596Z_01.fits' / Bias correction file
BAND    = 'z       '
NITE    = '20131012'
DESBIAS = 'Wed Jul  3 21:13:28 2019'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Wed Jul  3 21:13:29 2019'
DESBPM  = 'Wed Jul  3 21:13:29 2019' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20130716_cd_01.fits' / BPM file used to build mask
DESSAT  = 'Wed Jul  3 21:13:29 2019' / Flag saturated pixels
NSATPIX =                 5296 / Number of saturated pixels
FLATMEDA=     4.22924692588568
FLATMEDB=      4.3537493649292
SATURATE=     40349.7382898429
DESGAINC= 'Wed Jul  3 21:13:29 2019'
FLATFIL = 'STARFLAT_20131115_94b995c-56611zF_01.fits' / Dome flat correction fil
DESFLAT = 'Wed Jul  3 21:13:30 2019'
STARFIL = 'STARFLAT_20130829_94a2c53-zP_ci_01.fits' / Star flat correction file
DESSTAR = 'Wed Jul  3 21:13:31 2019'
HISTORY Wed Jul  3 21:13:31 2019 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                    4
FZQMETHD= 'SUBTRACTIVE_DITHER_1'
DESBLEED= 'Wed Jul 3 21:17:03 2019' / bleed trail masking
NBLEED  = 20054  / Number of bleed trail pixels.
STARMASK= 'Wed Jul 3 21:17:03 2019' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DECALS_DR9_94fbe54-fil
HISTORY z-ccd01z/DECALS_DR9_94fbe54-z-131013033121_01.fits omibleed_01.fits
DES_EXT = 'IMAGE   '
FWHM    =             3.951917 / Median FWHM in pixels
ELLIPTIC=            0.0956879 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
DESCRMSK= 'Wed Jul  3 21:29:17 2019' / Run Maskcosmics. Unable to fit sky
HISTORY DESDM: maskcosmics omimask_01.fits: Unable to fit sky. No cosmics ray de
HISTORY tections done. Saving with no changes to either masks.
PHOTREF = 'PS1     '           / Photometric ref. catalog
MAGZERO =               29.966 / [mag] Magnitude zero point
MAGZPT  =               25.080 / [mag] Magnitude zero point per sec
NPHTMTCH=                 2986 / Number of phot. stars matched
RADIUS  =                 1.67 / [pix] Average radius of stars
RADSTD  =                0.051 / [pix] Standard deviation of star radii
WCSDIM  =                    2
WAT0_001= 'system=image'
WAT1_001= 'wtype=tpv axtype=ra'
WAT2_001= 'wtype=tpv axtype=dec'
MEAN    =             1344.552
NCOMBINE=                59001
PUPILSKY=     1341.89691548162
PUPILSUM=     1077076.14295322
PUPILMAX=     30.1937141418457
PUPILPAR= '0.0 -5.0 10.0 10.0 210.0 15.0 "BPM"'
HISTORY Image was compressed by CFITSIO using scaled integer quantization:
HISTORY   q = 4.000000 / quantized level scaling parameter
HISTORY 'SUBTRACTIVE_DITHER_1' / Pixel Quantization Algorithm
CHECKSUM= '1jVc1iUb1iUb1iUb'   / HDU checksum updated 2020-07-14T21:28:29
DATASUM = '1160492814'         / data unit checksum updated 2020-07-14T21:28:29
PUPSCL  =    0.064060308937246
   Þ     ¡  Ì                 é  é  Ì  	  	      êÿÿÿ               ÿ	       @J``
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
 
@`
`
 
@`
`
 
@`
 
@`
 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
`````` @ @` @ @```````` @ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
 
@`
 
@`
`
 
@`
`
 
@`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@U   ÿ      ¡ J ÿ      A¡  ÿ 
   
  @Á "@Ÿ  ÿ 
   
  @º 0@·  ÿ 
   
  @µ :@²  ÿ 
   
  @° D@­  ÿ 
   
  @¬ L@©  ÿ 
   
  @š T@¥  ÿ 
   
  @¥ Z@¢  ÿ 
   
  @¢ `@  ÿ 
   
  @ f@  ÿ 
   
  @ l@  ÿ 
   
  @ p@  ÿ 
   
  @ t@  ÿ 
   
  @ z@  ÿ 
   
  @ ~@  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @  @  ÿ 
   
  @ ¢@~  ÿ 
   
  @ Š@|  ÿ 
   
  @~ š@{  ÿ 
   
  @| ¬@y  ÿ 
   
  @{ ®@x  ÿ 
   
  @z °@w  ÿ 
   
  @x Ž@u  ÿ 
   
  @w ¶@t  ÿ 
   
  @v ž@s  ÿ 
   
  @t Œ@q  ÿ 
   
  @s Ÿ@p  ÿ 
   
  @r À@o  ÿ 
   
  @q Â@n  ÿ 
   
  @o Æ@l  ÿ 
   
  @n È@k  ÿ 
   
  @m Ê@j  ÿ 
   
  @l Ì@i  ÿ 
   
  @k Î@h  ÿ 
   
  @j Ð@g  ÿ 
   
  @i Ò@f  ÿ 
   
  @h Ô@e  ÿ 
   
  @g Ö@d  ÿ 
   
  @f Ø@c  ÿ 
   
  @e Ú@b  ÿ 
   
  @d Ü@a  ÿ 
   
  @c Þ@`  ÿ 
   
  @b à@_  ÿ 
   
  @a â@^  ÿ 
   
  @` ä@]  ÿ 
   
  @_ æ@\  ÿ 
   
  @^ è@[  ÿ 
   
  @] ê@Z  ÿ 
   
  @\ ì@Y  ÿ 
   
  @[ î@X  ÿ 
   
  @Z ð@W  ÿ 
   
  @Y ò@V  ÿ 
   
  @X ô@U  ÿ 
   
  @W ö@T  ÿ 
   
  @V ø@S  ÿ 
   
  @U ú@R  ÿ 
   
  @T ü@Q  ÿ 
   
  @S þ@P  ÿ 
   
  @R @O  ÿ 
   
  @Q@N  ÿ 
   
  @P@M  ÿ 
   
  @O@L  ÿ 
   
  @N@K  ÿ 
   
  @M
@J  ÿ 
   
  @L@I  ÿ 
   
  @K@H  ÿ 
   
  @J@G  ÿ 
   
  @I@F  ÿ 
   
  @H@E  ÿ 
   
  @G@D  ÿ 
   
  @F@C  ÿ 
   
  @E@B  ÿ 
   
  @D@A  ÿ 
   
  @C@@  ÿ 
   
  @B @?  ÿ 
   
  @A"@>  ÿ 
   
  @@$@=  ÿ 
   
  @?&@<  ÿ 
   
  @>(@;  ÿ 
   
  @=*@:  ÿ 
   
  @<,@9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @8  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @ @9  ÿ      @< @P @9  ÿ 
   
  @<,@9  ÿ 
   
  @=*@:  ÿ 
   
  @>(@;  ÿ 
   
  @?&@<  ÿ 
   
  @@$@=  ÿ 
   
  @A"@>  ÿ 
   
  @B @?  ÿ 
   
  @C@@  ÿ 
   
  @D@A  ÿ 
   
  @E@B  ÿ 
   
  @F@C  ÿ 
   
  @G@D  ÿ 
   
  @H@E  ÿ 
   
  @I@F  ÿ 
   
  @J@G  ÿ 
   
  @K@H  ÿ 
   
  @L@I  ÿ 
   
  @M
@J  ÿ 
   
  @N@K  ÿ 
   
  @O@L  ÿ 
   
  @P@M  ÿ 
   
  @Q@N  ÿ 
   
  @R @O  ÿ 
   
  @S þ@P  ÿ 
   
  @T ü@Q  ÿ 
   
  @U ú@R  ÿ 
   
  @V ø@S  ÿ 
   
  @W ö@T  ÿ 
   
  @X ô@U  ÿ 
   
  @Y ò@V  ÿ 
   
  @Z ð@W  ÿ 
   
  @[ î@X  ÿ 
   
  @\ ì@Y  ÿ 
   
  @] ê@Z  ÿ 
   
  @^ è@[  ÿ 
   
  @_ æ@\  ÿ 
   
  @` ä@]  ÿ 
   
  @a â@^  ÿ 
   
  @b à@_  ÿ 
   
  @c Þ@`  ÿ 
   
  @d Ü@a  ÿ 
   
  @e Ú@b  ÿ 
   
  @f Ø@c  ÿ 
   
  @g Ö@d  ÿ 
   
  @h Ô@e  ÿ 
   
  @i Ò@f  ÿ 
   
  @j Ð@g  ÿ 
   
  @k Î@h  ÿ 
   
  @l Ì@i  ÿ 
   
  @m Ê@j  ÿ 
   
  @n È@k  ÿ 
   
  @o Æ@l  ÿ 
   
  @q Â@n  ÿ 
   
  @r À@o  ÿ 
   
  @s Ÿ@p  ÿ 
   
  @t Œ@q  ÿ 
   
  @v ž@s  ÿ 
   
  @w ¶@t  ÿ 
   
  @x Ž@u  ÿ 
   
  @z °@w  ÿ 
   
  @{ ®@x  ÿ 
   
  @| ¬@y  ÿ 
   
  @~ š@{  ÿ 
   
  @ Š@|  ÿ 
   
  @ ¢@~  ÿ 
   
  @  @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ @  ÿ 
   
  @ ~@  ÿ 
   
  @ z@  ÿ 
   
  @ t@  ÿ 
   
  @ p@  ÿ 
   
  @ l@  ÿ 
   
  @ f@  ÿ 
   
  @¢ `@  ÿ 
   
  @¥ Z@¢  ÿ 
   
  @š T@¥  ÿ 
   
  @¬ L@©  ÿ 
   
  @° D@­  ÿ 
   
  @µ :@²  ÿ 
   
  @º 0@·  ÿ 
   
  @Á "@Ÿ U ÿ      A¡