  �V  =  �$TITLE = "DECaLS_7720_g"
$CTIME = 1195201789
$MTIME = 1195201789
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    T / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2017-11-14T22:18:10' / Date FITS file was generated
IRAF-TLM= '2017-11-15T01:35:23' / Time of last modification
OBJECT  = 'DECaLS_7720_g'      / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_46.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                 148. / [s] Requested exposure duration
EXPTIME =                 148. / [s] Exposure duration
EXPDUR  =                 148. / [s] Exposure duration
DARKTIME=          140.3121998 / [s] Dark time
OBSID   = 'ct4m20171113t010333' / Unique Observation ID
DATE-OBS= '2017-11-13T01:03:33.350951' / DateTime of observation start (UTC)
TIME-OBS= '01:03:33.350951'    / Time of observation start (UTC)
MJD-OBS =       58070.04413601 / MJD of observation start
MJD-END =      58070.045848973 / Exposure end
OPENSHUT= '2017-11-13T01:03:24.530780' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               695611 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'The DECam Legacy Survey of the SDSS Equatorial Sky' / Current observi
OBSERVER= 'C. Stillman, J.Moustakas, M.Poremba' / Observer name(s)
PROPOSER= 'Schlegel'           / Proposal Principle Investigator
DTPI    = 'Schlegel'           / Proposal Principle Investigator (iSTB)
PROPID  = '2014B-0404'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'g DECam SDSS c0001 4720.0 1520.0' / Unique filter identifier
FILTPOS = 'cassette_2'         / Filter position in FCM
INSTANCE= 'DECam_20171112'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '23:43:51.600'       / [HH:MM:SS] Target RA
DEC     = '23:45:57.600'       / [DD:MM:SS] Target DEC
TELRA   = '23:43:51.517'       / [HH:MM:SS] Telescope RA
TELDEC  = '23:45:53.197'       / [DD:MM:SS] Telescope DEC
HA      = '00:04:54.810'       / [HH:MM:SS] Telescope hour angle
ZD      =                54.03 / [deg] Telescope zenith distance
AZ      =             358.6672 / [deg] Telescope azimuth angle
DOMEAZ  =                0.538 / [deg] Dome azimuth angle
ZPDELRA =             -28.0368 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                 -0.1 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-1259.06,-3221.72,2296.32,110.58,-100.65, 0.01' / DECam hexapod setti
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =                3.701 / [m/s] Wind speed
WINDDIR =                 340. / [deg] Wind direction (from North)
HUMIDITY=                  31. / [%] Ambient relative humidity (outside)
PRESSURE=                 777. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                0.879 / [arcsec] DIMM2 Seeing
MASS2   =                 0.41 /  MASS(2) FSEE
ASTIG1  =                -0.05 / 4MAPS correction 1
ASTIG2  =                -0.13 / 4MAPS correction 2
OUTTEMP =                 15.2 / [deg C] Outside temperature
AIRMASS =                  1.7 / Airmass
GSKYVAR =                0.037 / RASICAM global sky standard deviation
GSKYHOT =                0.071 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               13.262 / [deg C] Mirror surface average temperature
MAIRTEMP=                 13.9 / [deg C] Mirror temperature above surface
UPTRTEMP=               15.369 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                13.85 / [deg C] Mirror top surface temperature
UTN-TEMP=               15.265 / [deg C] Upper truss temperature north
UTS-TEMP=               15.255 / [deg C] Upper truss temperature south
UTW-TEMP=                 15.6 / [deg C] Upper truss temperature west
UTE-TEMP=               15.355 / [deg C] Upper truss temperature east
PMN-TEMP=                 13.1 / [deg C] Mirror north edge temperature
PMS-TEMP=                13.05 / [deg C] Mirror south edge temperature
PMW-TEMP=                 12.9 / [deg C] Mirror west edge temperature
PME-TEMP=                  14. / [deg C] Mirror east edge temperature
DOMELOW =               15.565 / [deg C] Low dome temperature
DOMEHIGH=                   0. / [deg C] High dome temperature
DOMEFLOR=                  8.5 / [deg C] Dome floor temperature
G-MEANX =               0.0263 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0548 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[0.44,0.95,-8.65,-0.14,-0.07,-0.21,-0.25,0.06,-0.39,]' / Mean Wavefro
DONUTFS3= '[0.75,1.85,8.74,0.03,-0.08,0.05,-0.19,0.04,-0.34,]' / Mean Wavefront
DONUTFS2= '[1.72,0.07,-8.61,0.43,0.02,-0.14,-0.22,0.37,-0.01,]' / Mean Wavefront
DONUTFS1= '[0.95,1.32,8.76,0.27,0.23,0.21,-0.25,0.29,0.11,]' / Mean Wavefront fo
G-FLXVAR=          1743976.236 / [arcsec] Guider mean guide star flux variances
G-MEANXY=             0.001659 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[1.39,0.26,-8.74,-0.14,-0.16,-0.11,-0.09,0.26,-0.21,]' / Mean Wavefro
DONUTFN2= '[1.20,1.04,8.75,-0.32,-0.17,0.13,0.11,0.25,-0.08,]' / Mean Wavefront
DONUTFN3= '[1.04,0.39,-8.72,0.09,-0.32,-0.15,-0.02,0.06,0.30,]' / Mean Wavefront
DONUTFN4= '[1.55,1.53,8.75,0.13,-0.37,-0.04,0.06,-0.03,0.16,]' / Mean Wavefront
HIERARCH TIME_RECORDED = '2017-11-13T01:06:23.343784'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    4 / Number of guide CCDs that remained active
DOXT    =                 0.12 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.3218 / [arcsec] Guider x-axis maximum offset
FADZ    =                -6.46 / [um] FA Delta focus.
FADY    =              -255.29 / [um] FA Delta Y.
FADX    =               308.83 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =               -21.57 / [arcsec] FA Delta Y-theta.
DODZ    =                -6.46 / [um] Delta-Z from donut analysis
DODY    =                -1.13 / [um] Y-decenter from donut analysis
DODX    =                -0.93 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2017-11-13T01:03:19' / Time of last RASICAM exposure (UTC)
G-SEEING=                2.134 / [arcsec] Guider average seeing
G-TRANSP=                0.617 / Guider average sky transparency
G-MEANY2=             0.013935 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                -0.04 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.348 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                -11.6 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.3826 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.014308 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:56'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTTELESC= 'ct4m    '
DTPROPID= '2014B-0404'
DTCALDAT= '2017-11-12'
DTACQNAM= '/data_local/images/DTS/2014B-0404/DECam_00695611.fits.fz'
DTSITE  = 'ct      '
ODATEOBS= '2017-11-13T01:03:33.350951'
DTINSTRU= 'decam   '
DTNSANAM= 'c4d_171113_010333_ori.fits.fz'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DATASEC = '[1:2048,1:4096]'    / Data section within image
DETSEC  = '[18433:20480,6145:10240]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[18433:19456,6145:10240]' / Detector display tile for amp A
CCDSECA = '[1:1024,1:4096]'    / CCD section from amp A
AMPSECA = '[1:1024,1:4096]'    / CCD section in read order for amp A
DATASECA= '[1:1024,1:4096]'    / Data section from amp A
DETSECB = '[19457:20480,6145:10240]' / Detector display tile for amp B
CCDSECB = '[1025:2048,1:4096]' / CCD section from amp B
AMPSECB = '[2048:1025,1:4096]' / CCD section in read order for amp B
DATASECB= '[1025:2048,1:4096]' / Data section from amp B
DETECTOR= 'S3-213_124750-22-3' / Detector Identifier
CCDNUM  =                   46 / CCD number
DETPOS  = 'N15     '           / detector position ID
GAINA   =     4.00619415946591
RDNOISEA=             5.836056 / [electrons] Read noise for amp A
SATURATA=     38983.8958830745
GAINB   =     4.02115134075783
RDNOISEB=             6.039488 / [electrons] Read noise for amp B
SATURATB=       38842.84046135
CRPIX1  =  -4.612000000000E+03 / Reference pixel on this axis
CRPIX2  =   8.437000000000E+03 / Reference pixel on this axis
FPA     = 'DECAM_BKP1'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 15 5.210000'    / Monsoon module
SLOT01  = 'DESCB 22 4.010000'  / Monsoon module
SLOT02  = 'DESCB 3 4.010000'   / Monsoon module
SLOT03  = 'CCD12 16 4.080000'  / Monsoon module
SLOT04  = 'CCD12 11 4.080000'  / Monsoon module
SLOT05  = 'CCD12 14 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =        2000.00000000 / Mean equinox
PV1_7   =  -7.781843348242E-04 / Projection distortion parameter
CUNIT1  = 'deg     '           / Axis unit
PV2_8   =   6.076319161388E-03 / Projection distortion parameter
PV2_9   =   5.940945456044E-04 / Projection distortion parameter
CD1_1   =  -1.115216309878E-07 / Linear projection matrix
LTM2_2  =                   1. / Detector to image transformation
LTM2_1  =                   0. / Detector to image transformation
PV2_0   =   1.218983392829E-03 / Projection distortion parameter
PV2_1   =   1.003787461007E+00 / Projection distortion parameter
PV2_2   =   3.123503955312E-03 / Projection distortion parameter
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =  -8.583371174037E-03 / Projection distortion parameter
PV2_5   =   7.982008100308E-03 / Projection distortion parameter
PV2_6   =   2.887368469330E-03 / Projection distortion parameter
PV2_7   =  -1.277299386312E-02 / Projection distortion parameter
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =   5.488696535234E-03 / Projection distortion parameter
PV2_10  =   1.143926125105E-03 / Projection distortion parameter
PV1_4   =   4.786870426684E-03 / Projection distortion parameter
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =   2.793058962057E-03 / Projection distortion parameter
PV1_1   =   1.008708267735E+00 / Projection distortion parameter
PV1_0   =   1.283813867097E-03 / Projection distortion parameter
LTM1_2  =                   0. / Detector to image transformation
PV1_9   =   1.404438473406E-03 / Projection distortion parameter
PV1_8   =   3.865498449985E-03 / Projection distortion parameter
CD1_2   =   7.284975592106E-05 / Linear projection matrix
PV1_5   =   6.586641743571E-03 / Projection distortion parameter
CUNIT2  = 'deg     '           / Axis unit
CD2_1   =  -7.287568371554E-05 / Linear projection matrix
CD2_2   =  -1.101405085785E-07 / Linear projection matrix
LTV2    =                   0. / Detector to image transformation
LTV1    =                   0. / Detector to image transformation
PV1_10  =   3.147605677229E-03 / Projection distortion parameter
CTYPE2  = 'DEC--TPV'           / WCS projection type for this axis
CTYPE1  = 'RA---TPV'           / WCS projection type for this axis
CRVAL1  =   3.559647990376E+02 / World coordinate on this axis
CRVAL2  =   2.376333438094E+01 / World coordinate on this axis
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Tue Nov 14 15:27:25 2017' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Tue Nov 14 15:27:25 2017' / overscan corrected
DESSAT  = 'Tue Nov 14 15:27:25 2017' / saturated pixels flagged
NSATPIX =                 1680 / Number of saturated pixels
BIASFIL = 'DES17B_20171106_90095f1-58066Z_46.fits' / Bias correction file
BAND    = 'g       '
NITE    = '20171112'
DESBIAS = 'Tue Nov 14 15:47:17 2017'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Tue Nov 14 15:47:18 2017'
DESBPM  = 'Tue Nov 14 15:47:18 2017' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_46.fits' / BPM file used to build mask
FLATMEDA=     4.00619415946591
FLATMEDB=     4.02115134075783
SATURATE=     38983.8958830745
DESGAINC= 'Tue Nov 14 15:47:18 2017'
FLATFIL = 'DES17B_20171106_90095f1-58066gF_46.fits' / Dome flat correction file
DESFLAT = 'Tue Nov 14 15:47:18 2017'
STARFIL = 'DECam_Master_20130829v3-gI_ci_g_46.fits' / Star flat correction file
DESSTAR = 'Tue Nov 14 15:47:19 2017'
HISTORY Tue Nov 14 15:47:19 2017 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                   16
FZQMETHD= 'SUBTRACTIVE_DITHER_2'
DESBLEED= 'Tue Nov 14 18:19:55 2017' / bleed trail masking
NBLEED  = 4563  / Number of bleed trail pixels.
STARMASK= 'Tue Nov 14 18:19:55 2017' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC17B_20171110_90123f
HISTORY c-filg-ccd46/DEC17B_20171110_90123fc-g-171113010333_46.fits omibleed_46.
HISTORY fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Nov 14 18:20 Bad pixel file is omibleed_46_bpm.pl'
FWHM    =              4.82198 / Median FWHM in pixels
ELLIPTIC=            0.1198242 / Median ELLIPTICITY
RADECSYS= 'ICRS    '           / Astrometric system
SCAMPFLG=                    0 / WCS successful
    �                          h  h      X  �      ���               �� X       Y�`	`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@	`
 
@	 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
�� ��         �� 	   	  S�  �� 
   
  @�  �� 
   
  @�  �� 
   
  @#�  �� 
   
  @'�  �� 
   
  @+�  �� 
   
  @/�  �� 
   
   @3�  �� 
   
  �@7�  �� 
   
  �@9�  �� 
   
  �@=�  �� 
   
  �@?�  �� 
   
  �@A�  �� 
   
  �@C�  �� 
   
  �@E�  �� 
   
  �@G�  �� 
   
  �@I�  �� 
   
  �@K�  �� 
   
  �@M�  �� 
   
  �@O�  �� 
   
  �@Q�  �� 
   
  �@S�  �� 
   
  �@U�  �� 
   
  �@W�  �� 
   
  �@Y�  �� 
   
  �@[�  �� 
   
  �@]�  �� 
   
  �@_�  �� 
   
  �@a� 	 �� 
   
  �@c�  �� 
   
  �@e� 	 �� 
   
  �@c�  �� 
   
  �@a�  �� 
   
  �@_�  �� 
   
  �@]�  �� 
   
  �@[�  �� 
   
  �@Y�  �� 
   
  �@W�  �� 
   
  �@U�  �� 
   
  �@S�  �� 
   
  �@Q�  �� 
   
  �@O�  �� 
   
  �@M�  �� 
   
  �@K�  �� 
   
  �@I�  �� 
   
  �@G�  �� 
   
  �@E�  �� 
   
  �@C�  �� 
   
  �@A�  �� 
   
  �@?�  �� 
   
  �@=�  �� 
   
  �@9�  �� 
   
  �@7�  �� 
   
   @3�  �� 
   
  @/�  �� 
   
  @+�  �� 
   
  @'�  �� 
   
  @#�  �� 
   
  @�  �� 
   
  @�  �� 	   	  S�