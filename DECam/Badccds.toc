# Bad CCDs: Be sure values are comma delimited

CLASS = "'badccds'"
QUALITY = "'1'"
DATATYPE = "'keyword'"

MJD,MJDWIN1,MJDWIN2,VALUE

56626,NULL,-0.5,',61,'
56627,-0.4,1124.5,',2,61,'
57752,-0.4,613.5,',61,'
58366,-0.4,0.5,',53,54,55,57,58,59,60,61,62,'
58367,-0.4,NULL,',61,'
