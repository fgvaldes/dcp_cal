
# LED on

CLASS = "'ledmask'"
DIR = "'DECam'"
DATATYPE = "'file'"

MJD,MJDWIN1,MJDWIN2,IMAGEID,MATCH,VALUE

58067.5,0,2,'46','object_brite','ledmask_brite_46.pl'
58067.5,0,2,'46','object_300','ledmask_300_46.pl'
58067.5,0,2,'46','object_200','ledmask_200_46.pl'
58067.5,0,2,'46','object_100','ledmask_100_46.pl'
58067.5,0,2,'46','object_50','ledmask_50_46.pl'

58071.5,0,NULL,'46','zero','ledmaskZ.pl'
58071.5,0,NULL,'46','dflat_50','ledmaskF_50.pl'
58071.5,0,NULL,'46','dflat_200','ledmaskF_200.pl'

58071.5,0,NULL,'46','object_brite','ledmask_brite_46.pl'
58071.5,0,NULL,'46','object_300','ledmask_300_46.pl'
58071.5,0,NULL,'46','object_200','ledmask_200_46.pl'
58071.5,0,NULL,'46','object_100','ledmask_100_46.pl'
58071.5,0,NULL,'46','object_50','ledmask_50_46.pl'
