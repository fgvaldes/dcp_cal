  ŚV  ;Ě  x$TITLE = "DECaLS_2246_g"
$CTIME = 1195312630
$MTIME = 1195312630
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    T / File may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2017-11-14T18:27:02' / Date FITS file was generated
IRAF-TLM= '2017-11-14T19:17:07' / Time of last modification
OBJECT  = 'DECaLS_2246_g'      / Name of the object observed
PROCTYPE= 'raw     '           / Data processing level
PRODTYPE= 'image   '           / Data product type
PIXSCAL1=                 0.27 / [arcsec/pixel] Pixel scale, axis 1
PIXSCAL2=                 0.27 / [arcsec/pixel] Pixel scale, axis 2
FILENAME= 'omibleed_46.fits'
OBS-LONG=             70.81489 / [deg] Observatory east longitude
TELESCOP= 'CTIO 4.0-m telescope' / Telescope name
OBSERVAT= 'CTIO    '           / Observatory name
OBS-LAT =            -30.16606 / [deg] Observatory latitude
OBS-ELEV=                2215. / [m] Observatory elevation
INSTRUME= 'DECam   '           / Instrument used to obtain these data
EXPREQ  =                  80. / [s] Requested exposure duration
EXPTIME =                  80. / [s] Exposure duration
EXPDUR  =                  80. / [s] Exposure duration
DARKTIME=           72.8448501 / [s] Dark time
OBSID   = 'ct4m20171111t024032' / Unique Observation ID
DATE-OBS= '2017-11-11T02:40:32.466864' / DateTime of observation start (UTC)
TIME-OBS= '02:40:32.466864'    / Time of observation start (UTC)
MJD-OBS =       58068.11148689 / MJD of observation start
MJD-END =      58068.112412816 / Exposure end
OPENSHUT= '2017-11-11T02:40:24.211690' / Time when shutter opened (UTC)
TIMESYS = 'UTC     '           / Time system
EXPNUM  =               694957 / DECam exposure number
OBSTYPE = 'raw_obj '           / Type of observation
CAMSHUT = 'Open    '           / Camera shutter at exposure start
PROGRAM = 'The DECam Legacy Survey of the SDSS Equatorial Sky' / Current observi
OBSERVER= 'C. Stillman, J.Moustakas, M.Poemba, A.Walker' / Observer name(s)
PROPOSER= 'Schlegel'           / Proposal Principle Investigator
DTPI    = 'Schlegel'           / Proposal Principle Investigator (iSTB)
PROPID  = '2014B-0404'         / Proposal ID
EXCLUDED= '        '           / DECam components not used for this frame
AOS     =                    T / AOS data available if true
BCAM    =                    F / BCAM data available if true
GUIDER  =                    1 / Guider (0-absent,1-ok,2-lost star,3-los
SKYSTAT =                    T / Cloud camera (RASICAM) available if true
FILTER  = 'g DECam SDSS c0001 4720.0 1520.0' / Unique filter identifier
FILTPOS = 'cassette_2'         / Filter position in FCM
INSTANCE= 'DECam_20171110'     / SISPI instance name
ERRORS  = 'None    '           / SISPI readout errors
TELEQUIN=                2000. / Equinox of telescope coordinates
TELSTAT = 'Track   '           / Telescope tracking status
RA      = '01:02:04.560'       / [HH:MM:SS] Target RA
DEC     = '25:13:19.200'       / [DD:MM:SS] Target DEC
TELRA   = '01:02:04.470'       / [HH:MM:SS] Telescope RA
TELDEC  = '25:13:12.097'       / [DD:MM:SS] Telescope DEC
HA      = '00:16:04.800'       / [HH:MM:SS] Telescope hour angle
ZD      =                 55.6 / [deg] Telescope zenith distance
AZ      =             355.6411 / [deg] Telescope azimuth angle
DOMEAZ  =              355.618 / [deg] Dome azimuth angle
ZPDELRA =              -5.9296 / [arcsec] Telescope zeropoint adjustment (RA)
ZPDELDEC=                 -0.1 / [arcsec] Telescope zeropoint adjustment (DEC)
TELFOCUS= '-726.16,-3244.15,2232.90,94.66,-103.98, 0.00' / DECam hexapod setting
VSUB    =                    T / True if CCD substrate voltage is on
GSKYPHOT=                    T / RASICAM global sky clear flag
LSKYPHOT=                    F / RASICAM local sky clear flag
WINDSPD =                3.219 / [m/s] Wind speed
WINDDIR =                 147. / [deg] Wind direction (from North)
HUMIDITY=                  31. / [%] Ambient relative humidity (outside)
PRESSURE=                 776. / [Torr] Barometric pressure (outside)
DIMMSEE = 'NaN     '           / [arcsec] DIMM Seeing
DIMM2SEE=                0.488 / [arcsec] DIMM2 Seeing
MASS2   =                 0.18 /  MASS(2) FSEE
ASTIG1  =                -0.06 / 4MAPS correction 1
ASTIG2  =                -0.03 / 4MAPS correction 2
OUTTEMP =                  15. / [deg C] Outside temperature
AIRMASS =                 1.77 / Airmass
GSKYVAR =                0.024 / RASICAM global sky standard deviation
GSKYHOT =                0.015 / RASICAM global sky fraction above threshold
LSKYVAR = 'NaN     '           / RASICAM local sky standard deviation
LSKYHOT = 'NaN     '           / RASICAM local sky fraction above threshold
LSKYPOW = 'NaN     '           / RASICAM local sky normalized power
MSURTEMP=               13.225 / [deg C] Mirror surface average temperature
MAIRTEMP=                 13.8 / [deg C] Mirror temperature above surface
UPTRTEMP=               15.271 / [deg C] Upper truss average temperature
LWTRTEMP= 'NaN     '           / [deg C] Lower truss average temperature
PMOSTEMP=                 13.6 / [deg C] Mirror top surface temperature
UTN-TEMP=               15.215 / [deg C] Upper truss temperature north
UTS-TEMP=                 15.2 / [deg C] Upper truss temperature south
UTW-TEMP=               15.535 / [deg C] Upper truss temperature west
UTE-TEMP=               15.135 / [deg C] Upper truss temperature east
PMN-TEMP=                  13. / [deg C] Mirror north edge temperature
PMS-TEMP=                 13.1 / [deg C] Mirror south edge temperature
PMW-TEMP=                 13.1 / [deg C] Mirror west edge temperature
PME-TEMP=                 13.7 / [deg C] Mirror east edge temperature
DOMELOW =               15.395 / [deg C] Low dome temperature
DOMEHIGH=                   0. / [deg C] High dome temperature
DOMEFLOR=                  8.8 / [deg C] Dome floor temperature
G-MEANX =              -0.0319 / [arcsec] Guider x-axis mean offset
G-MEANY =               0.0674 / [arcsec] Guider y-axis mean offset
DONUTFS4= '[1.85,1.24,-8.69,-0.16,-0.12,-0.08,-0.24,0.08,-0.39,]' / Mean Wavefro
DONUTFS3= '[0.80,1.09,8.78,-0.02,0.03,0.11,-0.19,0.13,-0.35,]' / Mean Wavefront
DONUTFS2= '[2.36,0.63,-8.70,0.27,-0.07,-0.03,-0.19,0.39,0.02,]' / Mean Wavefront
DONUTFS1= '[1.08,1.36,8.79,0.19,0.40,0.34,-0.21,0.35,0.15,]' / Mean Wavefront fo
G-FLXVAR=          2232814.159 / [arcsec] Guider mean guide star flux variances
G-MEANXY=            -0.000811 / [arcsec2] Guider (xy) 2nd moment mean offset
DONUTFN1= '[0.90,-0.59,-8.84,-0.35,0.12,0.09,-0.01,0.24,-0.22,]' / Mean Wavefron
DONUTFN2= '[1.97,-0.31,8.66,-0.35,0.04,0.34,0.22,0.39,-0.06,]' / Mean Wavefront
DONUTFN3= '[1.49,0.95,-8.84,-0.08,-0.17,-0.06,-0.02,0.08,0.31,]' / Mean Wavefron
DONUTFN4= '[2.17,1.52,8.66,0.12,-0.03,0.13,0.21,0.06,0.25,]' / Mean Wavefront fo
HIERARCH TIME_RECORDED = '2017-11-11T02:42:13.286656'
G-FEEDBK= '10, 5   '           / [%] Guider feedback (HA, dec)
G-CCDNUM=                    4 / Number of guide CCDs that remained active
DOXT    =                -0.03 / [arcsec] X-theta from donut analysis
G-MAXX  =               0.2511 / [arcsec] Guider x-axis maximum offset
FADZ    =                 4.03 / [um] FA Delta focus.
FADY    =                 40.7 / [um] FA Delta Y.
FADX    =              -259.22 / [um] FA Delta X.
G-MODE  = 'auto    '           / Guider operation mode
FAYT    =                18.37 / [arcsec] FA Delta Y-theta.
DODZ    =                 4.03 / [um] Delta-Z from donut analysis
DODY    =                -1.58 / [um] Y-decenter from donut analysis
DODX    =                -0.74 / [um] X-decenter from donut analysis
MULTIEXP=                    F / Frame contains multiple exposures if true
SKYUPDAT= '2017-11-11T02:40:13' / Time of last RASICAM exposure (UTC)
G-SEEING=                2.091 / [arcsec] Guider average seeing
G-TRANSP=                0.619 / Guider average sky transparency
G-MEANY2=             0.008621 / [arcsec2] Guider (y) 2nd moment mean offset
DOYT    =                 0.05 / [arcsec] Y-theta from donut analysis
G-LATENC=                1.321 / [s] Guider avg. latency between exposures
LUTVER  = 'v20160516'          / Hexapod Lookup Table version
FAXT    =                -1.01 / [arcsec] FA Delta X-theta.
G-MAXY  =               0.2666 / [arcsec] Guider y-axis maximum offset
G-MEANX2=             0.007855 / [arcsec2] Guider (x) 2nd moment mean offset
SISPIVER= 'trunk   '           / SISPI software version
CONSTVER= 'DECAM:56'           / SISPI constants version
HDRVER  = '13      '           / DECam fits header version
DTTELESC= 'ct4m    '
DTPROPID= '2014B-0404'
DTCALDAT= '2017-11-10'
DTACQNAM= '/data_local/images/DTS/2014B-0404/DECam_00694957.fits.fz'
DTSITE  = 'ct      '
ODATEOBS= '2017-11-11T02:40:32.466864'
DTINSTRU= 'decam   '
DTNSANAM= 'c4d_171111_024032_ori.fits.fz'
WCSAXES =                    2 / WCS Dimensionality
DETSIZE = '[1:29400,1:29050]'  / Detector size
DATASEC = '[1:2048,1:4096]'    / Data section within image
DETSEC  = '[18433:20480,6145:10240]' / Location of this CCD on Focal Plane
CCDSEC  = '[1:2048,1:4096]'    / CCD section to display
DETSECA = '[18433:19456,6145:10240]' / Detector display tile for amp A
CCDSECA = '[1:1024,1:4096]'    / CCD section from amp A
AMPSECA = '[1:1024,1:4096]'    / CCD section in read order for amp A
DATASECA= '[1:1024,1:4096]'    / Data section from amp A
DETSECB = '[19457:20480,6145:10240]' / Detector display tile for amp B
CCDSECB = '[1025:2048,1:4096]' / CCD section from amp B
AMPSECB = '[2048:1025,1:4096]' / CCD section in read order for amp B
DATASECB= '[1025:2048,1:4096]' / Data section from amp B
DETECTOR= 'S3-213_124750-22-3' / Detector Identifier
CCDNUM  =                   46 / CCD number
DETPOS  = 'N15     '           / detector position ID
GAINA   =     4.00619415946591
RDNOISEA=             5.836056 / [electrons] Read noise for amp A
SATURATA=     38983.8958830745
GAINB   =     4.02115134075783
RDNOISEB=             6.039488 / [electrons] Read noise for amp B
SATURATB=       38842.84046135
CRPIX1  =               -4668. / Coordinate Reference axis 1
CRPIX2  =                8437. / Coordinate Reference axis 2
FPA     = 'DECAM_BKP1'         / DECam focal plane name
CCDBIN1 =                    1 / Pixel binning, axis 1
CCDBIN2 =                    1 / Pixel binning, axis 2
DHEINF  = 'MNSN fermi hardware' / DHE Hardware
DHEFIRM = 'demo30  '           / DHE Firmware
SLOT00  = 'MCB 15 5.210000'    / Monsoon module
SLOT01  = 'DESCB 22 4.010000'  / Monsoon module
SLOT02  = 'DESCB 3 4.010000'   / Monsoon module
SLOT03  = 'CCD12 16 4.080000'  / Monsoon module
SLOT04  = 'CCD12 11 4.080000'  / Monsoon module
SLOT05  = 'CCD12 14 4.080000'  / Monsoon module
RADESYS = 'FK5     '           / World coordinate reference frame
EQUINOX =                2000. / [yr] Equinox of WCS
PV1_7   =    -0.00124150405198 / PV distortion coefficient
CUNIT1  = 'deg     '
PV2_8   =     0.00341916488037 / PV distortion coefficient
PV2_9   =   -0.000338602303247 / PV distortion coefficient
CD1_1   =    -1.8249413473E-07 / World coordinate transformation matrix
LTM2_2  =                   1. / Detector to image transformation
LTM2_1  =                   0. / Detector to image transformation
PV2_0   =     0.00147829393152 / PV distortion coefficient
PV2_1   =        1.00683924379 / PV distortion coefficient
PV2_2   =     0.00241629553299 / PV distortion coefficient
PV2_3   =                   0. / PV distortion coefficient
PV2_4   =     0.00123580540733 / PV distortion coefficient
PV2_5   =     0.00493239194256 / PV distortion coefficient
PV2_6   =     0.00288719564727 / PV distortion coefficient
PV2_7   =    -0.00339663522804 / PV distortion coefficient
LTM1_1  =                   1. / Detector to image transformation
PV1_6   =      0.0026846636872 / PV distortion coefficient
PV2_10  =     0.00142454829367 / PV distortion coefficient
PV1_4   =     0.00416347740979 / PV distortion coefficient
PV1_3   =                   0. / PV distortion coefficient
PV1_2   =     0.00175547226328 / PV distortion coefficient
PV1_1   =        1.00827273875 / PV distortion coefficient
PV1_0   =     0.00108443456189 / PV distortion coefficient
LTM1_2  =                   0. / Detector to image transformation
PV1_9   =    0.000559647891333 / PV distortion coefficient
PV1_8   =     0.00407933553245 / PV distortion coefficient
CD1_2   =    7.28535892432E-05 / World coordinate transformation matrix
PV1_5   =     0.00615775194769 / PV distortion coefficient
CUNIT2  = 'deg     '
CD2_1   = -7.2849900453899E-05 / World coordinate transformation matrix
CD2_2   =   -1.82181666096E-07 / World coordinate transformation matrix
LTV2    =                   0. / Detector to image transformation
LTV1    =                   0. / Detector to image transformation
PV1_10  =    0.000941177077713 / PV distortion coefficient
CTYPE2  = 'DEC--TPV'           / Coordinate type
CTYPE1  = 'RA---TPV'           / Coordinate type
CRVAL1  =            15.518625 / [deg] WCS Reference Coordinate (RA)
CRVAL2  =            25.220027 / [deg] WCS Reference Coordinate (DEC)
VALIDA  =                    T / Data from amp A is valid
VALIDB  =                    T / Data from amp B is valid
NDONUTS =                    0 / AOS number of donuts analyzed for this CCD
        = '        '
PHOTFLAG=                    0 / Night Photometric (1) or not (0)
DESDCXTK= 'Tue Nov 14 11:28:48 2017' / DECam image conversion and crosstalk corr
XTALKFIL= 'xtalk_20130606.txt' / Crosstalk file
DESOSCN = 'Tue Nov 14 11:28:48 2017' / overscan corrected
DESSAT  = 'Tue Nov 14 11:28:48 2017' / saturated pixels flagged
NSATPIX =                  622 / Number of saturated pixels
BIASFIL = 'DES17B_20171106_90095f1-58066Z_46.fits' / Bias correction file
BAND    = 'g       '
NITE    = '20171110'
DESBIAS = 'Tue Nov 14 11:45:08 2017'
LINCFIL = 'linearity_table_v0.4.fits' / Nonlinearity correction file
DESLINC = 'Tue Nov 14 11:45:09 2017'
DESBPM  = 'Tue Nov 14 11:45:09 2017' / Construct mask from BPM
BPMFIL  = 'DECam_Master_20170101_cd_46.fits' / BPM file used to build mask
FLATMEDA=     4.00619415946591
FLATMEDB=     4.02115134075783
SATURATE=     38983.8958830745
DESGAINC= 'Tue Nov 14 11:45:09 2017'
FLATFIL = 'DES17B_20171106_90095f1-58066gF_46.fits' / Dome flat correction file
DESFLAT = 'Tue Nov 14 11:45:09 2017'
STARFIL = 'DECam_Master_20130829v3-gI_ci_g_46.fits' / Star flat correction file
DESSTAR = 'Tue Nov 14 11:45:09 2017'
HISTORY Tue Nov 14 11:45:09 2017 Null weights with mask 0x0209
FZALGOR = 'RICE_1  '
FZDTHRSD= 'CHECKSUM'
FZQVALUE=                   16
FZQMETHD= 'SUBTRACTIVE_DITHER_2'
DESBLEED= 'Tue Nov 14 12:17:00 2017' / bleed trail masking
NBLEED  = 1410  / Number of bleed trail pixels.
STARMASK= 'Tue Nov 14 12:17:00 2017' / created bright star mask
HISTORY DESDM: mkbleedmask -m -b 5 -f 1.0 -l 7 -n 7 -r 5 -s 40 -t 20 -v 3 -y 1.0
HISTORY  -E 5 /data/sharedfs/deccp/NHPPS_DATA/DCP/DCP_CCD/DEC17B_20171110_9011eb
HISTORY 1-filg-ccd46/DEC17B_20171110_9011eb1-g-171111024032_46.fits omibleed_46.
HISTORY fits
DES_EXT = 'IMAGE   '
FIXPIX  = 'Nov 14 12:17 Bad pixel file is omibleed_46_bpm.pl'
   Ţ                          Ź  Ź      ¤  x      ˙˙˙               ˙ ¤       YÉ`	`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
 
@`
 
@`
 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@`
 
@`
 
@`
`
`
`
 
@`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
o7 ˙         ˙ 	   	  Sć  ˙ 
   
  @Ř  ˙ 
   
  @'Ó  ˙ 
   
  @1Î  ˙ 
   
  ý@9Ę  ˙ 
   
  ú@?Ç  ˙ 
   
  ÷@EÄ  ˙ 
   
  ő@IÂ  ˙ 
   
  ň@Oż  ˙ 
   
  đ@S˝  ˙ 
   
  î@Wť  ˙ 
   
  ě@[š  ˙ 
   
  ę@_ˇ  ˙ 
   
  č@cľ  ˙ 
   
  ć@gł  ˙ 
   
  ĺ@i˛  ˙ 
   
  ă@m°  ˙ 
   
  â@oŻ  ˙ 
   
  ŕ@s­  ˙ 
   
  ß@uŹ  ˙ 
   
  Ý@yŞ  ˙ 
   
  Ü@{Š  ˙ 
   
  Ű@}¨  ˙ 
   
  Ú@§  ˙ 
   
  Ů@Ś  ˙ 
   
  ×@¤  ˙ 
   
  Ö@Ł  ˙ 
   
  Ő@˘  ˙ 
   
  Ô@Ą  ˙ 
   
  Ó@   ˙ 
   
  Ň@  ˙ 
   
  Ń@  ˙ 
   
  Đ@  ˙ 
   
  Ď@  ˙ 
   
  Î@  ˙ 
   
  Í@  ˙ 
   
  Ě@  ˙ 
   
  Ë@  ˙ 
   
  Ę@  ˙ 
   
  É@Ą  ˙ 
   
  Č@Ł  ˙ 
   
  Ç@Ľ  ˙ 
   
  Ć@§  ˙ 
   
  Ĺ@Š  ˙ 
   
  Ä@Ť  ˙ 
   
  Ă@­  ˙ 
   
  Â@Ż  ˙ 
   
  Á@ą  ˙ 
   
  Ŕ@ł  ˙ 
   
  ż@ľ  ˙ 
   
  ž@ˇ  ˙ 
   
  ˝@š  ˙ 
   
  ź@ť  ˙ 
   
  ť@˝  ˙ 
   
  ş@ż  ˙ 
   
  š@Á  ˙ 
   
  ¸@Ă  ˙ 
   
  ˇ@Ĺ  ˙ 
   
  ś@Ç  ˙ 
   
  ľ@É  ˙ 
   
  ś@Ç  ˙ 
   
  ˇ@Ĺ  ˙ 
   
  ¸@Ă  ˙ 
   
  š@Á  ˙ 
   
  ş@ż  ˙ 
   
  ť@˝  ˙ 
   
  ź@ť  ˙ 
   
  ˝@š  ˙ 
   
  ž@ˇ  ˙ 
   
  ż@ľ  ˙ 
   
  Ŕ@ł  ˙ 
   
  Á@ą  ˙ 
   
  Â@Ż  ˙ 
   
  Ă@­  ˙ 
   
  Ä@Ť  ˙ 
   
  Ĺ@Š  ˙ 
   
  Ć@§  ˙ 
   
  Ç@Ľ  ˙ 
   
  Č@Ł  ˙ 
   
  É@Ą  ˙ 
   
  Ę@  ˙ 
   
  Ë@  ˙ 
   
  Ě@  ˙ 
   
  Í@  ˙ 
   
  Î@  ˙ 
   
  Ď@  ˙ 
   
  Đ@  ˙ 
   
  Ń@  ˙ 
   
  Ň@  ˙ 
   
  Ó@   ˙ 
   
  Ô@Ą  ˙ 
   
  Ő@˘  ˙ 
   
  Ö@Ł  ˙ 
   
  ×@¤  ˙ 
   
  Ů@Ś  ˙ 
   
  Ú@§  ˙ 
   
  Ű@}¨  ˙ 
   
  Ü@{Š  ˙ 
   
  Ý@yŞ  ˙ 
   
  ß@uŹ  ˙ 
   
  ŕ@s­  ˙ 
   
  â@oŻ  ˙ 
   
  ă@m°  ˙ 
   
  ĺ@i˛  ˙ 
   
  ć@gł  ˙ 
   
  č@cľ  ˙ 
   
  ę@_ˇ  ˙ 
   
  ě@[š  ˙ 
   
  î@Wť  ˙ 
   
  đ@S˝  ˙ 
   
  ň@Oż  ˙ 
   
  ő@IÂ  ˙ 
   
  ÷@EÄ  ˙ 
   
  ú@?Ç  ˙ 
   
  ý@9Ę  ˙ 
   
  @1Î  ˙ 
   
  @'Ó  ˙ 
   
  @Ř  ˙ 	   	  Sć